package org.supcomhub.server.apt;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

// Public getters are accessed by the Freemarker template.
@SuppressWarnings("WeakerAccess")
public class Type {
  private String identifier;
  private String title;
  private String description;
  private Collection<Field> fields;
  private String jsonFormat;
  private List<EnumValue> enumValues;

  public Type(String identifier, String title, String description, Collection<Field> fields, String jsonFormat, List<EnumValue> enumValues) {
    this.identifier = identifier;
    this.title = title;
    this.description = description;
    this.fields = fields;
    this.jsonFormat = jsonFormat;
    this.enumValues = enumValues;
  }

  /** Used in index.ftlh. */
  @SuppressWarnings("unused")
  public Set<Type> getRelatedTypes() {
    return fields.stream()
      .map(Field::getType)
      .filter(type -> !type.getFields().isEmpty() && type.enumValues.isEmpty())
      .collect(Collectors.toSet());
  }

  public String getIdentifier() {
    return this.identifier;
  }

  public String getTitle() {
    return this.title;
  }

  public String getDescription() {
    return this.description;
  }

  public Collection<Field> getFields() {
    return this.fields;
  }

  public String getJsonFormat() {
    return this.jsonFormat;
  }

  public List<EnumValue> getEnumValues() {
    return this.enumValues;
  }

  public String toString() {
    return "Type(identifier=" + this.getIdentifier() + ", title=" + this.getTitle() + ", description=" + this.getDescription() + ", fields=" + this.getFields() + ", jsonFormat=" + this.getJsonFormat() + ", enumValues=" + this.getEnumValues() + ")";
  }

  public static class Field {
    private String name;
    private Type type;
    private String description;
    private boolean required;
    private boolean array;

    public Field(String name, Type type, String description, boolean required, boolean array) {
      this.name = name;
      this.type = type;
      this.description = description;
      this.required = required;
      this.array = array;
    }

    public String getName() {
      return this.name;
    }

    public Type getType() {
      return this.type;
    }

    public String getDescription() {
      return this.description;
    }

    public boolean isRequired() {
      return this.required;
    }

    public boolean isArray() {
      return this.array;
    }

    public String toString() {
      return "Type.Field(name=" + this.getName() + ", type=" + this.getType() + ", description=" + this.getDescription() + ", required=" + this.isRequired() + ", array=" + this.isArray() + ")";
    }
  }

  public static class EnumValue {
    private String name;
    private String description;

    public EnumValue(String name, String description) {
      this.name = name;
      this.description = description;
    }

    public String getName() {
      return this.name;
    }

    public String getDescription() {
      return this.description;
    }

    public String toString() {
      return "Type.EnumValue(name=" + this.getName() + ", description=" + this.getDescription() + ")";
    }
  }
}
