package org.supcomhub.server.apt;

import java.util.List;

public class MessageCategory {
  private String title;
  private List<Type> messageTypes;

  public MessageCategory(String title, List<Type> messageTypes) {
    this.title = title;
    this.messageTypes = messageTypes;
  }

  public String getTitle() {
    return this.title;
  }

  public List<Type> getMessageTypes() {
    return this.messageTypes;
  }

  public String toString() {
    return "MessageCategory(title=" + this.getTitle() + ", messageTypes=" + this.getMessageTypes() + ")";
  }
}
