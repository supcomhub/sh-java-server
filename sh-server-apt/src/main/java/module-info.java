module sh.server.apt {
  requires com.fasterxml.jackson.core;
  requires com.fasterxml.jackson.databind;
  requires jsr305;
  requires java.compiler;
  requires sh.server.annotations;
  requires auto.service;
  requires freemarker;
  requires java.validation;
  requires com.github.nocatch;
  exports org.supcomhub.server.apt;
}
