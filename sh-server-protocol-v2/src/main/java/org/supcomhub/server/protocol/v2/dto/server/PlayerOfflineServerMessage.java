package org.supcomhub.server.protocol.v2.dto.server;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.supcomhub.server.annotations.V2ServerResponse;

/** Message sent from the server to the client informing it about a player who went offline. */
@Getter
@Setter
@V2ServerResponse
public class PlayerOfflineServerMessage extends V2ServerMessage {

  public static final String TYPE_NAME = "playerOffline";

  /** The player who went offline. */
  private Player player;

  @Data
  public static class Player {
    /** The ID of the player. */
    int id;
    /** The current display name of the player. */
    String name;
  }
}
