package org.supcomhub.server.protocol.v2.dto.client;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.supcomhub.server.annotations.V2ClientNotification;

/**
 * Message sent from the client to the server to request disconnecting a client from the server.
 */
@Getter
@Setter
@AllArgsConstructor
@V2ClientNotification
@NoArgsConstructor
public class DisconnectClientClientMessage extends V2ClientMessage {

  public static final String TYPE_NAME = "disconnectClient";

  /** ID of the player whose client should be disconnected. */
  private int playerId;
}
