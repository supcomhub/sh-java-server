package org.supcomhub.server.protocol.v2.dto.client;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.supcomhub.server.annotations.V2ClientNotification;

/**
 * Message sent from the game to the server to inform the server that a player slot within the game lobby has been
 * cleared.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@V2ClientNotification
public class ClearSlotClientMessage extends V2GpgClientMessage {

  public static final String TYPE_NAME = "gpgClearSlot";

  /**
   * <ol start="0">
   * <li>(int) The ID of the game slot that has been cleared.</li>
   * </ol>
   */
  private Object[] args;

  public ClearSlotClientMessage(int slotId) {
    args = new Object[]{slotId};
  }

  @JsonIgnore
  public int getSlotId() {
    return getInt(0);
  }
}
