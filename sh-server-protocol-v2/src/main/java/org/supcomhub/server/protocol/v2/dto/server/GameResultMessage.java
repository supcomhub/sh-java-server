package org.supcomhub.server.protocol.v2.dto.server;

import lombok.Getter;
import lombok.Setter;
import org.supcomhub.server.annotations.V2ServerResponse;

import java.util.Set;

/**
 * Results of a finished game.
 */
@Getter
@Setter
@V2ServerResponse
public class GameResultMessage extends V2ServerMessage {

  public static final String TYPE_NAME = "gameResult";

  /** ID of the game. */
  private int gameId;

  /** {@code true} if there is no winner, {@code false} otherwise. */
  private boolean draw;

  /** The list of player results. */
  private Set<PlayerResult> playerResults;

  /** Represents a player result. */
  @Getter
  @Setter
  public static class PlayerResult {
    /** The ID of the player. */
    private int playerId;
    /** {@code true} if the player won, {@code false} otherwise. */
    private boolean winner;
    /** {@code true} if the player's ACU got killed, {@code false} otherwise. */
    private boolean acuKilled;
  }
}
