package org.supcomhub.server.protocol.v2.dto.client;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.supcomhub.server.annotations.V2ClientNotification;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Message sent from the client to the server informing it about the activated simulation mods in the game.
 */
@AllArgsConstructor
@NoArgsConstructor
@V2ClientNotification
public class GameModsClientMessage extends V2GpgClientMessage {

  public static final String TYPE_NAME = "gpgGameMods";

  /**
   * Due to the current implementation in the game code, the arguments of this message are:
   * <ol start="0">
   * <li>(string) Either {@code activated} or {@code uids}</li>
   * <li>(string or int) If the first parameter is {@code activated}, the number of mods that have been activated. If
   * the first parameter is {@code uids}, a space-separated list of simulation mod UUIDs that have been activated.</li>
   * </ol>
   * This should be refactored to exactly one parameter: an (optionally empty) list of active UIDs.
   */
  private Object[] args = new Object[2];

  @JsonIgnore
  public int getActivatedMods() {
    // TODO the game should stop sending `activated`. Once it does, remove it.
    if (Objects.equals(args[0], "activated")) {
      return Integer.parseInt(Objects.toString(args[1]));
    }
    if (Objects.equals(args[0], "uids")) {
      return getModUuids().size();
    }
    throw new IllegalArgumentException("First argument needs to be 'activated' or 'uids', but was: " + args[0]);
  }

  public void setActivatedMods(int activatedMods) {
    args[0] = activatedMods;
  }

  public void setModUuids(List<UUID> uuids) {
    if (Objects.equals(args[0], "activated")) {
      args[1] = uuids.size();
    }
    if (Objects.equals(args[0], "uids")) {
      args[1] = uuids.stream()
        .map(UUID::toString)
        .collect(Collectors.joining(" "));
    }
    throw new IllegalArgumentException("First argument needs to be 'activated' or 'uids', but was: " + args[0]);
  }

  @JsonIgnore
  public List<UUID> getModUuids() {
    if (!Objects.equals(args[0], "uids")) {
      return Collections.emptyList();
    }

    return Arrays.stream(getString(1).split(" "))
      .map(UUID::fromString)
      .collect(Collectors.toList());
  }

  @Override
  public Object[] getArgs() {
    return args;
  }
}
