package org.supcomhub.server.protocol.v2.dto.client;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.supcomhub.server.annotations.V2ClientNotification;

/**
 * Message sent from the game to the server to inform the server that a previously reported bottleneck has been
 * cleared.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@V2ClientNotification
public class BottleneckClientMessage extends V2GpgClientMessage {

  public static final String TYPE_NAME = "gpgBottleneckCleared";

  /**
   * Not sure if there are any arguments.
   */
  private Object[] args;

}
