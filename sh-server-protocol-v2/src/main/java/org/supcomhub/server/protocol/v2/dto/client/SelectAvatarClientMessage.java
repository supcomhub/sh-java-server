package org.supcomhub.server.protocol.v2.dto.client;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.supcomhub.server.annotations.V2ClientNotification;

/**
 * Message sent from the client to the server to change the current player's avatar.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@V2ClientNotification
public class SelectAvatarClientMessage extends V2ClientMessage {

  public static final String TYPE_NAME = "selectAvatar";

  /** The ID of the new avatar. */
  private int avatarId;
}
