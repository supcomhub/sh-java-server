package org.supcomhub.server.protocol.v2.dto.client;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.supcomhub.server.annotations.V2ClientNotification;

/**
 * Message sent from the game to the server informing it about an army outcome.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@V2ClientNotification
public class ArmyOutcomeClientMessage extends V2GpgClientMessage {

  public static final String TYPE_NAME = "gpgArmyOutcome";

  /**
   * <ol start="0">
   * <li>(int) The ID of the army affected by this report.</li>
   * <li>(string) Whether the army was victorious or not. One of {@code DEFEAT}, {@code VICTORY}, {@code DRAW}.</li>
   * <li>(int) The army's final game score.</li>
   * </ol>
   */
  private Object[] args;

  public ArmyOutcomeClientMessage(int armyId, Outcome outcome, int score) {
    args = new Object[]{armyId, outcome.toProtocolValue(), score};
  }

  @JsonIgnore
  public int getArmyId() {
    return getInt(0);
  }

  @JsonIgnore
  public Outcome getOutcome() {
    return Outcome.fromProtocolValue(getString(1));
  }

  @JsonIgnore
  public int getScore() {
    return getInt(2);
  }

}
