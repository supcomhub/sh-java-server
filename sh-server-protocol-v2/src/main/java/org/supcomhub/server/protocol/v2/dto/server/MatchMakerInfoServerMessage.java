package org.supcomhub.server.protocol.v2.dto.server;


import lombok.Getter;
import lombok.Setter;
import org.supcomhub.server.annotations.V2ServerResponse;

import java.util.Map;

/**
 * Message sent from the server to the client informing it about available matches.
 */
@Getter
@Setter
@V2ServerResponse
public class MatchMakerInfoServerMessage extends V2ServerMessage {

  public static final String TYPE_NAME = "matchAvailable";

  /**
   * A map of match maker pool names to the number of players who are currently searching for a game in this pool. The
   * server may choose to only send information about updates queues. That is, if a pool was announced to have N players
   * but a subsequent message does not specify this pool name again, it means that the number of players remains
   * unchanged.
   */
  private Map<String, Integer> playersByPool;
}
