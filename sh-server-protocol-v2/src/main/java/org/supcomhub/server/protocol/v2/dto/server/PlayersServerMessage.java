package org.supcomhub.server.protocol.v2.dto.server;

import lombok.Getter;
import lombok.Setter;
import org.supcomhub.server.annotations.V2ServerResponse;

import java.util.List;

/**
 * Message sent from the server to the client containing information about players.
 */
@Getter
@Setter
@V2ServerResponse
public class PlayersServerMessage extends V2ServerMessage {

  public static final String TYPE_NAME = "players";

  /** The list of players. */
  List<PlayerServerMessage> players;
}
