package org.supcomhub.server.protocol.v2.dto.client;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.supcomhub.server.annotations.V2ClientNotification;

/**
 * Message sent from the game to the server informing it about a changed game option.
 */
@Getter
@AllArgsConstructor
@NoArgsConstructor
@V2ClientNotification
public class GameOptionClientMessage extends V2GpgClientMessage {

  public static final String TYPE_NAME = "gpgGameOption";

  /**
   * <ol start="0">
   * <li>(string) The game option's key as in the game code.</li>
   * <li>(string) The game option's value.</li>
   * </ol>
   */
  private Object[] args;

  public GameOptionClientMessage(String key, String value) {
    args = new Object[]{key, value};
  }

  @JsonIgnore
  public String getKey() {
    return getString(0);
  }

  @JsonIgnore
  public String getValue() {
    return getString(1);
  }
}
