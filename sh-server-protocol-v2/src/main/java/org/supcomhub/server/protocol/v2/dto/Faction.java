package org.supcomhub.server.protocol.v2.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.HashMap;
import java.util.Map;

public enum Faction {
  /** Aeon. */
  AEON("aeon"),
  /** Cybran. */
  CYBRAN("cybran"),
  /** UEF. */
  UEF("uef"),
  /** Seraphim. */
  SERAPHIM("seraphim"),
  /** Nomad. */
  NOMAD("nomad"),
  /** Civilian. Can not be chosen as a faction to play. */
  CIVILIAN("civilian");

  private static final Map<String, Faction> byProtocolValue;

  static {
    byProtocolValue = new HashMap<>();
    for (Faction faction : values()) {
      byProtocolValue.put(faction.protocolValue, faction);
    }
  }

  private final String protocolValue;

  Faction(String protocolValue) {
    this.protocolValue = protocolValue;
  }

  /**
   * Returns the faction value used as in the protocol.
   */
  @JsonValue
  public String toProtocolValue() {
    return protocolValue;
  }

  @JsonCreator
  public static Faction fromProtocolValue(String value) {
    return byProtocolValue.get(value);
  }
}
