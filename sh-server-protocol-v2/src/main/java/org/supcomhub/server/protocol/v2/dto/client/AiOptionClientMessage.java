package org.supcomhub.server.protocol.v2.dto.client;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.supcomhub.server.annotations.V2ClientNotification;

/**
 * Message sent from the game to the server informing it about a changed AI option.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@V2ClientNotification
public class AiOptionClientMessage extends V2GpgClientMessage {

  public static final String TYPE_NAME = "gpgAIOption";

  /**
   * <ol start="0">
   * <li>(string) The name of the AI whose option has been changed.</li>
   * <li>(string) The AI option's key as in the game code.</li>
   * <li>(string) The AI option's value.</li>
   * </ol>
   */
  private Object[] args;

  public AiOptionClientMessage(String aiName, String key, String value) {
    args = new Object[]{aiName, key, value};
  }

  @JsonIgnore
  public String getAiName() {
    return getString(0);
  }

  @JsonIgnore
  public String getKey() {
    return getString(1);
  }

  @JsonIgnore
  public String getValue() {
    return getString(2);
  }
}
