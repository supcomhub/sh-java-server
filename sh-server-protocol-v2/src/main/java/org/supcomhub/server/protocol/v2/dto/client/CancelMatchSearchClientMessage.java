package org.supcomhub.server.protocol.v2.dto.client;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.supcomhub.server.annotations.V2ClientNotification;

import javax.validation.constraints.NotNull;

/**
 * Message sent from the client to the server to inform the server to stop searching for a match.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@V2ClientNotification
public class CancelMatchSearchClientMessage extends V2ClientMessage {

  public static final String TYPE_NAME = "cancelMatchSearch";

  /** The name of the matchmaker pool to cancel the search for. */
  @NotNull
  private String pool;
}
