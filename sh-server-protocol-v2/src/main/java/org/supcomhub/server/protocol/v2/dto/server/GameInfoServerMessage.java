package org.supcomhub.server.protocol.v2.dto.server;


import lombok.Getter;
import lombok.Setter;
import org.supcomhub.server.annotations.V2ServerResponse;
import org.supcomhub.server.protocol.v2.dto.GameVisibility;
import org.supcomhub.server.protocol.v2.dto.VictoryCondition;

import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.List;
import java.util.UUID;

/**
 * Message sent from the server to the client containing information about a game.
 */
@Getter
@Setter
@V2ServerResponse
public class GameInfoServerMessage extends V2ServerMessage {

  public static final String TYPE_NAME = "game";

  /** The game's ID, e.g. {@code 951989}. */
  int id;

  /** The game's title, e.g. {@code All Welcome}. */
  @NotNull
  String title;

  /** Specifies who can see the game, e.g. {@code PUBLIC}. */
  @NotNull
  GameVisibility gameVisibility;

  /** {@code true} if the game requires a password in order to join. */
  boolean passwordProtected;

  /** The current game state, e.g. {@code OPEN}. */
  @NotNull
  GameState state;

  /** The condition to win this game. */
  @NotNull
  VictoryCondition victoryCondition;

  /**
   * The ID of the leaderboard that will be affected by the result of this game. {@code null} if no leaderboard will be
   * affected.
   */
  Integer leaderboardId;

  /** The technical name of the game's featured mod, e.g. {@code sh}. */
  @NotNull
  FeaturedMod mod;

  /** The list of simulation mods enabled in this game. */
  @NotNull
  List<SimMod> simMods;

  /** The folder name of the map played in this game, e.g. {@code africa_ultimate.v0002} */
  @NotNull
  String map;

  /** The player who created the game. */
  @NotNull
  Player host;

  /** The players that are currently part of this game. */
  @NotNull
  List<Player> players;

  /** The maximum number of players who can play on the currently selected map. */
  int maxPlayers;

  /** When the game has been started, e.g. {@code 2018-05-09T22:53}. */
  @NotNull
  Instant startTime;

  /** The minimum rank participating players should have, as desired by the host of the game. E.g. {@code 8}. */
  Integer minRank;

  /** The maximum rank participating players should have, as desired by the host of the game. E.g. {@code 10}. */
  Integer maxRank;

  /** A player within a game. */
  @Getter
  @Setter
  public static class Player {
    /** The player's ID. */
    int id;
    /** The player's display name. */
    String name;
    /**
     * The team within the game this player is currently assigned to. 1 for "no team", -1 for "observer", >= 2 for teams
     * >=1.
     */
    int team;
  }

  /** A simulation mod. */
  @Getter
  @Setter
  public static class SimMod {
    /** The simulation mod's unique identifier. */
    UUID uuid;
    /** The simulation mod's human-readable display name. */
    String displayName;
  }

  @Getter
  @Setter
  public static class FeaturedMod {
    /** The technical name of this mod which identifies it uniquely. */
    String name;
    /** The version number of the mod, e.g. {@code 3765} */
    int version;
  }
}
