/**
 * Contains DTO classes and mappers to serialize messages sent from the server to the client.
 */
package org.supcomhub.server.protocol.v2.dto.server;
