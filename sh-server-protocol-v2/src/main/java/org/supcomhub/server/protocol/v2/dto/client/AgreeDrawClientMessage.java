package org.supcomhub.server.protocol.v2.dto.client;

import lombok.Getter;
import org.supcomhub.server.annotations.V2ClientNotification;

/**
 * Message sent from the game to the server informing it that the sending player would like to agree to a draw.
 */
@Getter
@V2ClientNotification
public class AgreeDrawClientMessage extends V2GpgClientMessage {

  public static final String TYPE_NAME = "gpgAgreeDraw";

  /** Empty. */
  private Object[] args = new Object[0];
}
