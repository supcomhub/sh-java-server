package org.supcomhub.server.protocol.v2.dto.server;

import lombok.Getter;
import lombok.Setter;
import org.supcomhub.server.annotations.V2ServerResponse;

import java.util.List;

/**
 * Message sent from the server to the client containing information about games.
 */
@Getter
@Setter
@V2ServerResponse
public
class GameInfosServerMessage extends V2ServerMessage {

  public static final String TYPE_NAME = "games";

  /** The list of games. */
  List<GameInfoServerMessage> games;
}
