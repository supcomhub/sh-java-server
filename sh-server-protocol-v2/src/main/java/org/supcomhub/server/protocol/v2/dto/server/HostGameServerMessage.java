package org.supcomhub.server.protocol.v2.dto.server;

import lombok.Getter;
import lombok.Setter;
import org.supcomhub.server.annotations.V2ServerResponse;

/**
 * Message sent from the server to the client telling it to host a game.
 */
@Getter
@Setter
@V2ServerResponse
public class HostGameServerMessage extends V2ServerMessage {

  public static final String TYPE_NAME = "hostGame";

  /** (Folder) name of the map to be played. */
  String mapName;
}
