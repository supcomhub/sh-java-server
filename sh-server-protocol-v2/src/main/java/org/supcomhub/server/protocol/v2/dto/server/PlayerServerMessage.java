package org.supcomhub.server.protocol.v2.dto.server;

import lombok.Getter;
import lombok.Setter;
import org.supcomhub.server.annotations.V2ServerResponse;

import javax.validation.constraints.NotNull;
import java.net.URL;
import java.util.Map;
import java.util.TimeZone;

/**
 * Message sent from the server to the client containing information about a player.
 */
@Getter
@Setter
@V2ServerResponse
public class PlayerServerMessage extends V2ServerMessage {

  public static final String TYPE_NAME = "player";

  /** The player's ID, e.g. {@code 51231}. */
  private int id;

  /** The player's display name. */
  @NotNull
  private String displayName;

  /** The two-letter country code of the location of the player's IP address, if available. */
  private String country;
  /**
   * The time zone ID of the location of the player's IP address, if available. Either an abbreviation such as "PST", a
   * full name such as "America/Los_Angeles", or a custom ID such as "GMT-8:00"
   */
  private TimeZone timeZone;

  /** The player's ranks, mapped by leaderboard ID. */
  private Map<Integer, Integer> ranks;

  /** The number of games the player has played. */
  private int numberOfGames;

  /** The player's avatar, if any. */
  private Avatar avatar;

  /** The player's clan tag, if any. */
  private String clanTag;

  /** The game the player is currently part of. */
  private Game game;

  /** A player's avatar. */
  @Getter
  @Setter
  public static class Avatar {
    /** The avatar's URL. */
    private URL url;

    /** The avatar's description. */
    private String description;
  }

  /**
   * Limited information about the player's game. The full game info is sent separately, clients should wait for the
   * respective message to complete the information once it's available.
   */
  @Getter
  @Setter
  public static class Game {
    private int id;
  }
}
