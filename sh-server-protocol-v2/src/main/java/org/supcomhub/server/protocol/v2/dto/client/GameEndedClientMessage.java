package org.supcomhub.server.protocol.v2.dto.client;

import lombok.Getter;
import lombok.Setter;
import org.supcomhub.server.annotations.V2ClientNotification;

/**
 * Message sent from either the client or the game to the server informing it that the simulation of a game has ended.
 */
@Getter
@Setter
@V2ClientNotification
public class GameEndedClientMessage extends V2GpgClientMessage {

  public static final String TYPE_NAME = "gpgGameEnded";

  private final Object[] args = new Object[0];

}
