package org.supcomhub.server.protocol.v2.dto.client;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.supcomhub.server.annotations.V2ClientRequest;
import org.supcomhub.server.protocol.v2.dto.server.StartGameProcessServerMessage;

/**
 * Message sent from the client to the server to request joining a game.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@V2ClientRequest(successResponse = StartGameProcessServerMessage.class)
public class JoinGameClientMessage extends V2ClientMessage {

  public static final String TYPE_NAME = "joinGame";

  /** The ID of the game to be joined. */
  private int id;

  /** The password to use in case the game is password protected. */
  private String password;
}
