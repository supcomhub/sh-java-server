package org.supcomhub.server.protocol.v2.dto.client;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.supcomhub.server.annotations.V2ClientNotification;
import org.supcomhub.server.protocol.v2.dto.Faction;

import javax.validation.constraints.NotNull;

/**
 * Message sent from the client to the server to inform it that the current player would like to participate in a
 * match.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@V2ClientNotification
public class SearchMatchClientMessage extends V2ClientMessage {

  public static final String TYPE_NAME = "searchMatch";

  /** The faction the player will be playing. */
  @NotNull
  private Faction faction;

  /** The name of the matchmaker pool to submit this search request to. */
  @NotNull
  private String pool;
}
