package org.supcomhub.server.protocol.v2.dto.client;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.supcomhub.server.annotations.V2ClientNotification;

/**
 * Message sent from the game to the server to inform the server about some sort of bottleneck in the P2P communication.
 * Details are currently unknown (please let us know if you have details).
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@V2ClientNotification
public class BottleneckClearedClientMessage extends V2GpgClientMessage {

  public static final String TYPE_NAME = "gpgBottleneck";

  /**
   * Not sure if there are any arguments.
   */
  private Object[] args;

}
