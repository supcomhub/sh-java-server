package org.supcomhub.server.protocol.v2.dto.server;

public abstract class V2GpgServerMessage extends V2ServerMessage {

  void setValue(int index, Object value) {
    getArgs()[index] = value;
  }

  public abstract Object[] getArgs();
}
