package org.supcomhub.server.protocol.v2.dto.client;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.supcomhub.server.annotations.V2ClientNotification;

import javax.validation.constraints.NotNull;

/**
 * Message sent from the client to the server, containing an ICE message to be forwarded to the specified received.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@V2ClientNotification
public class IceClientMessage extends V2ClientMessage {

  public static final String TYPE_NAME = "iceMessage";

  /** ID of the player to send the message to. */
  private int receiverId;

  /** The ICE message content. */
  @NotNull
  private Object content;
}
