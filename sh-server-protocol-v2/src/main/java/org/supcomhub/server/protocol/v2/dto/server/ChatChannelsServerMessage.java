package org.supcomhub.server.protocol.v2.dto.server;

import lombok.Getter;
import lombok.Setter;
import org.supcomhub.server.annotations.V2ServerResponse;

import java.util.List;

/**
 * Message sent from the server to the client containing a list of chat channels to be joined.
 */
@Getter
@Setter
@V2ServerResponse
public class ChatChannelsServerMessage extends V2ServerMessage {

  public static final String TYPE_NAME = "chatChannels";

  /** A list of chat channels to be joined by the client. */
  List<String> channels;
}
