package org.supcomhub.server.protocol.v2.dto.client;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.supcomhub.server.annotations.V2ClientNotification;

/**
 * Message sent from either the client or the game to the server informing it about the player's game state.
 */
@Getter
@AllArgsConstructor
@NoArgsConstructor
@V2ClientNotification
public class GameStateClientMessage extends V2GpgClientMessage {

  public static final String TYPE_NAME = "gpgGameState";

  /**
   * <ol start="0">
   * <li>(string) The new state of the player's game.</li>
   * </ol>
   */
  private Object[] args;

  public GameStateClientMessage(PlayerGameState state) {
    args = new Object[]{state.toProtocolValue()};
  }

  @JsonIgnore
  public PlayerGameState getState() {
    return PlayerGameState.fromProtocolValue(getString(0));
  }
}
