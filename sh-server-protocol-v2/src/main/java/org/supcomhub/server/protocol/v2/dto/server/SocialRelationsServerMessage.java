package org.supcomhub.server.protocol.v2.dto.server;

import lombok.Getter;
import lombok.Setter;
import org.supcomhub.server.annotations.V2ServerResponse;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Message sent from the server to the client containing information about the player's social relations.
 */
@Getter
@Setter
@V2ServerResponse
public class SocialRelationsServerMessage extends V2ServerMessage {

  public static final String TYPE_NAME = "socialRelations";

  /** The list of social relations. */
  @NotNull
  private List<SocialRelation> socialRelations;

  /** Defines a social relation the current player has to another player has to another (uni-directional). */
  @Getter
  @Setter
  public static class SocialRelation {

    /** The ID of the "other" account affected by this relation. */
    int accountId;

    /** The type of the relation. */
    @NotNull
    RelationType type;

    /** The type of the social relation. */
    public enum RelationType {
      /** The "current" player sees the "other" player as a friend. */
      FRIEND,

      /** The "current" player sees the "other" player as a foe. */
      FOE
    }
  }
}
