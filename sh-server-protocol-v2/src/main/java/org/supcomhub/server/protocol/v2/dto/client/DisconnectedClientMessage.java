package org.supcomhub.server.protocol.v2.dto.client;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.supcomhub.server.annotations.V2ClientNotification;

/**
 * Message sent from the game to the server informing it that the current player has disconnected the specified player.
 */
@Getter
@Setter
@NoArgsConstructor
@V2ClientNotification
public class DisconnectedClientMessage extends V2GpgClientMessage {

  public static final String TYPE_NAME = "gpgDisconnected";

  /**
   * <ol start="0">
   * <li>(int) ID of the player who has been disconnected.</li>
   * </ol>
   */
  private Object[] args;

  public DisconnectedClientMessage(int playerId) {
    args = new Object[]{playerId};
  }

  @JsonIgnore
  public int getPlayerId() {
    return getInt(0);
  }
}
