package org.supcomhub.server.protocol.v2.dto.server;


import org.supcomhub.server.annotations.V2ServerResponse;

/**
 * Message sent from the server to the client telling it to disconnect from a peer.
 */
@V2ServerResponse
public class DisconnectPeerServerMessage extends V2GpgServerMessage {

  public static final String TYPE_NAME = "gpgDisconnectFromPeer";

  /**
   * <ol start="0">
   * <li>(int32) ID of the player to disconnect.</li>
   * </ol>
   */
  private Object[] args = new Object[1];

  public void setPlayerId(int playerId) {
    setValue(0, playerId);
  }

  @Override
  public Object[] getArgs() {
    return args;
  }
}
