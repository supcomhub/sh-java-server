package org.supcomhub.server.protocol.v2.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

public enum VictoryCondition {
  /** The game ends when all enemy commanders are destroyed. */
  DEMORALIZATION("demoralization"),
  /** The game ends when all enemy structures, commanders and engineers are destroyed. */
  DOMINATION("domination"),
  /** The game ends when all enemy units are destroyed. */
  ERADICATION("eradication"),
  /** The game never ends. */
  SANDBOX("sandbox");

  private static final Map<String, VictoryCondition> byProtocolValue;

  static {
    byProtocolValue = new HashMap<>();
    for (VictoryCondition condition : values()) {
      byProtocolValue.put(condition.protocolValue, condition);
    }
  }

  @Getter
  private final String protocolValue;

  VictoryCondition(String protocolValue) {
    this.protocolValue = protocolValue;
  }

  /**
   * Returns the {@link VictoryCondition} for a string sent by the game.
   */
  @JsonCreator
  public static VictoryCondition fromProtocolValue(String string) {
    return byProtocolValue.get(string);
  }

  @JsonValue
  public String toProtocolValue() {
    return protocolValue;
  }
}
