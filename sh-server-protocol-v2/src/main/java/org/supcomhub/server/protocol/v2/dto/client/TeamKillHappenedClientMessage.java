package org.supcomhub.server.protocol.v2.dto.client;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.supcomhub.server.annotations.V2ClientNotification;

/**
 * Message sent from the game to the server to inform it that a player killed his team member.
 */
@Getter
@AllArgsConstructor
@NoArgsConstructor
@V2ClientNotification
public class TeamKillHappenedClientMessage extends V2GpgClientMessage {

  public static final String TYPE_NAME = "gpgTeamkillHappened";

  // TODO the reporter should not be able to send information about the victim because this information is implicitly
  //  available (one can only report if oneself is the victim) and the names are irrelevant, too.
  /**
   * <ol start="0">
   * <li>(int) How many seconds into the game the team kill happened.</li>
   * <li>(int) The ID of the player who has been killed.</li>
   * <li>(string) The name of the player who has been killed.</li>
   * <li>(int) The ID of the player who performed the team kill.</li>
   * <li>(String) The name of the player who performed the team kill.</li>
   * </ol>
   */
  private Object[] args;

  public TeamKillHappenedClientMessage(int time, int victimId, String victimName, int killerId, String killerName) {
    args = new Object[]{time, victimId, victimName, killerId, killerName};
  }

  @JsonIgnore
  public int getTime() {
    return getInt(0);
  }

  @JsonIgnore
  public int getVictimId() {
    return getInt(1);
  }

  @JsonIgnore
  public String getVictimName() {
    return getString(2);
  }

  @JsonIgnore
  public int getKillerId() {
    return getInt(3);
  }

  @JsonIgnore
  public String getKillerName() {
    return getString(4);
  }
}
