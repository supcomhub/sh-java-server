package org.supcomhub.server.protocol.v2.dto.client;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.HashMap;
import java.util.Map;

public enum Outcome {
  /** The army has been defeated. */
  DEFEAT("Defeat"),
  /** The army won the game. */
  VICTORY("Victory"),
  /** The army hasn't won nor lost the game. */
  DRAW("Draw");

  private static final Map<String, Outcome> fromProtocolValue;

  static {
    fromProtocolValue = new HashMap<>();
    for (Outcome state : values()) {
      fromProtocolValue.put(state.protocolValue, state);
    }
  }

  /**
   * String as sent by the game.
   */
  private final String protocolValue;

  Outcome(String protocolValue) {
    this.protocolValue = protocolValue;
  }

  @JsonValue
  public String toProtocolValue() {
    return protocolValue;
  }

  @JsonCreator
  public static Outcome fromProtocolValue(String string) {
    return fromProtocolValue.get(string);
  }
}
