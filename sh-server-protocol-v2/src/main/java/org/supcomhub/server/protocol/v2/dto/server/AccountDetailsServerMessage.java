package org.supcomhub.server.protocol.v2.dto.server;

import lombok.Getter;
import lombok.Setter;
import org.supcomhub.server.annotations.V2ServerResponse;

import javax.validation.constraints.NotNull;
import java.net.URL;
import java.util.Map;
import java.util.TimeZone;

/**
 * Message sent from the server to the client containing details about the logged in account.
 */
@Getter
@Setter
@V2ServerResponse
public class AccountDetailsServerMessage extends V2ServerMessage {
  public static final String TYPE_NAME = "loginDetails";

  /** The player's ID, e.g. {@code 51231}. */
  private int id;

  /** The player's display name. */
  @NotNull
  private String displayName;

  /** The player's ranks, mapped by leaderboard ID. */
  private Map<Integer, Integer> ranks;

  /** The number of games the player has played. */
  private int numberOfGames;

  /** The player's avatar, if any. */
  private Avatar avatar;

  /** The player's clan tag, if any. */
  private String clanTag;

  /** The player's time zone, if known. */
  private TimeZone timeZone;

  /** The player's country, if known. */
  private String country;

  /** A player's avatar. */
  @Getter
  @Setter
  public static class Avatar {
    /** The avatar's URL. */
    private URL url;

    /** The avatar's description. */
    private String description;
  }
}
