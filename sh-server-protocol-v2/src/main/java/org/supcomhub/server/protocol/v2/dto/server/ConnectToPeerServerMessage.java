package org.supcomhub.server.protocol.v2.dto.server;

import org.supcomhub.server.annotations.V2ServerResponse;

/**
 * Message sent from the server to the game telling it to connect to another player.
 */
@V2ServerResponse
public class ConnectToPeerServerMessage extends V2GpgServerMessage {

  public static final String TYPE_NAME = "gpgConnectToPeer";

  /**
   * <ol start="0">
   * <li>(int32) The ID of the player to connect to.</li>
   * <li>(string) The name of the player to connect to.</li>
   * <li>(boolean) {@code true} if the connection is an offer, {@code false} if it's a response.</li>
   * </ol>
   */
  private Object[] args = new Object[3];

  public void setPlayerId(int playerId) {
    setValue(0, playerId);
  }

  public void setPlayerName(String playerName) {
    setValue(1, playerName);
  }

  public void setOffer(boolean offer) {
    setValue(2, offer);
  }

  @Override
  public Object[] getArgs() {
    return args;
  }
}
