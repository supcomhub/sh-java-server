package org.supcomhub.server.protocol.v2.dto.client;


import lombok.Getter;
import lombok.Setter;
import org.supcomhub.server.annotations.V2ClientNotification;

/**
 * Message sent from the game to the server to inform it that an army has been defeated.
 *
 * @deprecated since it's none of the game's business to decide whether or not a game should be rated, this message is
 * ignored and will be removed in a future version.
 */
@Getter
@Setter
@V2ClientNotification
@Deprecated(forRemoval = true)
public class EnforceRatingClientMessage extends V2GpgClientMessage {

  public static final String TYPE_NAME = "gpgEnforceRating";

  private final Object[] args = new Object[0];
}
