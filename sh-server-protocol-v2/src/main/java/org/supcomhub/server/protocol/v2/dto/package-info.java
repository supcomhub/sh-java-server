/**
 * Contains classes required to serialize/deserialize messages to/from the v2  protocol.
 */
package org.supcomhub.server.protocol.v2.dto;
