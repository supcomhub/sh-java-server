package org.supcomhub.server.protocol.v2.dto.client;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.supcomhub.server.annotations.V2ClientNotification;

/**
 * Message sent from the client to the server requesting to create, update or remove a social relation to another
 * account.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@V2ClientNotification
public class UpdateSocialRelationClientMessage extends V2ClientMessage {

  public static final String TYPE_NAME = "updateSocialRelation";

  /** The ID of the "other" account. */
  private int accountId;

  /** The operation to perform on this social relation. */
  private Operation operation;

  /** The type of the relation. */
  private RelationType relationType;

  /** The type of the relation. */
  public enum RelationType {
    /** Modify a 'friend' relation. */
    FRIEND,

    /** Modify a 'foe' relation. */
    FOE
  }

  /** The operation to perform on this social relation. */
  public enum Operation {
    /** Add this social relation. */
    ADD,
    /** Remove this social relation. */
    REMOVE
  }
}
