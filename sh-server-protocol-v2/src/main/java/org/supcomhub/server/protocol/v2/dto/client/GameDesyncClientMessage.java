package org.supcomhub.server.protocol.v2.dto.client;


import lombok.Getter;
import lombok.Setter;
import org.supcomhub.server.annotations.V2ClientNotification;

/**
 * Message sent from the game to the server informing it about a desync in current player's game.
 */
@Getter
@Setter
@V2ClientNotification
public class GameDesyncClientMessage extends V2GpgClientMessage {

  public static final String TYPE_NAME = "gpgDesync";

  private final Object[] args = new Object[0];

}
