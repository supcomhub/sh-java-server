package org.supcomhub.server.protocol.v2.dto.client;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.supcomhub.server.annotations.V2ClientNotification;

/**
 * Message sent from the game to the server informing it about a chat message that has been sent by a player in the game
 * lobby.
 */
@Getter
@NoArgsConstructor
@V2ClientNotification
public class GameChatClientMessage extends V2GpgClientMessage {

  public static final String TYPE_NAME = "gpgChat";

  /**
   * <ol start="0">
   * <li>(string) The chat message that has been sent.</li>
   * </ol>
   */
  private Object[] args;

  public GameChatClientMessage(String message) {
    args = new Object[]{message};
  }

  @JsonIgnore
  public String getMessage() {
    return getString(0);
  }
}
