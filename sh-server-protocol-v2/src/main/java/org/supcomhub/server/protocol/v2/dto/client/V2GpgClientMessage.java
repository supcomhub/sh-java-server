package org.supcomhub.server.protocol.v2.dto.client;

import com.github.nocatch.NoCatch;

import java.net.URL;

public abstract class V2GpgClientMessage extends V2ClientMessage {

  private static final Object[] EMPTY_ARRAY = new Object[0];

  protected String getString(int index) {
    return String.valueOf(getArgs()[index]);
  }

  protected URL getUrl(int index) {
    return NoCatch.noCatch(() -> new URL(String.valueOf(getArgs()[index])));
  }

  int getInt(int index) {
    return (int) getArgs()[index];
  }

  float getFloat(int index) {
    return (float) getArgs()[index];
  }

  byte getByte(int index) {
    return (byte) getArgs()[index];
  }

  boolean getBoolean(int index) {
    return (boolean) getArgs()[index];
  }

  public Object[] getArgs() {
    return EMPTY_ARRAY;
  }
}
