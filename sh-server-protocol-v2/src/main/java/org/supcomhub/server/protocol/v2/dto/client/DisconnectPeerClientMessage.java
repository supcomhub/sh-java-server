package org.supcomhub.server.protocol.v2.dto.client;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.supcomhub.server.annotations.V2ClientNotification;

/**
 * Message sent from the client to the server to request players to close the connection to another player.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@V2ClientNotification
public class DisconnectPeerClientMessage extends V2ClientMessage {

  public static final String TYPE_NAME = "disconnectPeer";

  /** The ID of the player who should be disconnected. */
  private int playerId;
}
