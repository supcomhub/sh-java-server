package org.supcomhub.server.protocol.v2.dto.server;

import org.supcomhub.server.annotations.V2Message;

/**
 * Superclass of all message DTO classes sent from the server using the V2 protocol to the client. A client does not
 * need to be a player's client but can also be another service like the Galactic War Server or the API.
 */
@V2Message
public abstract class V2ServerMessage {
}
