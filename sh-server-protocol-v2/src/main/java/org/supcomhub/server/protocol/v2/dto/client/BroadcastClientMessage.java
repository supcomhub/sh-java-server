package org.supcomhub.server.protocol.v2.dto.client;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.supcomhub.server.annotations.V2ClientNotification;

import javax.validation.constraints.NotNull;

/**
 * Message sent from the client to the server to broadcast a text message to all connected clients.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@V2ClientNotification
public class BroadcastClientMessage extends V2ClientMessage {

  public static final String TYPE_NAME = "broadcast";

  /** The message to be broadcasted. */
  @NotNull
  private String message;
}
