package org.supcomhub.server.protocol.v2.dto.server;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class V2ServerMessageWrapper {

  @JsonTypeInfo(use = Id.NAME, include = As.EXTERNAL_PROPERTY, property = "type")
  @JsonSubTypes({
    // TODO use @JsonTypeName instead of specifying the names here
    @Type(value = ChatChannelsServerMessage.class, name = ChatChannelsServerMessage.TYPE_NAME),
    @Type(value = ConnectToPeerServerMessage.class, name = ConnectToPeerServerMessage.TYPE_NAME),
    @Type(value = DisconnectPeerServerMessage.class, name = DisconnectPeerServerMessage.TYPE_NAME),
    @Type(value = ErrorServerMessage.class, name = ErrorServerMessage.TYPE_NAME),
    @Type(value = GameInfoServerMessage.class, name = GameInfoServerMessage.TYPE_NAME),
    @Type(value = GameInfosServerMessage.class, name = GameInfosServerMessage.TYPE_NAME),
    @Type(value = HostGameServerMessage.class, name = HostGameServerMessage.TYPE_NAME),
    @Type(value = InfoServerMessage.class, name = InfoServerMessage.TYPE_NAME),
    @Type(value = IceServersServerMessage.class, name = IceServersServerMessage.TYPE_NAME),
    @Type(value = AccountDetailsServerMessage.class, name = AccountDetailsServerMessage.TYPE_NAME),
    @Type(value = MatchMakerInfoServerMessage.class, name = MatchMakerInfoServerMessage.TYPE_NAME),
    @Type(value = PlayerServerMessage.class, name = PlayerServerMessage.TYPE_NAME),
    @Type(value = PlayersServerMessage.class, name = PlayersServerMessage.TYPE_NAME),
    @Type(value = PlayerOfflineServerMessage.class, name = PlayerOfflineServerMessage.TYPE_NAME),
    @Type(value = SocialRelationsServerMessage.class, name = SocialRelationsServerMessage.TYPE_NAME),
    @Type(value = StartGameProcessServerMessage.class, name = StartGameProcessServerMessage.TYPE_NAME),
    @Type(value = UpdatedAchievementsServerMessage.class, name = UpdatedAchievementsServerMessage.TYPE_NAME),
    @Type(value = MatchCreatedServerMessage.class, name = MatchCreatedServerMessage.TYPE_NAME),
    @Type(value = ConnectToHostServerMessage.class, name = ConnectToHostServerMessage.TYPE_NAME),
    @Type(value = GameResultMessage.class, name = GameResultMessage.TYPE_NAME),
  })
  private V2ServerMessage data;
}
