package org.supcomhub.server.protocol.v2.dto.server;

import lombok.Getter;
import lombok.Setter;
import org.supcomhub.server.annotations.V2ServerResponse;

/**
 * Message sent from the server to the client containing an info message to be displayed to the user.
 */
@Getter
@Setter
@V2ServerResponse
public class InfoServerMessage extends V2ServerMessage {

  public static final String TYPE_NAME = "info";

  /** The message to be displayed to the user. */
  String message;
}
