package org.supcomhub.server.protocol.v2.dto.client;


import lombok.Getter;
import org.supcomhub.server.annotations.V2ClientNotification;

/**
 * <p>
 * Message sent from the game to the server requesting to rehost the current game. The server will send a "host game"
 * message to the sending player with the same map as the current game. It's the game's responsibility to assign any
 * players to their previous slots.
 * </p>
 * <p>
 * In future, the server might also ask any participants whether they want to join the rehosted game.
 * </p>
 */
@V2ClientNotification
@Getter
public class RehostClientMessage extends V2GpgClientMessage {

  public static final String TYPE_NAME = "gpgRehost";

  public void setArgs(Object[] args) {
    // Ignored
  }
}
