package org.supcomhub.server.protocol.v2.dto.client;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.HashMap;
import java.util.Map;

/**
 * The state of a running instance of Forged Alliance.
 */
public enum PlayerGameState {
  /** There is no game process, so there is no state. */
  NONE(null),

  /** The game process is starting but has not yet started. */
  INITIALIZING("Initializing"),

  /** The game process opened the game lobby. */
  LOBBY("Lobby"),

  /**
   * The game process has left the lobby and is now launching the game and its simulation. For unknown reasons, the game
   * doesn't have a state "Playing", so every game that is "Launching" is actually playing. Hopefully, this will follow
   * in future.
   */
  LAUNCHING("Launching"),

  /**
   * The game simulation has ended but the game process is still running. Currently, the game does not send this command
   * but the client does when the process closed.
   */
  ENDED("Ended"),

  /** The game process has been closed. */
  CLOSED("Closed");

  private static final Map<String, PlayerGameState> fromProtocolValue;

  static {
    fromProtocolValue = new HashMap<>();
    for (PlayerGameState state : values()) {
      fromProtocolValue.put(state.protocolValue, state);
    }
  }

  /**
   * String as sent by the game.
   */
  private final String protocolValue;

  PlayerGameState(String protocolValue) {
    this.protocolValue = protocolValue;
  }

  @JsonValue
  public String toProtocolValue() {
    return protocolValue;
  }

  @JsonCreator
  public static PlayerGameState fromProtocolValue(String string) {
    return fromProtocolValue.get(string);
  }
}
