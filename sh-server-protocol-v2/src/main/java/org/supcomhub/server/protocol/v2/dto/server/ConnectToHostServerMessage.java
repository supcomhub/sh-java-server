package org.supcomhub.server.protocol.v2.dto.server;

import org.supcomhub.server.annotations.V2ServerResponse;

/**
 * Message sent from the server to the game telling it to connect to the host of a game.
 */
@V2ServerResponse
public class ConnectToHostServerMessage extends V2GpgServerMessage {

  public static final String TYPE_NAME = "gpgConnectToHost";

  /**
   * <ol start="0">
   * <li>(int32) The player ID of the host to connect to.</li>
   * <li>(string) The name of the host to connect to.</li>
   * </ol>
   */
  private Object[] args = new Object[2];

  public void setHostId(int hostId) {
    setValue(0, hostId);
  }

  public void setHostUsername(String hostUsername) {
    setValue(1, hostUsername);
  }

  @Override
  public Object[] getArgs() {
    return args;
  }
}
