package org.supcomhub.server.protocol.v2.dto.client;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.supcomhub.server.annotations.V2ClientRequest;
import org.supcomhub.server.protocol.v2.dto.GameVisibility;
import org.supcomhub.server.protocol.v2.dto.server.HostGameServerMessage;

import javax.validation.constraints.NotNull;

/**
 * Message sent from the client to the server to request hosting a game.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@V2ClientRequest(successResponse = HostGameServerMessage.class)
public class HostGameClientMessage extends V2ClientMessage {

  public static final String TYPE_NAME = "hostGame";

  /** The name of the map to be hosted. This is the maps directory name. The server may or may not know the map. */
  @NotNull
  private String map;

  /** The game's title. Limited to 128 characters and latin1 characters by the database. */
  @NotNull
  private String title;

  /** The technical name of the "featured mod". */
  private String mod;

  /** The password required to join the game. */
  private String password;
  /** Whether the game is visible to the public or restricted. */
  private GameVisibility visibility;
  /** The minimum rank required to join this game. */
  private Integer minRank;
  /** The maximum rank requried to join this game. */
  private Integer maxRank;
}
