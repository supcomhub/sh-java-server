package org.supcomhub.server.protocol.v2.dto.client;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.supcomhub.server.annotations.V2ClientNotification;

/**
 * Message sent from the game to the server informing it about the updated score of an army.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@V2ClientNotification
public class ArmyScoreClientMessage extends V2GpgClientMessage {

  public static final String TYPE_NAME = "gpgArmyScore";

  /**
   * <ol start="0">
   * <li>(int) The ID of the army affected by this report.</li>
   * <li>(int) The army's new score.</li>
   * </ol>
   */
  private Object[] args;

  public ArmyScoreClientMessage(int armyId, int score) {
    args = new Object[]{armyId, score};
  }

  @JsonIgnore
  public int getArmyId() {
    return getInt(0);
  }

  @JsonIgnore
  public int getScore() {
    return getInt(1);
  }
}
