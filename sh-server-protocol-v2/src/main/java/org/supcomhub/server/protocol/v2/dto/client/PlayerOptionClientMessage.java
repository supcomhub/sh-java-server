package org.supcomhub.server.protocol.v2.dto.client;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.supcomhub.server.annotations.V2ClientNotification;

/**
 * Message sent from the game to the server informing it about a changed player option.
 */
@Getter
@AllArgsConstructor
@NoArgsConstructor
@V2ClientNotification
public class PlayerOptionClientMessage extends V2GpgClientMessage {

  public static final String TYPE_NAME = "gpgPlayerOption";

  /**
   * <ol start="0">
   * <li>(int) ID of the player whose option has been changed.</li>
   * <li>(boolean) The player option's key as in the game code.</li>
   * <li>(int) The player option's value.</li>
   * </ol>
   */
  private Object[] args;

  public PlayerOptionClientMessage(int playerId, String key, String value) {
    args = new Object[]{playerId, key, value};
  }

  @JsonIgnore
  public int getPlayerId() {
    return getInt(0);
  }

  @JsonIgnore
  public String getKey() {
    return getString(1);
  }

  @JsonIgnore
  public String getValue() {
    return getString(2);
  }
}
