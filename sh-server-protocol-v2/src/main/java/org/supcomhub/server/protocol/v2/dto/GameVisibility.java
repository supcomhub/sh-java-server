package org.supcomhub.server.protocol.v2.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.HashMap;
import java.util.Map;

public enum GameVisibility {
  /** The game is visible to everyone. */
  PUBLIC("public"),
  /** The game is only visible to friends. */
  FRIENDS("friends"),
  /** The game is not visible to anyone. */
  PRIVATE("private");

  private static final Map<String, GameVisibility> byProtocolValue;

  static {
    byProtocolValue = new HashMap<>();
    for (GameVisibility visibility : values()) {
      byProtocolValue.put(visibility.protocolValue, visibility);
    }
  }

  private final String protocolValue;

  GameVisibility(String protocolValue) {
    this.protocolValue = protocolValue;
  }

  @JsonValue
  public String getProtocolValue() {
    return protocolValue;
  }

  @JsonCreator
  public static GameVisibility fromProtocolValue(String string) {
    return byProtocolValue.get(string);
  }
}
