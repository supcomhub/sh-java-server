package org.supcomhub.server.protocol.v2.dto.client;

import org.supcomhub.server.annotations.V2ClientNotification;

/**
 * Message sent from the client to the server informing it about a changed AI option.
 */
@V2ClientNotification
public class ListIceServersClientMessage extends V2ClientMessage {

  public static final String TYPE_NAME = "listIceServers";
}
