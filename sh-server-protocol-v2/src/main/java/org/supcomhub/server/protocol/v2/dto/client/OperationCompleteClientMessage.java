package org.supcomhub.server.protocol.v2.dto.client;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.supcomhub.server.annotations.V2ClientNotification;

/**
 * Message sent from the game to the server informing it about the results of a completed Co-Op mission.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@V2ClientNotification
public class OperationCompleteClientMessage extends V2GpgClientMessage {

  public static final String TYPE_NAME = "gpgOperationComplete";

  /**
   * <ol start="0">
   * <li>(boolean) Whether primary targets where completed or not.</li>
   * <li>(boolean) Whether secondary targets where completed or not.</li>
   * <li>(int) How many seconds it took to finish the mission, in seconds.</li>
   * </ol>
   */
  private Object[] args;

  public OperationCompleteClientMessage(boolean primaryTargets, boolean secondaryTargets, int time) {
    args = new Object[]{primaryTargets, secondaryTargets, time};
  }

  @JsonIgnore
  public boolean isPrimaryTargets() {
    return getBoolean(0);
  }

  @JsonIgnore
  public boolean isSecondaryTargets() {
    return getBoolean(1);
  }

  @JsonIgnore
  public int getTime() {
    return getInt(2);
  }
}
