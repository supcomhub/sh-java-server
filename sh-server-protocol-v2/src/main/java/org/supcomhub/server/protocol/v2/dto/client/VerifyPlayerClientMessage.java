package org.supcomhub.server.protocol.v2.dto.client;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.supcomhub.server.annotations.V2ClientNotification;

import java.net.URL;

/**
 * Message sent from the game to the server to verify information received from another player. If multiple players
 * report spoofed data, the server may choose to kick the player who spoofed their data.
 */
@Getter
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@V2ClientNotification
public class VerifyPlayerClientMessage extends V2GpgClientMessage {
  public static final String TYPE_NAME = "gpgVerifyPlayer";

  /**
   * <ol start="0">
   * <li>(int) The ID of the player to verify.</li>
   * <li>(string) The player's name to verify.</li>
   * <li>(byte) The player's rank to verify.</li>
   * <li>(String) The player's country to verify.</li>
   * <li>(String) The player's avatar URL to verify.</li>
   * <li>(String) The player's avatar description to verify.</li>
   * </ol>
   */
  private Object[] args;

  public int getId() {
    return getInt(0);
  }

  public String getName() {
    return getString(1);
  }

  public byte getRank() {
    return getByte(2);
  }

  public String getCountry() {
    return getString(4);
  }

  public URL getAvatarUrl() {
    return getUrl(5);
  }

  public String getAvatarDescription() {
    return getString(6);
  }
}
