# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## v2.3.1

### Added

- Add v2 protocol message `gpgDisconnected`. This was removed before since the server has no interest in this event.
However, since the game currently sends it, this message has been added but is currently ignored.
- Add v2 protocol message `gpgRehost`. This message is supposed to be handled by the client but since it might forward
it, this message has been added but is currently ignored.
- Add v2 protocol message `gpgTeamkillReport`

### Fixed

- Fix handling of incoming v2 protocol message `gpgGameEnded`
- Outgoing v2 protocol message `playerOffline` is now properly sent
- Fix automatic user creation when `sh-server.disable-authentication` is enabled
- Fix type name of incoming v2 protocol message `gpgAIOption` (was: `gpgAiOption`)
- Fix type name of incoming v2 protocol message `gpgTeamkillHappened` (was: `gpgTeamKill`)
- Fix type name of incoming v2 protocol message `gpgGameChatMessage` (was: `gpgChat`). The name is subject to change
in a future version but `gpgChat` is what the game currently sends. 
- Fix type name of incoming v2 protocol message `gpgChat` (was: `gpgGameChatMessage`). The name is subject to change
in a future version but `gpgChat` is what the game currently sends. 
- Fix type name of incoming v2 protocol message `gpgJsonStats` (was: `gpgArmyStats`). This will be reverted in future
since `gpgArmyStats` is a better name but `gpgJsonStats` is what the game currently sends
- Fix handling of `gpgGameMods`. The game currently sends two variants of this message: `uids` and `activated`. The
support for `activated` will be removed in a future version but this is what's currently implemented in the game.

### Changed

- Incoming "enforce rating" messages are now ignored. It's not for the client to decide whether a game should be rated
or not. Instead, the server will automatically enforce rating whenever a player has been defeated.
- The victim of a team kill can only report said team kill when it has previously been reported as "happened".

## v2.2.3

### Changed

- The v2 protocol `playerOffline` message now contains `"player": {"id": int, "name": string}` instead of `"playerId":
int`
- The v2 protocol Java DTO `ChatChannelsServerMessage` now contains a `List` of chat channels instead of a `Set`, with
the "main channel" at position 0

## v2.1.1

### Added
- Broadcast message `playerOffline` when a player goes offline
- Add back `public\v2-protocol.html` to `sh-server-protocol-v2.jar`

## v2.0.0

### Changed
- Switched from MySQL to Postgres
- Adapted new database schema
- Login by username is no longer supported. Instead, the email address must be used
- **BREAKING**: Matchmaker notifications now tell which pool has how many searchers
- Closing WebSocket connections has now been implemented 

### Removed
- Support for FAF's legacy protocol
- List of avatars can no longer be requested. API needs to be used instead
- A policy service is no longer supported in order do detect potentially fraudulent login attempts 

## v1.3.5
### Added
- Allow setting team presets by external matchmaking

### Changed
- Custom ICE server credentials now use long term credentials as specified in RFC 5389
- Update to Spring Boot Admin client 2.0.3
- Use long term credentials for custom ICE servers

### Fixed
- Don't require login for actuator endpoints
- Notify client about ended games

## v1.3.4
### Added
- Added player verification message
- Mark games as unranked when they have at least one AI
- The v2 protocol documentation is now available at `/v2-protocol.html`

### Changed
- Replaced UID verification with integration of the [FAF Policy Server](https://github.com/FAForever/faf-policy-server).
The environment variables `UID_ENABLED` and `UID_PRIVATE_KEY` have been removed, `POLICY_SERVICE_URL` has been added.
If no policy service URL is specified, the service is disabled.
- If ladder1v1 or coop isn't available in the database, the server will now start anyway but log warn messages.
These game modes will then not be available and an error will be sent to the client if it tries to use them.

### Fixed
- Fix missing `data` block in v2 protocol documentation
- Games reported as password protected when they were not
- Respect permanent as well as temporary bans correctly
- Don't throw error when receiving a `Disconnect` message. No behavior is implemented (because not needed) but a debug 
statement is logged
- (Player) clients can now log in using the v2 protocol
- Close open games when its host leaves
- Properly escape single quotes `'` in standard error messages
