#!/usr/bin/env bash

./ci/prepare-docker.sh

git clone -b develop --single-branch https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/supcomhub/sh-stack.git

pushd sh-stack
cp -r config.template config
cp .env.template .env
./scripts/init-core-db.sh
popd
