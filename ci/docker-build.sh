#!/usr/bin/env bash

./ci/prepare-docker.sh

CONTAINER_IMAGE=registry.gitlab.com/${CI_PROJECT_PATH}
IMAGE_VERSION=$([ -n "${CI_COMMIT_TAG}" ] && echo "${CI_COMMIT_TAG#*v}" || echo "${CI_COMMIT_REF_NAME##*/}")

pushd sh-server-app
docker pull ${CONTAINER_IMAGE}:latest || true
docker build --cache-from ${CONTAINER_IMAGE}:latest --tag ${CONTAINER_IMAGE}:${IMAGE_VERSION} --tag ${CONTAINER_IMAGE}:latest .
docker push ${CONTAINER_IMAGE}:${IMAGE_VERSION}
if [ -n "${CI_COMMIT_TAG}" ]; then docker push ${CONTAINER_IMAGE}:latest; fi
