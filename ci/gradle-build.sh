#!/usr/bin/env bash

./ci/prepare-db.sh

APP_VERSION=$([[ -n "${CI_COMMIT_TAG}" ]] && echo "${CI_COMMIT_TAG#*v}" || echo "${CI_COMMIT_REF_NAME##*/}")

./gradlew build -Pversion=${APP_VERSION} --info
