# Spring Boot based SupCom Hub Server

Implementation of the [SupCom Hub](https://www.supcomhub.org/)'s server application, often referred to as the
"lobby server".

## Technology Stack

This project uses:

* Java 11 as the programming language
* [OpenJDK 11](https://openjdk.java.net/projects/jdk/11/) as the runtime environment
* [Spring Boot](https://projects.spring.io/spring-boot/) as a framework
* [Hibernate ORM](http://hibernate.org/orm/) as ORM mapper
* [Gradle](https://gradle.org/) as a build automation tool
* [Docker](https://www.docker.com/) to deploy and run the application

## How to run

### Prerequisites

In order to run this software, you need to set up a [SH core database](https://gitlab.com/supcomhub/sh-core-db-migrations).

### From source within IntelliJ

1. Clone the repository
1. Import the project into IntelliJ as Gradle project. Doing so will delete the .idea folder which also deletes launch
configurations and code style settings. Please revert such deleted files first (Version Control (Alt+F9) -> Local
Changes)
1. Configure your JDK 10 if you haven't already
1. Make sure you have the _IntelliJ Lombok plugin_ installed
1. Launch `ShServerApplication`

### From source using the Command Line

1. Clone the repository
1. Run `.\gradlew bootRun`

### From Binary

Given the number of required configuration values, it's easiest to run the server using `sh-stack`:

    docker-compose up -d sh-server

### Note on first time Startup

The server will need to download the GeoLite2 database which is around `60mb` so
it may take a wile to start up the first time. You know the server is ready when
you see something like:
```
o.supcomhub.server.FafServerApplication  : Started FafServerApplication in 258.971 seconds
```

## Technology Stack

This project uses:

* Java 10 as the programming language
* [Spring Boot](https://projects.spring.io/spring-boot/) as a base framework
* [Spring Integration](https://projects.spring.io/spring-integration/) as a messaging framework
* [Gradle](https://gradle.org/) as a build automation tool
* [Docker](https://www.docker.com/) to deploy and run the application

## Architecture

TODO update overview to V2 protocol, check where the file is hosted

[Architecture overview](https://www.draw.io/?lightbox=1&highlight=0000ff&edit=_blank&layers=1&nav=1&title=faf-server-eip-v3.xml#Uhttps%3A%2F%2Fdrive.google.com%2Fuc%3Fid%3D0B9_8tdXfnFw2MnJjZlJ0RDZlMGc%26export%3Ddownload)

## V2 Protocol Documentation

TODO

## Learn

Learn about Spring Integration: https://www.youtube.com/watch?v=icIosLjHu3I&list=PLr2Nvl0YJxI5-QasO8XY5m8Fy34kG-YF2

