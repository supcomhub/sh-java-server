package org.supcomhub.server.security;

import lombok.experimental.UtilityClass;
import org.supcomhub.server.player.EmailAddress;

import java.util.Collections;

@UtilityClass
public class SecurityTestUtil {
  public Account createAccount() {
    Account account = new Account()
      .setPassword("pw")
      .setDisplayName("JUnit");
    account.getEmailAddresses().add(new EmailAddress("junit@example.com", "", true, account));

    return account;
  }

  public ShUserDetails createUserDetails() {
    Account account = createAccount();

    return new ShUserDetails(account, Collections.emptySet());
  }
}
