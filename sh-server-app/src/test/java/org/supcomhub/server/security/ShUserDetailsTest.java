package org.supcomhub.server.security;

import org.junit.Test;
import org.supcomhub.server.player.EmailAddress;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Collections;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class ShUserDetailsTest {

  private static final String TEST_PASSWORD = "pwd";
  private static final String TEST_EMAIL_ADDRESS = "login";

  @Test
  public void userWithoutBansIsNonLocked() {
    ShUserDetails shUserDetails = new ShUserDetails(createAccount(), Collections.emptySet());

    assertThat(shUserDetails.isAccountNonLocked(), is(true));
  }

  @Test
  public void userWithValidBansIsLockedWithExpiry() {
    Account user = createAccount();
    user.getBans().add(new Ban().setExpiresAt(Instant.now().plus(1, ChronoUnit.HOURS)));

    ShUserDetails shUserDetails = new ShUserDetails(user, Collections.emptySet());

    assertThat(shUserDetails.isAccountNonLocked(), is(false));
  }

  @Test
  public void userWithValidBanDetailsIsLockedWithoutExpiry() {
    Account user = createAccount();
    user.getBans().add(new Ban().setExpiresAt(null));

    ShUserDetails shUserDetails = new ShUserDetails(user, Collections.emptySet());

    assertThat(shUserDetails.isAccountNonLocked(), is(false));
  }

  @Test
  public void userWithExpiredBanDetailsIsNonLocked() {
    Account user = createAccount();
    user.getBans().add(new Ban().setExpiresAt(Instant.now().minusSeconds(1)));

    ShUserDetails shUserDetails = new ShUserDetails(user, Collections.emptySet());

    assertThat(shUserDetails.isAccountNonLocked(), is(true));
  }

  private Account createAccount(UserRole... roles) {
    Account user = new Account().setPassword(TEST_PASSWORD);
    user.getEmailAddresses().add(new EmailAddress().setPlain(TEST_EMAIL_ADDRESS).setPrimary(true));
    Arrays.stream(roles).forEach(userRole -> user.getUserGroups().add(new UserGroup().setTechnicalName(userRole.getString())));
    return user;
  }
}
