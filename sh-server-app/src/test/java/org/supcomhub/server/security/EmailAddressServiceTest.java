package org.supcomhub.server.security;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.supcomhub.server.player.EmailAddress;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.junit.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class EmailAddressServiceTest {

  private EmailAddressService instance;

  @Mock
  private EmailAddressRepository emailAddressRepository;

  @Before
  public void setUp() throws Exception {
    instance = new EmailAddressService(emailAddressRepository);

    when(emailAddressRepository.save(any())).thenAnswer(invocation -> invocation.getArgument(0));
  }

  @Test
  public void create() {
    Account owner = new Account();

    EmailAddress result = instance.create("j.unit@gmail.com", owner);

    assertThat(result.getDistilled(), is("junit@gmail.com"));
    assertThat(result.getPlain(), is("j.unit@gmail.com"));
    assertThat(result.getOwner(), is(owner));

    verify(emailAddressRepository).save(any());
  }

  @Test
  public void distillEmailAddress_gmail() {
    assertThat(instance.distillEmailAddress("j.un.it@gmail.com"), is("junit@gmail.com"));
    assertThat(instance.distillEmailAddress("j.un+it@gmail.com"), is("jun@gmail.com"));
    assertThat(instance.distillEmailAddress("J.Un.It+plus@gmail.com"), is("junit@gmail.com"));
  }

  @Test
  public void distillEmailAddress_regular() {
    assertThat(instance.distillEmailAddress("j.un.it@example.com"), is("j.un.it@example.com"));
    assertThat(instance.distillEmailAddress("j.un+it@example.com"), is("j.un+it@example.com"));
    assertThat(instance.distillEmailAddress("J.Un.It+plus@example.com"), is("J.Un.It+plus@example.com"));
  }

  @Test
  public void findOwnerOfAddress_found() {
    EmailAddress owner = new EmailAddress()
      .setOwner(new Account());

    when(emailAddressRepository.findAllByDistilled("junit@gmail.com"))
      .thenReturn(Optional.of(owner));

    Optional<Account> result = instance.findOwnerOfAddress("j.unit@gmail.com");

    assertThat(result.isPresent(), is(true));
    assertThat(result.get(), is(owner));
    verify(emailAddressRepository).findAllByDistilled("junit@gmail.com");
  }

  @Test
  public void findOwnerOfAddress_notFound() {
    when(emailAddressRepository.findAllByDistilled(any()))
      .thenReturn(Optional.empty());

    Optional<Account> ownerOfAddress = instance.findOwnerOfAddress("j.unit@gmail.com");

    assertThat(ownerOfAddress.isPresent(), is(false));
    verify(emailAddressRepository).findAllByDistilled("junit@gmail.com");
  }

  @Test
  public void emailExists() {
    instance.emailExists("j.unit@gmail.com");
    verify(emailAddressRepository).existsByDistilled("junit@gmail.com");
  }

  @Test
  public void findAll() {
    instance.findAll();
    verify(emailAddressRepository).findAll();
  }
}
