package org.supcomhub.server.client;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.supcomhub.server.ShServerApplication.ApplicationShutdownEvent;
import org.supcomhub.server.api.dto.AchievementState;
import org.supcomhub.server.api.dto.UpdatedAchievementResponse;
import org.supcomhub.server.avatar.Avatar;
import org.supcomhub.server.clan.Clan;
import org.supcomhub.server.common.ServerMessage;
import org.supcomhub.server.config.ServerProperties;
import org.supcomhub.server.game.Game;
import org.supcomhub.server.game.HostGameResponse;
import org.supcomhub.server.game.LobbyMode;
import org.supcomhub.server.game.StartGameProcessResponse;
import org.supcomhub.server.ice.ForwardedIceMessage;
import org.supcomhub.server.ice.IceServer;
import org.supcomhub.server.ice.IceServerList;
import org.supcomhub.server.integration.ClientGateway;
import org.supcomhub.server.integration.Protocol;
import org.supcomhub.server.player.PlayerOfflineResponse;
import org.supcomhub.server.player.PlayerResponse;
import org.supcomhub.server.rating.Rating;
import org.supcomhub.server.security.Account;
import org.supcomhub.server.social.SocialRelationListResponse;

import java.net.InetAddress;
import java.net.URI;
import java.net.URL;
import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import static java.util.Collections.emptyList;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class ClientServiceTest {

  private ClientService instance;

  @Mock
  private ClientGateway clientGateway;

  @Captor
  private ArgumentCaptor<ServerMessage> serverMessageCaptor;

  private ClientConnection clientConnection;
  private Account player;
  private ServerProperties serverProperties;

  @Before
  public void setUp() throws Exception {
    serverProperties = new ServerProperties();
    clientConnection = new ClientConnection("1", Protocol.V2_JSON_UTF_8, mock(InetAddress.class));
    player = new Account().setClientConnection(clientConnection);
    player.setDisplayName("playerLogin");

    instance = new ClientService(clientGateway, serverProperties);
  }

  @Test
  public void connectToPeer() {
    Account peer = new Account();
    peer.setId(2);
    peer.setDisplayName("test");

    instance.connectToPeer(player, peer, false);

    ArgumentCaptor<ConnectToPeerResponse> captor = ArgumentCaptor.forClass(ConnectToPeerResponse.class);
    verify(clientGateway).send(captor.capture(), eq(clientConnection));
    ConnectToPeerResponse response = captor.getValue();

    assertThat(response.getPlayerId(), is(peer.getId()));
    assertThat(response.getPlayerName(), is(peer.getDisplayName()));
    assertThat(response.isOffer(), is(false));
  }

  @Test
  public void startGameProcess() {
    Game game = new Game(1)
      .setFeaturedModName("sh")
      .setMapFolderName("scmp_001")
      .setLobbyMode(LobbyMode.DEFAULT);

    instance.startGameProcess(game, player);

    ArgumentCaptor<StartGameProcessResponse> captor = ArgumentCaptor.forClass(StartGameProcessResponse.class);
    verify(clientGateway).send(captor.capture(), any());

    StartGameProcessResponse message = captor.getValue();
    assertThat(message.getGameId(), is(1));
    assertThat(message.getLobbyMode(), is(LobbyMode.DEFAULT));
    assertThat(message.getMod(), is("sh"));
    assertThat(message.getMapFolderName(), is("scmp_001"));
  }

  @Test
  public void hostGame() {
    Game game = new Game(1).setMapFolderName("SCMP_001");

    instance.hostGame(game, player);

    ArgumentCaptor<HostGameResponse> captor = ArgumentCaptor.forClass(HostGameResponse.class);
    verify(clientGateway).send(captor.capture(), any());

    assertThat(captor.getValue().getMapFilename(), is("SCMP_001"));
  }

  @Test
  public void reportUpdatedAchievements() {
    List<UpdatedAchievementResponse> list = Collections.singletonList(new UpdatedAchievementResponse("1", "1", null, AchievementState.UNLOCKED, true));

    instance.reportUpdatedAchievements(list, player);

    ArgumentCaptor<UpdatedAchievementsResponse> captor = ArgumentCaptor.forClass(UpdatedAchievementsResponse.class);
    verify(clientGateway).send(captor.capture(), any());

    assertThat(captor.getValue().getUpdatedAchievements(), hasSize(1));
    assertThat(captor.getValue().getUpdatedAchievements().get(0).getCurrentState(), is(AchievementState.UNLOCKED));
    assertThat(captor.getValue().getUpdatedAchievements().get(0).getCurrentSteps(), is(nullValue()));
  }

  @Test
  public void sendUserDetails() throws Exception {
    Avatar avatar = new Avatar().setUrl(new URL("http://example.com")).setDescription("Tooltip");
    player
      .setAvatar(avatar)
      .setClan(new Clan().setTag("FOO"))
      .setNumberOfGames(12)
      .setCountry("CH")
      .setDisplayName("JUnit")
      .setId(5);
    player.getRatings().put(1, new Rating(1100d, 100d, 1, player,3 ));
    player.getRatings().put(2, new Rating(900d, 50d, 2, player,5));

    instance.sendLoginDetails(player, player);

    ArgumentCaptor<PlayerResponse> captor = ArgumentCaptor.forClass(PlayerResponse.class);
    verify(clientGateway).send(captor.capture(), any());

    PlayerResponse response = captor.getValue();
    assertThat(response.getId(), is(5));
    assertThat(response.getDisplayName(), is("JUnit"));
    assertThat(response.getAvatar().getDescription(), is("Tooltip"));
    assertThat(response.getAvatar().getUrl(), is(new URL("http://example.com")));
    assertThat(response.getRanks().get(1), is(3));
    assertThat(response.getRanks().get(2), is(5));
    assertThat(response.getNumberOfGames(), is(12));
    assertThat(response.getClanTag(), is("FOO"));
    assertThat(response.getCountry(), is("CH"));
  }

  @Test
  public void disconnectPlayerSendsToAllPlayersInGame() {
    List<Account> recipients = Arrays.asList(player, new Account(), new Account(), new Account());

    instance.disconnectPlayerFromGame(12, recipients);

    verify(clientGateway, times(4)).send(any(DisconnectPlayerFromGameResponse.class), any());
  }

  @Test
  public void sendOnlinePlayerList() throws Exception {
    List<Account> players = Arrays.asList(
      (Account) new Account().setDisplayName("JUnit").setCountry("CH").setId(1),
      (Account) new Account().setDisplayName("JUnit").setCountry("CH").setId(2)
    );
    ConnectionAware connectionAware = new Account().setClientConnection(clientConnection);

    CompletableFuture<PlayerResponses> sent = new CompletableFuture<>();
    doAnswer(invocation -> sent.complete(invocation.getArgument(0)))
      .when(clientGateway).send(any(PlayerResponses.class), eq(clientConnection));

    instance.sendPlayerInformation(players, connectionAware);

    PlayerResponses responses = sent.get(10, TimeUnit.SECONDS);
    assertThat(responses.getResponses(), hasSize(2));

    Iterator<PlayerResponse> iterator = responses.getResponses().iterator();
    assertThat(iterator.next().getId(), is(1));
    assertThat(iterator.next().getId(), is(2));
  }

  @Test
  public void sendIceServers() {
    List<IceServerList> iceServers = Collections.singletonList(
      new IceServerList(60, Instant.now(), Arrays.asList(
        new IceServer(URI.create("turn:test1"), null, null, null),
        new IceServer(URI.create("turn:test2"), "username", "credential", "credentialType")
      ))
    );
    ConnectionAware connectionAware = new Account().setClientConnection(clientConnection);

    instance.sendIceServers(iceServers, connectionAware);

    verify(clientGateway).send(new IceServersResponse(iceServers), clientConnection);
  }

  @Test
  public void sendIceMessage() {
    instance.sendIceMessage(1, Collections.emptyMap(), clientConnection);

    verify(clientGateway).send(new ForwardedIceMessage(1, Collections.emptyMap()), clientConnection);
  }

  @Test
  public void sendSocialRelations() {
    SocialRelationListResponse response = new SocialRelationListResponse(emptyList());
    instance.sendSocialRelations(response, clientConnection);

    verify(clientGateway).send(response, clientConnection);
  }

  @Test
  public void onServerShutdown() {
    serverProperties.getShutdown().setMessage("Shutdown test message");
    instance.onServerShutdown(ApplicationShutdownEvent.INSTANCE);

    ArgumentCaptor<InfoResponse> captor = ArgumentCaptor.forClass(InfoResponse.class);
    verify(clientGateway).broadcast(captor.capture());

    InfoResponse value = captor.getValue();
    assertThat(value.getMessage(), is("Shutdown test message"));
  }

  @Test
  public void onServerShutdownExceptionDoesntPropagate() {
    doThrow(new RuntimeException("This exception should be logged but not thrown"))
      .when(clientGateway).broadcast(any());

    instance.onServerShutdown(ApplicationShutdownEvent.INSTANCE);

    verify(clientGateway).broadcast(any());
    // Expect no exception to be thrown
  }

  @Test
  public void connectToHost() {
    Game game = new Game().setHost((Account) new Account().setDisplayName("JUnit").setId(1));

    instance.connectToHost(player, game);

    verify(clientGateway).send(new ConnectToHostResponse("JUnit", 1), clientConnection);
  }

  @Test
  public void broadcastPlayerInformation() {
    instance.broadcastMinDelay = Duration.ofMinutes(-1);

    instance.broadcastPlayerInformation(Arrays.asList(
      (Account) new Account().setId(1),
      (Account) new Account().setId(2),
      (Account) new Account().setId(3)
    ));
    instance.broadcastDelayedResponses();

    verify(clientGateway).broadcast(serverMessageCaptor.capture());
    ServerMessage message = serverMessageCaptor.getValue();

    assertThat(message, instanceOf(PlayerResponses.class));
  }

  @Test
  public void broadcastPlayerOffline() {
    instance.broadcastPlayerOffline((Account) new Account().setDisplayName("JUnit").setId(123));
    verify(clientGateway).broadcast(PlayerOfflineResponse.of(123, "JUnit"));
  }
}
