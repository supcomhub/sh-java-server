package org.supcomhub.server.client;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.supcomhub.server.integration.Protocol;
import org.supcomhub.server.security.Account;
import org.supcomhub.server.security.SecurityTestUtil;

import java.net.InetAddress;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class ConnectionServiceActivatorTest {

  private ConnectionServiceActivator instance;

  @Mock
  private ClientConnectionService clientConnectionService;

  private ClientConnection clientConnection;
  private Account user;

  @Before
  public void setUp() throws Exception {
    user = SecurityTestUtil.createAccount();

    clientConnection = new ClientConnection("1", Protocol.V2_JSON_UTF_8, mock(InetAddress.class))
      .setAuthentication(new TestingAuthenticationToken(user, null));

    instance = new ConnectionServiceActivator(clientConnectionService);
  }

  @Test
  public void disconnectClientRequest() {
    DisconnectClientRequest request = new DisconnectClientRequest(1);

    instance.disconnectClientRequest(request, clientConnection);

    verify(clientConnectionService).disconnectClient(new TestingAuthenticationToken(user, null), 1);
  }
}
