package org.supcomhub.server.user;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.supcomhub.server.security.Ban;
import org.supcomhub.server.security.Ban.BanScope;
import org.supcomhub.server.security.BanDetailsRepository;
import org.supcomhub.server.security.BanDetailsService;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class BanServiceTest {

  private BanDetailsService instance;

  @Mock
  private BanDetailsRepository banDetailsRepository;

  @Before
  public void setUp() throws Exception {
    instance = new BanDetailsService(banDetailsRepository);
  }

  @Test
  public void banUserGlobal() {
    instance.banUser(1, 2, BanScope.GLOBAL, "JUnit");

    ArgumentCaptor<Ban> captor = ArgumentCaptor.forClass(Ban.class);
    verify(banDetailsRepository).save(captor.capture());

    Ban ban = captor.getValue();
    assertThat(ban.getUser().getId(), is(1));
    assertThat(ban.getAuthor().getId(), is(2));
    assertThat(ban.getReason(), is("JUnit"));
    assertThat(ban.getExpiresAt(), is(nullValue()));
    assertThat(ban.getScope(), is(BanScope.GLOBAL));
  }

  @Test
  public void banUserChat() {
    instance.banUser(1, 2, BanScope.CHAT, "JUnit");

    ArgumentCaptor<Ban> captor = ArgumentCaptor.forClass(Ban.class);
    verify(banDetailsRepository).save(captor.capture());

    Ban ban = captor.getValue();
    assertThat(ban.getUser().getId(), is(1));
    assertThat(ban.getAuthor().getId(), is(2));
    assertThat(ban.getReason(), is("JUnit"));
    assertThat(ban.getExpiresAt(), is(nullValue()));
    assertThat(ban.getScope(), is(BanScope.CHAT));
  }
}
