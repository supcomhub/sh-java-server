package org.supcomhub.server.social;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.supcomhub.server.client.ClientConnection;
import org.supcomhub.server.integration.Protocol;
import org.supcomhub.server.security.Account;
import org.supcomhub.server.security.SecurityTestUtil;
import org.supcomhub.server.security.ShUserDetails;

import java.net.InetAddress;
import java.util.Collections;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.supcomhub.server.social.UpdateSocialRelationRequest.Operation.ADD;
import static org.supcomhub.server.social.UpdateSocialRelationRequest.Operation.REMOVE;
import static org.supcomhub.server.social.UpdateSocialRelationRequest.RelationType.FOE;
import static org.supcomhub.server.social.UpdateSocialRelationRequest.RelationType.FRIEND;

@RunWith(MockitoJUnitRunner.class)
public class SocialServiceActivatorsTest {
  private SocialServiceActivators instance;

  @Mock
  private SocialService socialService;

  private ClientConnection clientConnection;
  private Account player;

  @Before
  public void setUp() throws Exception {
    player = (Account) SecurityTestUtil.createAccount().setId(1);

    clientConnection = new ClientConnection("1", Protocol.V2_JSON_UTF_8, mock(InetAddress.class));
    clientConnection.setAuthentication(new TestingAuthenticationToken(new ShUserDetails(player, Collections.emptySet()), null));

    instance = new SocialServiceActivators(socialService);
  }

  @Test
  public void addFriend() {
    instance.updateSocialRelation(new UpdateSocialRelationRequest(10, ADD, FRIEND), clientConnection.getAuthentication());
    verify(socialService).updateSocialRelation(player, ADD, FRIEND, 10);
  }

  @Test
  public void addFoe() {
    instance.updateSocialRelation(new UpdateSocialRelationRequest(10, ADD, FOE), clientConnection.getAuthentication());
    verify(socialService).updateSocialRelation(player, ADD, FOE, 10);
  }

  @Test
  public void removeFriend() {
    instance.updateSocialRelation(new UpdateSocialRelationRequest(10, REMOVE, FRIEND), clientConnection.getAuthentication());
    verify(socialService).updateSocialRelation(player, REMOVE, FRIEND, 10);
  }

  @Test
  public void removeFoe() {
    instance.updateSocialRelation(new UpdateSocialRelationRequest(10, REMOVE, FOE), clientConnection.getAuthentication());
    verify(socialService).updateSocialRelation(player, REMOVE, FOE, 10);
  }
}
