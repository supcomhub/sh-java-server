package org.supcomhub.server.social;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.supcomhub.server.client.ClientService;
import org.supcomhub.server.player.PlayerOnlineEvent;
import org.supcomhub.server.player.SocialRelation;
import org.supcomhub.server.player.SocialRelationStatus;
import org.supcomhub.server.security.Account;
import org.supcomhub.server.security.SecurityTestUtil;
import org.supcomhub.server.security.ShUserDetails;
import org.supcomhub.server.social.SocialRelationListResponse.SocialRelationResponse;
import org.supcomhub.server.social.SocialRelationListResponse.SocialRelationResponse.RelationType;

import java.util.Arrays;
import java.util.Collections;

import static java.util.Collections.emptySet;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.supcomhub.server.social.UpdateSocialRelationRequest.Operation.ADD;
import static org.supcomhub.server.social.UpdateSocialRelationRequest.Operation.REMOVE;
import static org.supcomhub.server.social.UpdateSocialRelationRequest.RelationType.FOE;
import static org.supcomhub.server.social.UpdateSocialRelationRequest.RelationType.FRIEND;

@RunWith(MockitoJUnitRunner.class)
public class SocialServiceTest {

  private SocialService instance;

  @Mock
  private SocialRelationRepository socialRelationRepository;
  @Mock
  private ClientService clientService;
  private Account requester;

  @Captor
  private ArgumentCaptor<SocialRelation> captor;

  @Before
  public void setUp() throws Exception {
    requester = (Account) new Account().setId(1);

    instance = new SocialService(socialRelationRepository, clientService);
  }

  @Test
  public void addFriend() {
    instance.updateSocialRelation(requester, ADD, FRIEND, 5);

    verify(socialRelationRepository).deleteByFromIdAndToIdAndStatus(requester.getId(), 5, SocialRelationStatus.FOE);
    verify(socialRelationRepository).save(captor.capture());
    SocialRelation socialRelation = captor.getValue();

    assertThat(socialRelation.getStatus(), is(SocialRelationStatus.FRIEND));
    assertThat(socialRelation.getToId(), is(5));
    assertThat(socialRelation.getFromId(), is(requester.getId()));
  }

  @Test
  public void addFoe() {
    instance.updateSocialRelation(requester, ADD, FOE, 5);

    verify(socialRelationRepository).deleteByFromIdAndToIdAndStatus(requester.getId(), 5, SocialRelationStatus.FRIEND);
    verify(socialRelationRepository).save(captor.capture());
    SocialRelation socialRelation = captor.getValue();

    assertThat(socialRelation.getStatus(), is(SocialRelationStatus.FOE));
    assertThat(socialRelation.getToId(), is(5));
    assertThat(socialRelation.getFromId(), is(requester.getId()));
  }

  @Test
  public void removeFriend() {
    instance.updateSocialRelation(requester, REMOVE, FRIEND, 5);
    verify(socialRelationRepository).deleteByFromIdAndToIdAndStatus(requester.getId(), 5, SocialRelationStatus.FRIEND);
  }

  @Test
  public void removeFoe() {
    instance.updateSocialRelation(requester, REMOVE, FOE, 5);
    verify(socialRelationRepository).deleteByFromIdAndToIdAndStatus(requester.getId(), 5, SocialRelationStatus.FOE);
  }

  @Test
  public void onAuthenticationSuccess() {
    Account user = SecurityTestUtil.createAccount();

    ShUserDetails userDetails = new ShUserDetails(user, emptySet());

    when(socialRelationRepository.findAllByPlayer(user)).thenReturn(Arrays.asList(
      new SocialRelation(1, null, 10, SocialRelationStatus.FRIEND),
      new SocialRelation(2, null, 11, SocialRelationStatus.FOE)
    ));

    instance.onPlayerOnlineEvent(new PlayerOnlineEvent(this, userDetails.getPlayer()));

    ArgumentCaptor<SocialRelationListResponse> captor = ArgumentCaptor.forClass(SocialRelationListResponse.class);
    verify(clientService).sendSocialRelations(captor.capture(), any());

    SocialRelationListResponse response = captor.getValue();
    assertThat(response, instanceOf(SocialRelationListResponse.class));

    assertThat(response.getSocialRelations(), hasSize(2));
    assertThat(response.getSocialRelations().get(0), is(new SocialRelationResponse(10, RelationType.FRIEND)));
    assertThat(response.getSocialRelations().get(1), is(new SocialRelationResponse(11, RelationType.FOE)));
  }

  @Test
  public void onAuthenticationSuccessNoSocialRelations() {
    Account user = SecurityTestUtil.createAccount();

    ShUserDetails userDetails = new ShUserDetails(user, emptySet());

    when(socialRelationRepository.findAllByPlayer(user)).thenReturn(Collections.emptyList());
    instance.onPlayerOnlineEvent(new PlayerOnlineEvent(this, userDetails.getPlayer()));

    verify(clientService, never()).sendSocialRelations(any(), any());
  }
}
