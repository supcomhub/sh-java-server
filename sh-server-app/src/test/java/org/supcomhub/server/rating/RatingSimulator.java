package org.supcomhub.server.rating;

import org.supcomhub.server.config.ServerProperties;
import org.supcomhub.server.game.Game;
import org.supcomhub.server.game.GamePlayerStats;
import org.supcomhub.server.game.GameService;
import org.supcomhub.server.security.Account;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class RatingSimulator {
  public static void main(String[] args) {
    RatingService ratingService = new RatingService(new ServerProperties());

    int featuredModId = 1;

    List<Account> players = IntStream.range(0, 10_000)
      .mapToObj(playerId -> {
        Account player = (Account) new Account().setId(playerId);
        ratingService.initializeRating(player, featuredModId);
        return player;
      }).collect(Collectors.toList());

    IntStream.range(0, 1_000_000).forEach(gameId -> {
      Game game = new Game(gameId);

      Account player1, player2;
      do {
        player1 = players.get((int) (Math.random() * players.size()));
        player2 = players.get((int) (Math.random() * players.size()));
      } while (player1 == player2);

      Collection<GamePlayerStats> playerStats = Arrays.asList(
        new GamePlayerStats(game, player1)
          .setRatingMeanBefore(player1.getRatings().get(featuredModId).getMean())
          .setRatingDeviationBefore(player1.getRatings().get(featuredModId).getDeviation())
          .setScore(player1.getId() > player2.getId() ? 10 : -1)
          .setTeam(GameService.NO_TEAM_ID),
        new GamePlayerStats(game, player2)
          .setRatingMeanBefore(player2.getRatings().get(featuredModId).getMean())
          .setRatingDeviationBefore(player2.getRatings().get(featuredModId).getDeviation())
          .setScore(player2.getId() > player1.getId() ? 10 : -1)
          .setTeam(GameService.NO_TEAM_ID)
      );

      ratingService.updateRatings(playerStats, GameService.NO_TEAM_ID, featuredModId);
    });

    players.stream()
      .filter(player -> player.getId() % 10 == 0)
      .forEach(player -> System.out.printf("Player %d: %s, rating: %f%n",
        player.getId(),
        player.getRatings().get(featuredModId),
        player.getRatings().get(featuredModId).getMean() - 3 * player.getRatings().get(featuredModId).getDeviation())
      );
  }
}
