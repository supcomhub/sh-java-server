package org.supcomhub.server.rating;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.supcomhub.server.config.ServerProperties;
import org.supcomhub.server.game.GamePlayerStats;
import org.supcomhub.server.game.GameService;
import org.supcomhub.server.security.Account;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.collection.IsEmptyCollection.empty;
import static org.junit.Assert.assertThat;

public class RatingServiceTest {
  private static final int NO_TEAM_ID = GameService.NO_TEAM_ID;
  private RatingService instance;

  @Before
  public void setUp() throws Exception {
    instance = new RatingService(new ServerProperties());
  }

  @Test
  public void getInitialRating() {
    Account player = new Account();
    assertThat(player.getRatings().entrySet(), is(empty()));

    instance.initializeRating(player, 1);

    assertThat(player.getRatings().get(1), is(notNullValue()));
    assertThat(player.getRatings().get(1).getMean(), is(Matchers.greaterThan(0d)));
    assertThat(player.getRatings().get(1).getDeviation(), is(Matchers.greaterThan(0d)));
  }

  @Test
  public void updateRatings() {
    int featuredModId = 1;

    Account player1 = new Account();
    player1.getRatings().put(featuredModId, new Rating().setMean(1500d).setDeviation(500d));
    player1.setId(1);

    Account player2 = new Account();
    player2.getRatings().put(featuredModId, new Rating().setMean(1500d).setDeviation(500d));
    player2.setId(2);

    List<GamePlayerStats> playerStats = Arrays.asList(
      new GamePlayerStats()
        .setPlayer(player1)
        .setTeam(NO_TEAM_ID)
        .setRatingMeanBefore(player1.getRatings().get(featuredModId).getMean())
        .setRatingDeviationBefore(player1.getRatings().get(featuredModId).getDeviation())
        .setScore(10),
      new GamePlayerStats()
        .setPlayer(player2)
        .setTeam(NO_TEAM_ID)
        .setRatingMeanBefore(player2.getRatings().get(featuredModId).getMean())
        .setRatingDeviationBefore(player2.getRatings().get(featuredModId).getDeviation())
        .setScore(-1)
    );

    instance.updateRatings(playerStats, NO_TEAM_ID, 1);

    assertThat(player1.getRatings().get(featuredModId).getMean(), is(1765.511882354831));
    assertThat(player1.getRatings().get(featuredModId).getDeviation(), is(429.1918779825801));

    assertThat(player2.getRatings().get(featuredModId).getMean(), is(1234.4881176451688));
    assertThat(player2.getRatings().get(featuredModId).getDeviation(), is(429.1918779825801));
  }

  @Test
  public void calculateQuality() {
    Rating left = new Rating().setMean(1600d).setDeviation(30d);
    Rating right = new Rating().setMean(900d).setDeviation(160d);

    double quality = instance.calculateQuality(left, right);

    assertThat(quality, is(0.16000885216755253));
  }

  @Test
  public void calculateQualityDefaultForNull() {
    double quality = instance.calculateQuality(null, null);

    assertThat(quality, is(0.4327310675847713));
  }
}
