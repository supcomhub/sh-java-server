package org.supcomhub.server.map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.supcomhub.server.config.ServerProperties;
import org.supcomhub.server.mod.FeaturedMod;
import org.supcomhub.server.mod.ModService;
import org.supcomhub.server.security.Account;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.isOneOf;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MapServiceTest {

  private static final String MAP_NAME = "SCMP_001";
  private static final int LADDER_1V1_MOD_Id = 1;

  @Mock
  private MapVersionRepository mapVersionRepository;
  @Mock
  private Ladder1v1MapRepository ladder1v1MapRepository;
  @Mock
  private ModService modService;

  private MapVersion mapVersion;
  private MapService instance;

  @Before
  public void setUp() {
    mapVersion = (MapVersion) new MapVersion().setFilename(MAP_NAME).setId(1);
    when(mapVersionRepository.findByFilenameIgnoreCase(MAP_NAME + MapService.MAP_FILENAME_EXTENSION)).thenReturn(Optional.of(mapVersion));

    FeaturedMod ladder = new FeaturedMod();
    ladder.setId(LADDER_1V1_MOD_Id);
    when(modService.getLadder1v1Mod()).thenReturn(Optional.of(ladder));

    ServerProperties serverProperties = new ServerProperties();
    instance = new MapService(mapVersionRepository, ladder1v1MapRepository, serverProperties, modService);
  }

  @Test
  public void mapIsFoundByName() {
    assertThat(instance.findMap(MAP_NAME).get(), is(mapVersion));
  }

  @Test
  public void getRandomLadderMapForSmallLadderMapPool() {
    MapVersion oneMap = (MapVersion) new MapVersion().setRanked(true).setId(2);
    MapVersion otherMap = (MapVersion) new MapVersion().setRanked(true).setId(3);
    when(ladder1v1MapRepository.findAll()).thenReturn(Arrays.asList(oneMap, otherMap));
    Page pagePlayerOne = mock(Page.class);
    when(pagePlayerOne.getContent()).thenReturn(Arrays.asList(oneMap, otherMap));
    Page pagePlayerTwo = mock(Page.class);
    when(pagePlayerTwo.getContent()).thenReturn(Arrays.asList(oneMap, otherMap));
    doReturn(pagePlayerOne).when(ladder1v1MapRepository).findRecentlyPlayedLadderMapVersions(any(), eq(1), eq(LADDER_1V1_MOD_Id));
    doReturn(pagePlayerTwo).when(ladder1v1MapRepository).findRecentlyPlayedLadderMapVersions(any(), eq(2), eq(LADDER_1V1_MOD_Id));

    Account host = new Account();
    host.setId(1);
    Account opponent = new Account();
    opponent.setId(2);

    assertThat(instance.getRandomLadderMap(List.of(host, opponent)), isOneOf(oneMap, otherMap));
  }

  @Test
  public void getRandomLadderMapForBiggerLadderMapPool() {
    MapVersion oneMap = (MapVersion) new MapVersion().setRanked(true).setId(2);
    MapVersion otherMap = (MapVersion) new MapVersion().setRanked(true).setId(3);
    MapVersion firstPlayerFirstPlayedMap = (MapVersion) new MapVersion().setRanked(true).setId(4);
    MapVersion firstPlayerSecondPlayedMap = (MapVersion) new MapVersion().setRanked(true).setId(5);
    MapVersion secondPlayerFirstPlayedMap = (MapVersion) new MapVersion().setRanked(true).setId(6);
    MapVersion secondPlayerSecondPlayedMap = (MapVersion) new MapVersion().setRanked(true).setId(7);

    when(ladder1v1MapRepository.findAll()).thenReturn(Arrays.asList(oneMap, otherMap, firstPlayerFirstPlayedMap, firstPlayerSecondPlayedMap, secondPlayerFirstPlayedMap, secondPlayerSecondPlayedMap));

    Page pagePlayerOne = mock(Page.class);
    when(pagePlayerOne.getContent()).thenReturn(Arrays.asList(firstPlayerFirstPlayedMap, firstPlayerSecondPlayedMap));
    Page pagePlayerTwo = mock(Page.class);
    when(pagePlayerTwo.getContent()).thenReturn(Arrays.asList(secondPlayerFirstPlayedMap, secondPlayerSecondPlayedMap));
    doReturn(pagePlayerOne).when(ladder1v1MapRepository).findRecentlyPlayedLadderMapVersions(any(), eq(1), eq(LADDER_1V1_MOD_Id));
    doReturn(pagePlayerTwo).when(ladder1v1MapRepository).findRecentlyPlayedLadderMapVersions(any(), eq(2), eq(LADDER_1V1_MOD_Id));

    Account host = new Account();
    host.setId(1);
    Account opponent = new Account();
    opponent.setId(2);

    assertThat(instance.getRandomLadderMap(List.of(host, opponent)), isOneOf(oneMap, otherMap));
  }
}
