package org.supcomhub.server.player;

import io.micrometer.core.instrument.MeterRegistry;
import org.jetbrains.annotations.NotNull;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.context.ApplicationEventPublisher;
import org.supcomhub.server.client.ClientConnection;
import org.supcomhub.server.client.ClientService;
import org.supcomhub.server.geoip.GeoIpService;
import org.supcomhub.server.integration.Protocol;
import org.supcomhub.server.security.Account;
import org.supcomhub.server.security.SecurityTestUtil;
import org.supcomhub.server.security.ShUserDetails;

import java.net.InetAddress;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TimeZone;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PlayerServiceTest {
  private PlayerService instance;
  private Account player;

  @Mock
  private ClientService clientService;
  @Mock
  private MeterRegistry meterRegistry;
  @Mock
  private ApplicationEventPublisher eventPublisher;
  @Mock
  private GeoIpService geoIpService;
  private OnlinePlayerRepository onlinePlayerRepository;

  @Before
  public void setUp() throws Exception {
    onlinePlayerRepository = new FakeOnlineAccountRepository();
    player = (Account) SecurityTestUtil.createAccount().setId(1);
    instance = new PlayerService(clientService, meterRegistry, onlinePlayerRepository, eventPublisher, geoIpService);
  }

  @Test
  public void onClientDisconnectRemovesPlayerAndUnsetsGameAndRemovesGameIfLastPlayer() {
    ShUserDetails shUserDetails = createShUserDetails();

    when(geoIpService.lookupTimezone(any())).thenReturn(Optional.of(TimeZone.getDefault()));
    when(geoIpService.lookupCountryCode(any())).thenReturn(Optional.of("CH"));

    instance.setPlayerOnline(shUserDetails.getPlayer());
    assertThat(instance.getOnlinePlayer(player.getId()).isPresent(), is(true));
    assertThat(player.getTimeZone(), is(TimeZone.getDefault()));
    assertThat(player.getCountry(), is("CH"));

    instance.removePlayer(shUserDetails.getPlayer());

    assertThat(instance.getOnlinePlayer(player.getId()).isPresent(), is(false));
  }

  @Test
  public void removePlayerBroadcastsPlayerOffline() {
    Account player = createShUserDetails().getPlayer();
    onlinePlayerRepository.save(player);

    instance.removePlayer(player);

    verify(clientService).broadcastPlayerOffline(player);
  }

  @Test
  public void isPlayerOnline() {
    ShUserDetails shUserDetails = createShUserDetails();

    assertThat(instance.isPlayerOnline(shUserDetails.getPlayer().getDisplayName()), is(false));
    instance.setPlayerOnline(shUserDetails.getPlayer());
    assertThat(instance.isPlayerOnline(shUserDetails.getPlayer().getDisplayName()), is(true));
  }

  private ShUserDetails createShUserDetails() {
    player.setClientConnection(new ClientConnection("1", Protocol.V2_JSON_UTF_8, mock(InetAddress.class)));

    return new ShUserDetails(player, Collections.emptySet());
  }

  private class FakeOnlineAccountRepository implements OnlinePlayerRepository {
    private final Map<Integer, Account> players;

    private FakeOnlineAccountRepository() {
      players = new HashMap<>();
    }

    @NotNull
    @Override
    public <S extends Account> S save(@NotNull S entity) {
      players.put(entity.getId(), entity);
      return entity;
    }

    @NotNull
    @Override
    public <S extends Account> Iterable<S> saveAll(@NotNull Iterable<S> entities) {
      entities.forEach(s -> players.put(s.getId(), s));
      return entities;
    }

    @NotNull
    @Override
    public Optional<Account> findById(@NotNull Integer integer) {
      return Optional.ofNullable(players.get(integer));
    }

    @Override
    public boolean existsById(@NotNull Integer integer) {
      return players.containsKey(integer);
    }

    @NotNull
    @Override
    public Collection<Account> findAll() {
      return players.values();
    }

    @NotNull
    @Override
    public Iterable<Account> findAllById(@NotNull Iterable<Integer> integers) {
      Set<Integer> ids = StreamSupport.stream(integers.spliterator(), false)
        .collect(Collectors.toSet());

      return players.values().stream()
        .filter(player -> ids.contains(player.getId()))
        .collect(Collectors.toList());
    }

    @Override
    public long count() {
      return players.size();
    }

    @Override
    public void deleteById(@NotNull Integer integer) {
      players.remove(integer);
    }

    @Override
    public void delete(@NotNull Account entity) {
      players.remove(entity.getId());
    }

    @Override
    public void deleteAll(@NotNull Iterable<? extends Account> entities) {
      for (Account entity : entities) {
        players.remove(entity.getId());
      }
    }

    @Override
    public void deleteAll() {
      players.clear();
    }

    @Override
    public Optional<Account> findByDisplayName(String displayName) {
      return players.values().stream()
        .filter(player1 -> player1.getDisplayName().equals(displayName))
        .findFirst();
    }

    @Override
    public List<Account> findAllByCountry(String country) {
      return players.values().stream().filter(player -> player.getCountry() != null).collect(Collectors.toList());
    }
  }

}
