package org.supcomhub.server.game;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.supcomhub.server.avatar.Avatar;
import org.supcomhub.server.avatar.AvatarService;
import org.supcomhub.server.client.ClientService;
import org.supcomhub.server.error.ErrorCode;
import org.supcomhub.server.error.RequestExceptionWithCode;
import org.supcomhub.server.player.PlayerService;
import org.supcomhub.server.rating.Rating;
import org.supcomhub.server.security.Account;

import java.net.URL;
import java.util.Collection;
import java.util.Optional;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SpoofDetectorServiceTest {

  @Rule
  public final ExpectedException expectedException = ExpectedException.none();

  private SpoofDetectorService instance;

  @Mock
  private PlayerService playerService;
  @Mock
  private ClientService clientService;
  @Mock
  private AvatarService avatarService;

  @Before
  public void setUp() throws Exception {
    instance = new SpoofDetectorService(playerService, clientService, avatarService);
  }

  @Test
  public void verifyPlayerReporterNotInGame() {
    expectedException.expect(RequestExceptionWithCode.requestExceptionWithCode(ErrorCode.THIS_PLAYER_NOT_IN_GAME));

    instance.verifyPlayer(new Account(), 1, "", 1, null, null, null);
    verifyZeroInteractions(clientService);
  }

  @Test
  public void verifyPlayerPlayerNotOnline() {
    when(playerService.getOnlinePlayer(1)).thenReturn(Optional.empty());
    expectedException.expect(RequestExceptionWithCode.requestExceptionWithCode(ErrorCode.PLAYER_NOT_ONLINE));

    instance.verifyPlayer(new Account().setCurrentGame(new Game()), 1, "", 1, null, null, null);
    verifyZeroInteractions(clientService);
  }

  @Test
  public void verifyPlayerReporteeNotInGame() {
    when(playerService.getOnlinePlayer(1)).thenReturn(Optional.of(new Account()));
    expectedException.expect(RequestExceptionWithCode.requestExceptionWithCode(ErrorCode.OTHER_PLAYER_NOT_IN_GAME));

    instance.verifyPlayer(new Account().setCurrentGame(new Game()), 1, "", 1, null, null, null);
    verifyZeroInteractions(clientService);
  }

  @Test
  public void verifyPlayerReporterAndReporteeNotInSameGame() {
    when(playerService.getOnlinePlayer(1)).thenReturn(Optional.of(new Account().setCurrentGame(new Game(1))));
    expectedException.expect(RequestExceptionWithCode.requestExceptionWithCode(ErrorCode.NOT_SAME_GAME));

    instance.verifyPlayer(new Account().setCurrentGame(new Game(2)), 1, "", 1, null, null, null);
    verifyZeroInteractions(clientService);
  }

  @Test
  public void verifyPlayerReporterNameMismatch() {
    Account reportee = (Account) new Account()
      .setCurrentGame(new Game(1))
      .setDisplayName("JUnit")
      .setId(1);
    when(playerService.getOnlinePlayer(1)).thenReturn(Optional.of(reportee));

    Account reporter = (Account) new Account().setCurrentGame(new Game(1)).setId(42);
    instance.verifyPlayer(reporter, reportee.getId(), "Fraud", 1, null, null, null);

    assertThat(reportee.getFraudReporterIds(), hasSize(1));
    assertThat(reportee.getFraudReporterIds(), Matchers.contains(42));

    verifyZeroInteractions(clientService);
  }

  @Test
  public void verifyPlayerRankMismatch() {
    Account reportee = (Account) new Account()
      .setCurrentGame(new Game(1))
      .setDisplayName("JUnit")
      .setId(1);
    reportee.setRatingWithinCurrentGame(new Rating(51, 5, 0, reportee, 3));

    when(playerService.getOnlinePlayer(1)).thenReturn(Optional.of(reportee));

    Account reporter = (Account) new Account().setCurrentGame(new Game(1)).setId(42);
    instance.verifyPlayer(reporter, reportee.getId(), "JUnit", 5, null, null, null);

    assertThat(reportee.getFraudReporterIds(), hasSize(1));
    assertThat(reportee.getFraudReporterIds(), Matchers.contains(42));

    verifyZeroInteractions(clientService);
  }

  @Test
  public void verifyPlayerCountry() {
    Account reportee = (Account) new Account()
      .setCurrentGame(new Game(1))
      .setCountry("CH")
      .setDisplayName("JUnit")
      .setId(1);
    reportee.setRatingWithinCurrentGame(new Rating(50, 5, 0, reportee, 3));

    when(playerService.getOnlinePlayer(1)).thenReturn(Optional.of(reportee));

    Account reporter = (Account) new Account().setCurrentGame(new Game(1)).setId(42);
    instance.verifyPlayer(reporter, reportee.getId(), "JUnit", 3, "XX", null, null);

    assertThat(reportee.getFraudReporterIds(), hasSize(1));
    assertThat(reportee.getFraudReporterIds(), Matchers.contains(42));

    verifyZeroInteractions(clientService);
  }

  @Test
  public void verifyPlayerAvatarDescription() {
    Account reportee = (Account) new Account()
      .setCurrentGame(new Game(1))
      .setCountry("CH")
      .setDisplayName("JUnit")
      .setId(1);
    reportee.setRatingWithinCurrentGame(new Rating(50, 5, 0, reportee, 3));

    when(avatarService.getCurrentAvatar(reportee)).thenReturn(Optional.of(new Avatar().setDescription("Description")));
    when(playerService.getOnlinePlayer(1)).thenReturn(Optional.of(reportee));

    Account reporter = (Account) new Account().setCurrentGame(new Game(1)).setId(42);
    instance.verifyPlayer(reporter, reportee.getId(), "JUnit", 3, "CH", null, "Fake");

    assertThat(reportee.getFraudReporterIds(), hasSize(1));
    assertThat(reportee.getFraudReporterIds(), Matchers.contains(42));

    verifyZeroInteractions(clientService);
  }

  @Test
  public void verifyPlayerAvatarUrl() throws Exception {
    Account reportee = (Account) new Account()
      .setCurrentGame(new Game(1))
      .setCountry("CH")
      .setDisplayName("JUnit")
      .setId(1);
    reportee.setRatingWithinCurrentGame(new Rating(50, 5, 0, reportee, 3));

    when(avatarService.getCurrentAvatar(reportee)).thenReturn(Optional.of(new Avatar().setUrl(new URL("http://example.com/avatar.png"))));
    when(playerService.getOnlinePlayer(1)).thenReturn(Optional.of(reportee));

    Account reporter = (Account) new Account().setCurrentGame(new Game(1)).setId(42);
    instance.verifyPlayer(reporter, reportee.getId(), "JUnit", 3, "CH", new URL("http://example.com/fake.png"), null);

    assertThat(reportee.getFraudReporterIds(), hasSize(1));
    assertThat(reportee.getFraudReporterIds(), Matchers.contains(42));

    verifyZeroInteractions(clientService);
  }

  @Test
  public void verifyPlayerAllGood() throws Exception {
    Account reportee = (Account) new Account()
      .setCurrentGame(new Game(1))
      .setCountry("CH")
      .setDisplayName("JUnit")
      .setId(1);
    reportee.setRatingWithinCurrentGame(new Rating(50, 5, 0, reportee, 3));

    when(avatarService.getCurrentAvatar(reportee))
      .thenReturn(Optional.of(new Avatar().setUrl(new URL("http://example.com/avatar.png")).setDescription("Description")));
    when(playerService.getOnlinePlayer(1)).thenReturn(Optional.of(reportee));

    Account reporter = (Account) new Account().setCurrentGame(new Game(1)).setId(42);
    instance.verifyPlayer(reporter, reportee.getId(), "JUnit", 3, "CH", new URL("http://example.com/avatar.png"), "Description");

    assertThat(reportee.getFraudReporterIds(), empty());

    verifyZeroInteractions(clientService);
  }

  @Test
  public void verifyPlayerFakeNameAndMultipleReports() throws Exception {
    Game reporteeGame = new Game(1);

    Account reportee = (Account) new Account()
      .setCurrentGame(reporteeGame)
      .setCountry("CH")
      .setDisplayName("JUnit")
      .setId(1);
    reportee.setRatingWithinCurrentGame(new Rating(50, 5, 0, reportee, 3));

    when(playerService.getOnlinePlayer(1)).thenReturn(Optional.of(reportee));

    Account reporter1 = (Account) new Account().setCurrentGame(reporteeGame).setId(42);
    Account reporter2 = (Account) new Account().setCurrentGame(reporteeGame).setId(43);

    reporteeGame.getConnectedPlayers().put(reportee.getId(), reportee);
    reporteeGame.getConnectedPlayers().put(reporter1.getId(), reporter1);
    reporteeGame.getConnectedPlayers().put(reporter2.getId(), reporter2);

    instance.verifyPlayer(reporter1, reportee.getId(), "Fake", 3, "CH", new URL("http://example.com/avatar.png"), "Description");
    instance.verifyPlayer(reporter2, reportee.getId(), "Fake", 3, "CH", new URL("http://example.com/avatar.png"), "Description");

    assertThat(reportee.getFraudReporterIds(), hasSize(2));
    assertThat(reportee.getFraudReporterIds(), containsInAnyOrder(42, 43));

    @SuppressWarnings("unchecked")
    ArgumentCaptor<Collection<Account>> argumentCaptor = ArgumentCaptor.forClass(Collection.class);
    verify(clientService).disconnectPlayerFromGame(eq(reportee.getId()), argumentCaptor.capture());

    assertThat(argumentCaptor.getValue(), containsInAnyOrder(reporter1, reporter2));
  }
}
