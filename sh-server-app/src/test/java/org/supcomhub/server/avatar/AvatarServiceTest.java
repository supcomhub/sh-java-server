package org.supcomhub.server.avatar;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.supcomhub.server.error.ErrorCode;
import org.supcomhub.server.security.Account;

import static org.mockito.Mockito.verify;
import static org.supcomhub.server.error.RequestExceptionWithCode.requestExceptionWithCode;

@RunWith(MockitoJUnitRunner.class)
public class AvatarServiceTest {

  @Rule
  public ExpectedException expectedException = ExpectedException.none();
  @Mock
  private AvatarAssociationRepository avatarAssociationRepository;

  private Account player;
  private AvatarService instance;

  @Before
  public void setUp() throws Exception {
    player = (Account) new Account().setId(1);

    instance = new AvatarService(avatarAssociationRepository);
  }

  @Test
  public void selectAvatarById() {
    instance.selectAvatar(player, 1);

    verify(avatarAssociationRepository).selectAvatar(1, 1);
  }

  @Test
  public void selectAvatarNoId() {
    expectedException.expect(requestExceptionWithCode(ErrorCode.MISSING_AVATAR_ID));

    instance.selectAvatar(player, null);
  }
}
