package org.supcomhub.server.teamkill;

import org.hamcrest.core.Is;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.supcomhub.server.error.ErrorCode;
import org.supcomhub.server.error.RequestExceptionWithCode;
import org.supcomhub.server.game.Game;
import org.supcomhub.server.game.GameClosedEvent;
import org.supcomhub.server.game.TeamKill;
import org.supcomhub.server.player.PlayerService;
import org.supcomhub.server.security.Account;

import java.time.Duration;
import java.util.Optional;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.anyInt;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
// TODO verify logger calls
public class TeamKillServiceTest {

  @Rule
  public ExpectedException expectedException = ExpectedException.none();

  @Mock
  private PlayerService playerService;
  @Mock
  private TeamKillRepository teamKillRepository;

  private TeamKillService instance;

  @Before
  public void setUp() throws Exception {
    when(playerService.getOnlinePlayer(anyInt())).thenReturn(Optional.empty());

    instance = new TeamKillService(playerService, teamKillRepository);
  }

  @Test
  public void reportTeamKill() {
    Game game = new Game();
    game.setId(10);

    Account reporter = (Account) new Account().setCurrentGame(game).setId(1);
    Account killer = (Account) new Account().setId(2);

    when(playerService.getOnlinePlayer(killer.getId())).thenReturn(Optional.of(killer));

    Duration gameTime = Duration.ofMinutes(28);
    instance.onTeamKillHappened(reporter, gameTime, killer.getId());
    instance.reportTeamKill(reporter, gameTime, killer.getId());

    ArgumentCaptor<TeamKill> captor = ArgumentCaptor.forClass(TeamKill.class);
    verify(teamKillRepository).save(captor.capture());
    TeamKill teamKill = captor.getValue();

    assertThat(teamKill.getGameId(), is(game.getId()));
    assertThat(teamKill.getTeamKiller(), Is.is(killer.getId()));
    assertThat(teamKill.getVictim(), Is.is(reporter.getId()));
    assertThat(teamKill.getGameTime(), is((int) gameTime.getSeconds()));
  }

  @Test
  public void reportTeamKillWithoutGame() {
    Account player = (Account) new Account().setId(1);
    Account killer = (Account) new Account().setId(2);

    instance.reportTeamKill(player, Duration.ofMinutes(28), killer.getId());
    verifyZeroInteractions(teamKillRepository);
  }

  @Test
  public void reportTeamKillWhenKillerDoesntExist() {
    Account reporter = new Account();
    reporter.setCurrentGame((Game) new Game().setId(9));
    reporter.setId(1);

    Duration gameTime = Duration.ofMinutes(28);

    instance.onTeamKillHappened(reporter, gameTime, 991234);
    instance.reportTeamKill(reporter, gameTime, 991234);
    verifyZeroInteractions(teamKillRepository);
  }

  @Test
  public void reportTeamKillWhenTeamKillDidnHappen() {
    Account reporter = new Account();
    reporter.setCurrentGame((Game) new Game().setId(9));
    reporter.setId(1);

    Account killer = (Account) new Account().setId(2);

    Duration gameTime = Duration.ofMinutes(28);

    expectedException.expect(RequestExceptionWithCode.requestExceptionWithCode(ErrorCode.TEAM_KILL_NOT_REPORTABLE));
    instance.reportTeamKill(reporter, gameTime, killer.getId());

    verifyZeroInteractions(teamKillRepository);
  }

  @Test
  public void onGameClosedMakesTeamKillUnreportable() {
    Game game = new Game();
    game.setId(10);

    Account reporter = (Account) new Account().setCurrentGame(game).setId(1);
    Account killer = (Account) new Account().setId(2);

    Duration gameTime = Duration.ofMinutes(28);
    instance.onTeamKillHappened(reporter, gameTime, killer.getId());
    instance.onGameClosed(new GameClosedEvent(game.getId()));

    expectedException.expect(RequestExceptionWithCode.requestExceptionWithCode(ErrorCode.TEAM_KILL_NOT_REPORTABLE));
    instance.reportTeamKill(reporter, gameTime, killer.getId());
  }
}
