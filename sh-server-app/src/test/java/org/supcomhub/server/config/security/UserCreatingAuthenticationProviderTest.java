package org.supcomhub.server.config.security;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.supcomhub.server.player.AccountRepository;
import org.supcomhub.server.player.EmailAddress;
import org.supcomhub.server.security.Account;
import org.supcomhub.server.security.EmailAddressService;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserCreatingAuthenticationProviderTest {

  private static final String PLAIN_EMAIL_ADDRESS = "junit@example.com";
  private UserCreatingAuthenticationProvider instance;

  @Mock
  private AccountRepository accountRepository;
  @Mock
  private EmailAddressService emailAddressService;
  @Mock
  private PasswordEncoder passwordEncoder;

  @Before
  public void setUp() throws Exception {
    instance = new UserCreatingAuthenticationProvider(accountRepository, emailAddressService, passwordEncoder);

    when(accountRepository.save(any())).thenAnswer(invocation -> ((Account) invocation.getArgument(0))
      .setPassword("password")
      .setId(1));
    when(emailAddressService.create(any(), any())).thenAnswer(invocation -> new EmailAddress(
      invocation.getArgument(0),
      invocation.getArgument(0),
      true,
      invocation.getArgument(1)
    ));
  }

  @Test
  @SuppressWarnings("unchecked")
  public void test() {
    UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(PLAIN_EMAIL_ADDRESS, "password");

    instance.retrieveUser(PLAIN_EMAIL_ADDRESS, authentication);

    ArgumentCaptor<Account> userCaptor = ArgumentCaptor.forClass(Account.class);
    verify(accountRepository).save(userCaptor.capture());

    Account user = userCaptor.getValue();
    assertThat(user.getPrimaryEmailAddress().getPlain(), is(authentication.getPrincipal()));
    assertThat(user.getPassword(), is(authentication.getCredentials()));
    assertThat(user.getPrimaryEmailAddress().getPlain(), is(PLAIN_EMAIL_ADDRESS));
  }
}
