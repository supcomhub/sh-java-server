package org.supcomhub.server.integration.v2.server;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mapstruct.factory.Mappers;
import org.mockito.junit.MockitoJUnitRunner;
import org.supcomhub.server.api.dto.AchievementState;
import org.supcomhub.server.chat.JoinChatChannelResponse;
import org.supcomhub.server.client.ConnectToPeerResponse;
import org.supcomhub.server.client.DisconnectPlayerFromGameResponse;
import org.supcomhub.server.client.IceServersResponse;
import org.supcomhub.server.client.InfoResponse;
import org.supcomhub.server.client.UpdatedAchievementsResponse;
import org.supcomhub.server.client.UpdatedAchievementsResponse.UpdatedAchievement;
import org.supcomhub.server.config.JacksonConfig;
import org.supcomhub.server.error.ErrorCode;
import org.supcomhub.server.error.ErrorResponse;
import org.supcomhub.server.game.Faction;
import org.supcomhub.server.game.GameResponse;
import org.supcomhub.server.game.GameResponse.FeaturedMod;
import org.supcomhub.server.game.GameResponse.GameParticipant;
import org.supcomhub.server.game.GameResponse.SimMod;
import org.supcomhub.server.game.GameState;
import org.supcomhub.server.game.GameVisibility;
import org.supcomhub.server.game.HostGameResponse;
import org.supcomhub.server.game.LobbyMode;
import org.supcomhub.server.game.StartGameProcessResponse;
import org.supcomhub.server.ice.IceServer;
import org.supcomhub.server.ice.IceServerList;
import org.supcomhub.server.matchmaker.MatchMakerResponse;
import org.supcomhub.server.player.LoginDetailsResponse;
import org.supcomhub.server.player.PlayerResponse;
import org.supcomhub.server.player.PlayerResponse.Avatar;
import org.supcomhub.server.social.SocialRelationListResponse;
import org.supcomhub.server.social.SocialRelationListResponse.SocialRelationResponse;
import org.supcomhub.server.social.SocialRelationListResponse.SocialRelationResponse.RelationType;

import java.net.URI;
import java.net.URL;
import java.time.Instant;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;

import static com.spotify.hamcrest.jackson.JsonMatchers.isJsonStringMatching;
import static com.spotify.hamcrest.jackson.JsonMatchers.jsonArray;
import static com.spotify.hamcrest.jackson.JsonMatchers.jsonBoolean;
import static com.spotify.hamcrest.jackson.JsonMatchers.jsonInt;
import static com.spotify.hamcrest.jackson.JsonMatchers.jsonLong;
import static com.spotify.hamcrest.jackson.JsonMatchers.jsonObject;
import static com.spotify.hamcrest.jackson.JsonMatchers.jsonText;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;

@RunWith(MockitoJUnitRunner.class)
public class V2ServerMessageTransformerTest {

  private V2ServerMessageTransformer instance;

  @Before
  public void setUp() throws Exception {
    ObjectMapper objectMapper = new JacksonConfig().objectMapper();

    instance = new V2ServerMessageTransformer(objectMapper, Mappers.getMapper(V2ServerMessageMapper.class));
  }

  @Test
  public void infoResponse() {
    String response = instance.transform(new InfoResponse("Hello JUnit"));
    assertThat(response, isJsonStringMatching(
      jsonObject()
        .where("type", is(jsonText("info")))
        .where("data", is(
          jsonObject()
            .where("message", is(jsonText("Hello JUnit")))))));
  }

  @Test
  public void hostGame() {
    String response = instance.transform(new HostGameResponse("scmp01"));
    assertThat(response, isJsonStringMatching(
      jsonObject()
        .where("type", is(jsonText("hostGame")))
        .where("data", is(
          jsonObject()
            .where("mapName", is(jsonText("scmp01")))))));
  }

  @Test
  public void joinChatChannel() {
    String response = instance.transform(new JoinChatChannelResponse(Arrays.asList("#one", "#two")));
    assertThat(response, isJsonStringMatching(
      jsonObject()
        .where("type", is(jsonText("chatChannels")))
        .where("data", is(
          jsonObject().where("channels", is(jsonArray(contains(jsonText("#one"), jsonText("#two")))))))));
  }

  @Test
  @SuppressWarnings("unchecked")
  public void connectToPeer() {
    String response = instance.transform(new ConnectToPeerResponse("junit", 123, false));
    assertThat(response, isJsonStringMatching(
      jsonObject()
        .where("type", is(jsonText("gpgConnectToPeer")))
        .where("data", is(
          jsonObject()
            .where("args", is(jsonArray(contains(
              jsonInt(123),
              jsonText("junit"),
              jsonBoolean(false)
            ))))))));
  }

  @Test
  public void disconnectFromPlayer() {
    String response = instance.transform(new DisconnectPlayerFromGameResponse(123));
    assertThat(response, isJsonStringMatching(
      jsonObject()
        .where("type", is(jsonText("gpgDisconnectFromPeer")))
        .where("data", is(
          jsonObject()
            .where("args", is(
              jsonArray(contains(jsonInt(123))))
            )))));

  }

  @Test
  public void game() {
    Instant startTime = Instant.parse("2007-12-03T10:15:30.00Z");

    String response = instance.transform(new GameResponse(1, "Title", GameVisibility.PUBLIC, false, GameState.ENDED,
        new FeaturedMod("sh", 1),
        Arrays.asList(
          new SimMod(UUID.fromString("00000000-0000-0000-0000-000000000001"), "Mod #1"),
          new SimMod(UUID.fromString("00000000-0000-0000-0000-000000000002"), "Mod #2")
        ),
        "scmp01",
        new GameParticipant(4, "JUnit4", 3),
        Arrays.asList(
          new GameParticipant(1, "JUnit4", 1),
          new GameParticipant(2, "JUnit5", 1)
        ),
        12,
        startTime,
        5,
        8,
        9
      )
    );

    assertThat(response, isJsonStringMatching(
      jsonObject()
        .where("type", is(jsonText("game")))
        .where("data", is(
          jsonObject()
            .where("id", is(jsonInt(1)))
            .where("title", is(jsonText("Title")))
            .where("gameVisibility", is(jsonText("public")))
            .where("passwordProtected", is(jsonBoolean(false)))
            .where("state", is(jsonText("ENDED")))
            .where("mod", is(jsonObject()
              .where("name", is(jsonText("sh")))
              .where("version", is(jsonInt(1)))))
            .where("leaderboardId", is(jsonInt(9)))
            .where("simMods", is(jsonArray(contains(
              jsonObject()
                .where("uuid", is(jsonText("00000000-0000-0000-0000-000000000001")))
                .where("displayName", is(jsonText("Mod #1"))),
              jsonObject()
                .where("uuid", is(jsonText("00000000-0000-0000-0000-000000000002")))
                .where("displayName", is(jsonText("Mod #2")))
            ))))
            .where("map", is(jsonText("scmp01")))
            .where("host", is(jsonObject()
              .where("name", is(jsonText("JUnit4")))
              .where("team", is(jsonInt(3)))
              .where("id", is(jsonInt(4)))))
            .where("players", is(jsonArray(contains(
              jsonObject()
                .where("id", is(jsonInt(1)))
                .where("team", is(jsonInt(1))),
              jsonObject()
                .where("id", is(jsonInt(2)))
                .where("team", is(jsonInt(1)))))))
            .where("maxPlayers", is(jsonInt(12)))
            .where("startTime", is(jsonLong(1196676930000L)))
            .where("minRank", is(jsonInt(5)))
            .where("maxRank", is(jsonInt(8)))
        ))));
  }

  @Test
  public void iceServers() {
    Instant createdAt = Instant.parse("2007-12-03T10:15:30.00Z");

    String response = instance.transform(new IceServersResponse(Collections.singletonList(
      new IceServerList(3600, createdAt, Collections.singletonList(new IceServer(
        URI.create("http://localhost"),
        "anonymous",
        "123",
        "token"
      )))
    )));
    assertThat(response, isJsonStringMatching(
      jsonObject()
        .where("type", is(jsonText("iceServers")))
        .where("data", is(
          jsonObject()
            .where("iceServerLists", is(jsonArray(contains(
              jsonObject()
                .where("ttlSeconds", is(jsonInt(3600)))
                .where("createdAt", is(jsonLong(1196676930000L)))
                .where("servers", is(jsonArray(contains(
                  jsonObject()
                    .where("url", is(jsonText("http://localhost")))
                    .where("username", is(jsonText("anonymous")))
                    .where("credential", is(jsonText("123")))
                    .where("credentialType", is(jsonText("token")))
                ))))
            ))))
        ))));
  }

  @Test
  public void loginDetails() throws Exception {
    String response = instance.transform(new LoginDetailsResponse(new PlayerResponse(
      1,
      "A",
      "CH",
      TimeZone.getTimeZone("Europe/Berlin"),
      ImmutableMap.of(
        3, 5,
        9, 3
      ),
      new Avatar(new URL("http://localhost/avatar.png"), "Avatar"),
      "AA",
      12
    )));
    assertThat(response, isJsonStringMatching(
      jsonObject()
        .where("type", is(jsonText("loginDetails")))
        .where("data", is(
          jsonObject()
            .where("id", is(jsonInt(1)))
            .where("displayName", is(jsonText("A")))
            .where("country", is(jsonText("CH")))
            .where("timeZone", is(jsonText("Europe/Berlin")))
            .where("ranks", is(
              jsonObject()
                .where("3", is(jsonInt(5)))
                .where("9", is(jsonInt(3)))
            ))
            .where("numberOfGames", is(jsonInt(12)))
            .where("avatar", is(
              jsonObject()
                .where("url", is(jsonText("http://localhost/avatar.png")))
                .where("description", is(jsonText("Avatar")))
            ))
            .where("clanTag", is(jsonText("AA")))
        ))));
  }

  @Test
  public void matchAvailable() {
    String response = instance.transform(new MatchMakerResponse(Map.of("ladder2v2", 5)));
    assertThat(response, isJsonStringMatching(
      jsonObject()
        .where("type", is(jsonText("matchAvailable")))
        .where("data", is(
          jsonObject()
            .where("playersByPool", is(jsonObject()
              .where("ladder2v2", is(jsonInt(5)))
            ))
        ))));
  }

  @Test
  public void socialRelationList() {
    String response = instance.transform(new SocialRelationListResponse(Arrays.asList(
      new SocialRelationResponse(1, RelationType.FRIEND),
      new SocialRelationResponse(2, RelationType.FOE)
    )));
    assertThat(response, isJsonStringMatching(
      jsonObject()
        .where("type", is(jsonText("socialRelations")))
        .where("data", is(
          jsonObject()
            .where("socialRelations", is(jsonArray(contains(
              jsonObject()
                .where("accountId", is(jsonInt(1)))
                .where("type", is(jsonText("FRIEND"))),
              jsonObject()
                .where("accountId", is(jsonInt(2)))
                .where("type", is(jsonText("FOE")))
            ))))
        ))));
  }

  @Test
  public void startGameProcessWithoutMap() {
    String response = instance.transform(new StartGameProcessResponse("sh", 1, null,
      LobbyMode.DEFAULT, Faction.UEF, "someName", 2, 1, 3));
    assertThat(response, isJsonStringMatching(
      jsonObject()
        .where("type", is(jsonText("startGameProcess")))
        .where("data", is(
          jsonObject()
            .where("mod", is(jsonText("sh")))
            .where("gameId", is(jsonInt(1)))
            .where("lobbyMode", is(jsonText("DEFAULT")))
            .where("faction", is(jsonText("uef")))
            .where("name", is(jsonText("someName")))
            .where("expectedPlayers", is(jsonInt(2)))
            .where("team", is(jsonInt(1)))
            .where("mapPosition", is(jsonInt(3)))
        ))));
  }

  @Test
  public void startGameProcessWithMap() {
    String response = instance.transform(new StartGameProcessResponse("sh", 1, "scmp01",
      LobbyMode.DEFAULT, null, "someName", null, 0, null));
    assertThat(response, isJsonStringMatching(
      jsonObject()
        .where("type", is(jsonText("startGameProcess")))
        .where("data", is(
          jsonObject()
            .where("mod", is(jsonText("sh")))
            .where("gameId", is(jsonInt(1)))
            .where("lobbyMode", is(jsonText("DEFAULT")))
            .where("map", is(jsonText("scmp01")))
            .where("name", is(jsonText("someName")))
            .where("team", is(jsonInt(0)))
        ))));
  }

  @Test
  public void updatedAchievements() {
    String response = instance.transform(new UpdatedAchievementsResponse(Arrays.asList(
      new UpdatedAchievement("111", 2, AchievementState.REVEALED, false),
      new UpdatedAchievement("111", 2, AchievementState.REVEALED, false)
    )));
    assertThat(response, isJsonStringMatching(
      jsonObject()
        .where("type", is(jsonText("updatedAchievements")))
        .where("data", is(
          jsonObject()
            .where("updatedAchievements", is(jsonArray(contains(
              jsonObject()
                .where("achievementId", is(jsonText("111")))
                .where("currentSteps", is(jsonInt(2)))
                .where("currentState", is(jsonText("REVEALED")))
                .where("newlyUnlocked", is(jsonBoolean(false))),
              jsonObject()
                .where("achievementId", is(jsonText("111")))
                .where("currentSteps", is(jsonInt(2)))
                .where("currentState", is(jsonText("REVEALED")))
                .where("newlyUnlocked", is(jsonBoolean(false)))
            ))))
        ))));
  }

  @Test
  public void player() throws Exception {
    String response = instance.transform(new PlayerResponse(
      1,
      "A",
      "CH",
      TimeZone.getTimeZone("Europe/Berlin"),
      ImmutableMap.of(
        3, 5,
        9, 3
      ),
      new Avatar(new URL("http://localhost/avatar.png"), "Avatar"),
      "AA",
      12
    ));
    assertThat(response, isJsonStringMatching(
      jsonObject()
        .where("type", is(jsonText("player")))
        .where("data", is(
          jsonObject()
            .where("id", is(jsonInt(1)))
            .where("displayName", is(jsonText("A")))
            .where("country", is(jsonText("CH")))
            .where("timeZone", is(jsonText("Europe/Berlin")))
            .where("ranks", is(
              jsonObject()
                .where("3", is(jsonInt(5)))
                .where("9", is(jsonInt(3)))
            ))
            .where("numberOfGames", is(jsonInt(12)))
            .where("avatar", is(
              jsonObject()
                .where("url", is(jsonText("http://localhost/avatar.png")))
                .where("description", is(jsonText("Avatar")))
            ))
            .where("clanTag", is(jsonText("AA")))
        ))));
  }

  @Test
  public void errorMessage() {
    UUID requestId = UUID.fromString("fa41225f-d818-495f-8843-d421949d5968");
    String response = instance.transform(new ErrorResponse(ErrorCode.UNSUPPORTED_REQUEST, requestId, new String[]{"{\"foo\": \"bar\"}", "JUnit Test"}));
    assertThat(response, isJsonStringMatching(
      jsonObject()
        .where("type", is(jsonText("error")))
        .where("data", is(
          jsonObject()
            .where("code", is(jsonInt(109)))
            .where("title", is(jsonText("Unsupported request")))
            .where("text", is(jsonText("The server received an unsupported request from your client: {\"foo\": \"bar\"}. Cause: JUnit Test")))
            .where("requestId", is(jsonText("fa41225f-d818-495f-8843-d421949d5968")))
            .where("args", is(jsonArray(contains(
              jsonText("{\"foo\": \"bar\"}"),
              jsonText("JUnit Test")
            ))))
        ))));
  }
}
