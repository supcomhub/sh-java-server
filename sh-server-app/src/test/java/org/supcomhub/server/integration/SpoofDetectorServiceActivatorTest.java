package org.supcomhub.server.integration;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.supcomhub.server.client.ClientConnection;
import org.supcomhub.server.game.SpoofDetectorService;
import org.supcomhub.server.game.VerifyPlayerReport;
import org.supcomhub.server.security.Account;
import org.supcomhub.server.security.SecurityTestUtil;
import org.supcomhub.server.security.ShUserDetails;

import java.net.InetAddress;
import java.net.URL;
import java.util.Collections;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class SpoofDetectorServiceActivatorTest {

  private SpoofDetectorServiceActivator instance;
  private ClientConnection clientConnection;
  private Account reporter;

  @Mock
  private SpoofDetectorService spoofDetectorService;

  @Before
  public void setUp() throws Exception {
    clientConnection = new ClientConnection("1", Protocol.V2_JSON_UTF_8, mock(InetAddress.class));
    reporter = SecurityTestUtil.createAccount();

    clientConnection.setAuthentication(new TestingAuthenticationToken(new ShUserDetails(reporter, Collections.emptySet()), null));

    instance = new SpoofDetectorServiceActivator(spoofDetectorService);
  }

  @Test
  public void verifyPlayerReport() throws Exception {
    instance.verifyPlayerReport(new VerifyPlayerReport(
      123, "JUnit", 3, "CH", new URL("http://example.com/avatar.png"), "Avatar Description"
    ), clientConnection.getAuthentication());

    verify(spoofDetectorService).verifyPlayer(reporter, 123, "JUnit", 3, "CH", new URL("http://example.com/avatar.png"), "Avatar Description");
  }
}
