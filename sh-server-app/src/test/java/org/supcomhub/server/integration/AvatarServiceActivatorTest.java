package org.supcomhub.server.integration;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.supcomhub.server.avatar.AvatarService;
import org.supcomhub.server.avatar.SelectAvatarRequest;
import org.supcomhub.server.security.Account;
import org.supcomhub.server.security.SecurityTestUtil;
import org.supcomhub.server.security.ShUserDetails;

import java.util.Collections;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class AvatarServiceActivatorTest {

  private AvatarServiceActivator instance;

  @Mock
  private AvatarService avatarService;
  private TestingAuthenticationToken authentication;
  private Account player;

  @Before
  public void setUp() throws Exception {
    player = SecurityTestUtil.createAccount();

    authentication = new TestingAuthenticationToken(new ShUserDetails(player, Collections.emptySet()), null);

    instance = new AvatarServiceActivator(avatarService);
  }

  @Test
  public void selectAvatarByUrl() {
    SelectAvatarRequest request = new SelectAvatarRequest(null);

    instance.selectAvatar(request, authentication);

    verify(avatarService).selectAvatar(player, request.getAvatarId());
  }

  @Test
  public void selectAvatarById() {
    SelectAvatarRequest request = new SelectAvatarRequest(1);

    instance.selectAvatar(request, authentication);

    verify(avatarService).selectAvatar(player, 1);
  }
}
