package org.supcomhub.server.integration;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.supcomhub.server.client.ClientConnection;
import org.supcomhub.server.client.ClientDisconnectedEvent;
import org.supcomhub.server.game.AiOptionReport;
import org.supcomhub.server.game.ArmyOutcomeReport;
import org.supcomhub.server.game.ArmyScoreReport;
import org.supcomhub.server.game.ClearSlotRequest;
import org.supcomhub.server.game.DesyncReport;
import org.supcomhub.server.game.DisconnectPeerRequest;
import org.supcomhub.server.game.GameEndedReport;
import org.supcomhub.server.game.GameModsReport;
import org.supcomhub.server.game.GameOptionReport;
import org.supcomhub.server.game.GameService;
import org.supcomhub.server.game.GameStateReport;
import org.supcomhub.server.game.GameVisibility;
import org.supcomhub.server.game.HostGameRequest;
import org.supcomhub.server.game.JoinGameRequest;
import org.supcomhub.server.game.LobbyMode;
import org.supcomhub.server.game.MutuallyAgreedDrawRequest;
import org.supcomhub.server.game.Outcome;
import org.supcomhub.server.game.PlayerDisconnectedReport;
import org.supcomhub.server.game.PlayerGameState;
import org.supcomhub.server.game.PlayerOptionReport;
import org.supcomhub.server.game.RestoreGameSessionRequest;
import org.supcomhub.server.security.Account;
import org.supcomhub.server.security.SecurityTestUtil;
import org.supcomhub.server.security.ShUserDetails;
import org.supcomhub.server.stats.ArmyStatisticsReport;

import java.net.InetAddress;
import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;
import java.util.UUID;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class GameServiceActivatorsTest {

  private GameServiceActivators instance;

  @Mock
  private GameService gameService;
  private ClientConnection clientConnection;
  private Account player;

  @Before
  public void setUp() throws Exception {
    clientConnection = new ClientConnection("1", Protocol.V2_JSON_UTF_8, mock(InetAddress.class));
    player = SecurityTestUtil.createAccount();

    clientConnection.setAuthentication(new TestingAuthenticationToken(new ShUserDetails(player, Collections.emptySet()), null));

    instance = new GameServiceActivators(gameService);
  }

  @Test
  public void disconnectFromGame() {
    instance.disconnectFromGame(new DisconnectPeerRequest(13), clientConnection.getAuthentication());
    verify(gameService).disconnectPlayerFromGame(player, 13);
  }

  @Test
  public void restoreGameSession() {
    instance.restoreGameSession(new RestoreGameSessionRequest(5), clientConnection.getAuthentication());
    verify(gameService).restoreGameSession(player, 5);
  }

  @Test
  public void hostGameRequest() {
    instance.hostGameRequest(new HostGameRequest("scmp01", "Title", "sh", "pw", GameVisibility.PUBLIC, 600, 900), clientConnection.getAuthentication());
    verify(gameService).createCustomGame("Title", "sh", "scmp01", "pw", GameVisibility.PUBLIC, 600, 900, player, LobbyMode.DEFAULT, Optional.empty());
  }

  @Test
  public void joinGameRequest() {
    instance.joinGameRequest(new JoinGameRequest(1, "pw"), clientConnection.getAuthentication());
    verify(gameService).joinGame(1, "pw", player);
  }

  @Test
  public void updateGameState() {
    instance.updateGameState(new GameStateReport(PlayerGameState.LAUNCHING), clientConnection.getAuthentication());
    verify(gameService).updatePlayerGameState(PlayerGameState.LAUNCHING, player);
  }

  @Test
  public void updateGameOption() {
    instance.updateGameOption(new GameOptionReport("key", "value"), clientConnection.getAuthentication());
    verify(gameService).updateGameOption(player, "key", "value");
  }

  @Test
  public void updatePlayerOption() {
    instance.updatePlayerOption(new PlayerOptionReport(1, "key", "value"), clientConnection.getAuthentication());
    verify(gameService).updatePlayerOption(player, 1, "key", "value");
  }

  @Test
  public void clearSlot() {
    instance.clearSlot(ClearSlotRequest.valueOf(1), clientConnection.getAuthentication());
    verify(gameService).clearSlot(player.getCurrentGame(), 1);
  }

  @Test
  public void updateAiOption() {
    instance.updateAiOption(new AiOptionReport("ai", "key", "value"), clientConnection.getAuthentication());
    verify(gameService).updateAiOption(player, "ai", "key", "value");
  }

  @Test
  public void reportDesync() {
    instance.reportDesync(DesyncReport.INSTANCE, clientConnection.getAuthentication());
    verify(gameService).reportDesync(player);
  }

  @Test
  public void onGameEnded() {
    instance.onGameEnded(GameEndedReport.INSTANCE, clientConnection.getAuthentication());
    verify(gameService).reportGameEnded(player);
  }

  @Test
  public void updateGameMods() {
    UUID uuid1 = UUID.randomUUID();
    UUID uuid2 = UUID.randomUUID();
    instance.updateGameMods(new GameModsReport(2, Arrays.asList(uuid1, uuid2)), clientConnection.getAuthentication());
    verify(gameService).updateGameMods(player.getCurrentGame(), Arrays.asList(uuid1, uuid2));
  }

  @Test
  public void reportArmyOutcome() {
    instance.reportArmyOutcome(new ArmyOutcomeReport(1, Outcome.VICTORY, 10), clientConnection.getAuthentication());
    verify(gameService).reportArmyOutcome(player, 1, Outcome.VICTORY, 10);
  }

  @Test
  public void reportArmyScore() {
    instance.reportArmyScore(new ArmyScoreReport(1, 10), clientConnection.getAuthentication());
    verify(gameService).reportArmyScore(player, 1, 10);
  }

  @Test
  public void reportGameStatistics() {
    ArmyStatisticsReport report = new ArmyStatisticsReport(Collections.emptyList());
    instance.reportGameStatistics(report, clientConnection.getAuthentication());
    verify(gameService).reportArmyStatistics(player, report.getArmyStatistics());
  }

  @Test
  public void mutuallyAgreeDraw() {
    instance.mutuallyAgreeDraw(MutuallyAgreedDrawRequest.INSTANCE, clientConnection.getAuthentication());
    verify(gameService).mutuallyAgreeDraw(player);
  }

  @Test
  public void onClientDisconnected() {
    instance.onClientDisconnected(new ClientDisconnectedEvent(this, clientConnection));
    verify(gameService).removePlayer(player);
  }

  @Test
  public void onDisconnected() {
    instance.onDisconnected(new PlayerDisconnectedReport(42), clientConnection.getAuthentication());
    verify(gameService).playerDisconnected(player, 42);
  }
}
