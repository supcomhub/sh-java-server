package org.supcomhub.server.integration;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.supcomhub.server.client.ConnectionAware;
import org.supcomhub.server.game.Faction;
import org.supcomhub.server.matchmaker.CreateMatchRequest;
import org.supcomhub.server.matchmaker.CreateMatchRequest.LobbyMode;
import org.supcomhub.server.matchmaker.CreateMatchRequest.Participant;
import org.supcomhub.server.matchmaker.MatchMakerCancelRequest;
import org.supcomhub.server.matchmaker.MatchMakerMapper;
import org.supcomhub.server.matchmaker.MatchMakerSearchRequest;
import org.supcomhub.server.matchmaker.MatchMakerService;
import org.supcomhub.server.security.Account;
import org.supcomhub.server.security.SecurityTestUtil;
import org.supcomhub.server.security.ShUserDetails;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class MatchMakerServiceActivatorTest {
  private static final String LADDER_1V1 = "ladder1v1";
  private MatchMakerServiceActivator instance;

  @Mock
  private MatchMakerService matchmakerService;
  @Mock
  private MatchMakerMapper matchMakerMapper;

  private Account player;
  private Authentication authentication;

  @Before
  public void setUp() throws Exception {
    player = SecurityTestUtil.createAccount();

    authentication = new TestingAuthenticationToken(new ShUserDetails(player, Collections.emptySet()), null);

    instance = new MatchMakerServiceActivator(matchmakerService, matchMakerMapper);
  }

  @Test
  public void startSearch() {
    instance.startSearch(new MatchMakerSearchRequest(Faction.CYBRAN, LADDER_1V1), authentication);
    verify(matchmakerService).submitSearch(player, Faction.CYBRAN, LADDER_1V1);
  }

  @Test
  public void cancelSearch() {
    instance.cancelSearch(new MatchMakerCancelRequest(LADDER_1V1), authentication);
    verify(matchmakerService).cancelSearch(LADDER_1V1, player);
  }

  @Test
  public void createMatch() {
    UUID requestId = UUID.randomUUID();
    List<Participant> participants = Arrays.asList(
      new Participant(),
      new Participant()
    );

    CreateMatchRequest request = new CreateMatchRequest(requestId, "Test Match", 1, 33, participants, LobbyMode.NONE, 1);
    instance.createMatch(request, authentication);

    verify(matchmakerService).createMatch((ConnectionAware) authentication.getPrincipal(), requestId, "Test Match",
      33, matchMakerMapper.map(participants), 1, 1);
  }
}
