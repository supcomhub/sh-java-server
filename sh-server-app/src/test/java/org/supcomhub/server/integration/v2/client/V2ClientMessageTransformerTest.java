package org.supcomhub.server.integration.v2.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mapstruct.factory.Mappers;
import org.mockito.junit.MockitoJUnitRunner;
import org.supcomhub.server.avatar.SelectAvatarRequest;
import org.supcomhub.server.client.BroadcastRequest;
import org.supcomhub.server.common.ClientMessage;
import org.supcomhub.server.config.JacksonConfig;
import org.supcomhub.server.coop.CoopMissionCompletedReport;
import org.supcomhub.server.game.AiOptionReport;
import org.supcomhub.server.game.ArmyOutcomeReport;
import org.supcomhub.server.game.ArmyScoreReport;
import org.supcomhub.server.game.BottleneckClearedReport;
import org.supcomhub.server.game.BottleneckReport;
import org.supcomhub.server.game.ClearSlotRequest;
import org.supcomhub.server.game.DesyncReport;
import org.supcomhub.server.game.DisconnectPeerRequest;
import org.supcomhub.server.game.DisconnectedReport;
import org.supcomhub.server.game.EnforceRatingReport;
import org.supcomhub.server.game.GameChatMessageReport;
import org.supcomhub.server.game.GameEndedReport;
import org.supcomhub.server.game.GameModsReport;
import org.supcomhub.server.game.GameOptionReport;
import org.supcomhub.server.game.GameStateReport;
import org.supcomhub.server.game.HostGameRequest;
import org.supcomhub.server.game.JoinGameRequest;
import org.supcomhub.server.game.MutuallyAgreedDrawRequest;
import org.supcomhub.server.game.PlayerOptionReport;
import org.supcomhub.server.game.RehostRequest;
import org.supcomhub.server.game.RestoreGameSessionRequest;
import org.supcomhub.server.game.TeamKillReportReport;
import org.supcomhub.server.ice.IceMessage;
import org.supcomhub.server.ice.IceServersRequest;
import org.supcomhub.server.matchmaker.MatchMakerCancelRequest;
import org.supcomhub.server.matchmaker.MatchMakerSearchRequest;
import org.supcomhub.server.protocol.v2.dto.Faction;
import org.supcomhub.server.protocol.v2.dto.GameVisibility;
import org.supcomhub.server.protocol.v2.dto.client.AgreeDrawClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.AiOptionClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.ArmyOutcomeClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.ArmyScoreClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.BottleneckClearedClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.BottleneckClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.BroadcastClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.CancelMatchSearchClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.ClearSlotClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.DisconnectPeerClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.DisconnectedClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.EnforceRatingClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.GameChatClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.GameDesyncClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.GameEndedClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.GameModsClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.GameOptionClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.GameStateClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.HostGameClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.IceClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.JoinGameClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.ListIceServersClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.OperationCompleteClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.Outcome;
import org.supcomhub.server.protocol.v2.dto.client.PlayerGameState;
import org.supcomhub.server.protocol.v2.dto.client.PlayerOptionClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.RehostClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.RestoreGameSessionClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.SearchMatchClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.SelectAvatarClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.TeamKillReportClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.UpdateSocialRelationClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.UpdateSocialRelationClientMessage.Operation;
import org.supcomhub.server.protocol.v2.dto.client.UpdateSocialRelationClientMessage.RelationType;
import org.supcomhub.server.protocol.v2.dto.client.V2ClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.V2ClientMessageWrapper;
import org.supcomhub.server.social.UpdateSocialRelationRequest;

import java.time.Duration;
import java.util.Arrays;
import java.util.UUID;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class V2ClientMessageTransformerTest {

  private V2ClientMessageTransformer instance;

  private ObjectMapper objectMapper;

  @Before
  public void setUp() throws Exception {
    objectMapper = new JacksonConfig().objectMapper();

    instance = new V2ClientMessageTransformer(objectMapper, Mappers.getMapper(V2ClientMessageMapper.class));
  }

  private String write(V2ClientMessage message) throws JsonProcessingException {
    return objectMapper.writeValueAsString(new V2ClientMessageWrapper(message));
  }

  @Test
  public void agreeDraw() throws Exception {
    ClientMessage result = instance.transform(write(new AgreeDrawClientMessage()));
    assertThat(result, CoreMatchers.is(MutuallyAgreedDrawRequest.INSTANCE));
  }

  @Test
  public void aiOption() throws Exception {
    ClientMessage result = instance.transform(write(new AiOptionClientMessage("junit", "team", "1")));
    assertThat(result, CoreMatchers.is(new AiOptionReport("junit", "team", "1")));
  }

  @Test
  public void armyOutcome() throws Exception {
    ClientMessage result = instance.transform(write(new ArmyOutcomeClientMessage(1, Outcome.DEFEAT, -1)));
    assertThat(result, CoreMatchers.is(new ArmyOutcomeReport(1, org.supcomhub.server.game.Outcome.DEFEAT, -1)));
  }

  @Test
  public void armyScore() throws Exception {
    ClientMessage result = instance.transform(write(new ArmyScoreClientMessage(1, 10)));
    assertThat(result, CoreMatchers.is(new ArmyScoreReport(1, 10)));
  }

  @Test
  public void broadcast() throws Exception {
    ClientMessage result = instance.transform(write(new BroadcastClientMessage("Hello JUnit")));
    assertThat(result, is(new BroadcastRequest("Hello JUnit")));
  }

  @Test
  public void cancelMatchSearch() throws Exception {
    ClientMessage result = instance.transform(write(new CancelMatchSearchClientMessage("ladder1v1")));
    assertThat(result, is(new MatchMakerCancelRequest("ladder1v1")));
  }

  @Test
  public void clearSlot() throws Exception {
    ClientMessage result = instance.transform(write(new ClearSlotClientMessage(1)));
    assertThat(result, CoreMatchers.is(ClearSlotRequest.valueOf(1)));
  }

  @Test
  public void coopMissionCompleted() throws Exception {
    ClientMessage result = instance.transform(write(new OperationCompleteClientMessage(true, false, 300)));
    assertThat(result, CoreMatchers.is(new CoopMissionCompletedReport(true, false, Duration.ofSeconds(300))));
  }

  @Test
  public void disconnectPeer() throws Exception {
    ClientMessage result = instance.transform(write(new DisconnectPeerClientMessage(42)));
    assertThat(result, CoreMatchers.is(new DisconnectPeerRequest(42)));
  }

  @Test
  public void gameDesync() throws Exception {
    ClientMessage result = instance.transform(write(new GameDesyncClientMessage()));
    assertThat(result, CoreMatchers.is(DesyncReport.INSTANCE));
  }

  @Test
  public void gameEnded() throws Exception {
    ClientMessage result = instance.transform(write(new GameEndedClientMessage()));
    assertThat(result, CoreMatchers.is(GameEndedReport.INSTANCE));
  }

  @Test
  public void gameMods() throws Exception {
    UUID uuid1 = UUID.randomUUID();
    UUID uuid2 = UUID.randomUUID();

    ClientMessage result = instance.transform(write(new GameModsClientMessage(new Object[]{"uids", uuid1 + " " + uuid2})));
    assertThat(result, CoreMatchers.is(new GameModsReport(2, Arrays.asList(uuid1, uuid2))));
  }

  @Test
  public void gameOption() throws Exception {
    ClientMessage result = instance.transform(write(new GameOptionClientMessage("key", "value")));
    assertThat(result, CoreMatchers.is(new GameOptionReport("key", "value")));
  }

  @Test
  public void gameState() throws Exception {
    ClientMessage result = instance.transform(write(new GameStateClientMessage(PlayerGameState.LAUNCHING)));
    assertThat(result, CoreMatchers.is(new GameStateReport(org.supcomhub.server.game.PlayerGameState.LAUNCHING)));
  }

  @Test
  public void hostGame() throws Exception {
    ClientMessage result = instance.transform(write(new HostGameClientMessage("scmp1", "Game", "sh", "123", GameVisibility.PUBLIC, 5, 9)));
    assertThat(result, CoreMatchers.is(new HostGameRequest("scmp1", "Game", "sh", "123", org.supcomhub.server.game.GameVisibility.PUBLIC, 5, 9)));
  }

  @Test
  public void iceClient() throws Exception {
    ClientMessage result = instance.transform(write(new IceClientMessage(12, "foobar")));
    assertThat(result, is(new IceMessage(12, "foobar")));
  }

  @Test
  public void joinGame() throws Exception {
    ClientMessage result = instance.transform(write(new JoinGameClientMessage(34, "foobar")));
    assertThat(result, CoreMatchers.is(new JoinGameRequest(34, "foobar")));
  }

  @Test
  public void listIceServers() throws Exception {
    ClientMessage result = instance.transform(write(new ListIceServersClientMessage()));
    assertThat(result, is(IceServersRequest.INSTANCE));
  }

  @Test
  public void enforceRating() throws Exception {
    ClientMessage result = instance.transform(write(new EnforceRatingClientMessage()));
    assertThat(result, CoreMatchers.is(EnforceRatingReport.INSTANCE));
  }

  @Test
  public void playerOption() throws Exception {
    ClientMessage result = instance.transform(write(new PlayerOptionClientMessage(123, "key", "value")));
    assertThat(result, CoreMatchers.is(new PlayerOptionReport(123, "key", "value")));
  }

  @Test
  public void restoreGameSession() throws Exception {
    ClientMessage result = instance.transform(write(new RestoreGameSessionClientMessage(123)));
    assertThat(result, CoreMatchers.is(new RestoreGameSessionRequest(123)));
  }

  @Test
  public void searchMatch() throws Exception {
    ClientMessage result = instance.transform(write(new SearchMatchClientMessage(Faction.UEF, "ladder1v1")));
    assertThat(result, is(new MatchMakerSearchRequest(org.supcomhub.server.game.Faction.UEF, "ladder1v1")));
  }

  @Test
  public void selectAvatar() throws Exception {
    ClientMessage result = instance.transform(write(new SelectAvatarClientMessage(1231)));
    assertThat(result, is(new SelectAvatarRequest(1231)));
  }

  @Test
  public void teamKill() throws Exception {
    ClientMessage result = instance.transform(write(new TeamKillReportClientMessage(100, 1, "A", 2, "B")));
    assertThat(result, CoreMatchers.is(new TeamKillReportReport(Duration.ofSeconds(100), 1, "A", 2, "B")));
  }

  @Test
  public void gameChatMessage() throws Exception {
    ClientMessage result = instance.transform(write(new GameChatClientMessage("Message")));
    assertThat(result, CoreMatchers.is(new GameChatMessageReport("Message")));
  }

  @Test
  public void disconnectedClientMessage() throws Exception {
    ClientMessage result = instance.transform(write(new DisconnectedClientMessage(5)));
    assertThat(result, CoreMatchers.is(new DisconnectedReport(5)));
  }

  @Test
  public void bottleneckClientMessage() throws Exception {
    ClientMessage result = instance.transform(write(new BottleneckClientMessage()));
    assertThat(result, CoreMatchers.is(BottleneckReport.INSTANCE));
  }

  @Test
  public void bottleneckClearedClientMessage() throws Exception {
    ClientMessage result = instance.transform(write(new BottleneckClearedClientMessage()));
    assertThat(result, CoreMatchers.is(BottleneckClearedReport.INSTANCE));
  }

  @Test
  public void rehostClientMessage() throws Exception {
    ClientMessage result = instance.transform(write(new RehostClientMessage()));
    assertThat(result, CoreMatchers.is(RehostRequest.INSTANCE));
  }

  @Test
  public void addFriend() throws Exception {
    ClientMessage result = instance.transform(write(new UpdateSocialRelationClientMessage(42, Operation.ADD, RelationType.FRIEND)));
    assertThat(result, CoreMatchers.is(new UpdateSocialRelationRequest(
      42,
      UpdateSocialRelationRequest.Operation.ADD,
      UpdateSocialRelationRequest.RelationType.FRIEND
    )));
  }

  @Test
  public void addFoe() throws Exception {
    ClientMessage result = instance.transform(write(new UpdateSocialRelationClientMessage(42, Operation.ADD, RelationType.FOE)));
    assertThat(result, CoreMatchers.is(new UpdateSocialRelationRequest(
      42,
      UpdateSocialRelationRequest.Operation.ADD,
      UpdateSocialRelationRequest.RelationType.FOE
    )));
  }

  @Test
  public void removeFriend() throws Exception {
    ClientMessage result = instance.transform(write(new UpdateSocialRelationClientMessage(42, Operation.REMOVE, RelationType.FRIEND)));
    assertThat(result, CoreMatchers.is(new UpdateSocialRelationRequest(
      42,
      UpdateSocialRelationRequest.Operation.REMOVE,
      UpdateSocialRelationRequest.RelationType.FRIEND
    )));
  }

  @Test
  public void removeFoe() throws Exception {
    ClientMessage result = instance.transform(write(new UpdateSocialRelationClientMessage(42, Operation.REMOVE, RelationType.FOE)));
    assertThat(result, CoreMatchers.is(new UpdateSocialRelationRequest(
      42,
      UpdateSocialRelationRequest.Operation.REMOVE,
      UpdateSocialRelationRequest.RelationType.FOE
    )));
  }
}
