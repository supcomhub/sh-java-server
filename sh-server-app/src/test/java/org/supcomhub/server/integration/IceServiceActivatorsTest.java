package org.supcomhub.server.integration;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.supcomhub.server.client.ClientConnection;
import org.supcomhub.server.ice.IceMessage;
import org.supcomhub.server.ice.IceServersRequest;
import org.supcomhub.server.ice.IceService;
import org.supcomhub.server.security.Account;
import org.supcomhub.server.security.SecurityTestUtil;
import org.supcomhub.server.security.ShUserDetails;

import java.net.InetAddress;
import java.util.Collections;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class IceServiceActivatorsTest {
  private IceServiceActivators instance;

  @Mock
  private IceService iceService;
  private Account player;
  private ClientConnection clientConnection;

  @Before
  public void setUp() throws Exception {
    player = SecurityTestUtil.createAccount();

    player.setClientConnection(clientConnection);

    clientConnection = new ClientConnection("1", Protocol.V2_JSON_UTF_8, mock(InetAddress.class))
      .setAuthentication(new TestingAuthenticationToken(new ShUserDetails(player, Collections.emptySet()), null));

    instance = new IceServiceActivators(iceService);
  }

  @Test
  public void requestIceServers() {
    instance.requestIceServers(IceServersRequest.INSTANCE, clientConnection.getAuthentication());

    verify(iceService).requestIceServers(player);
  }

  @Test
  public void forwardIceMessage() {
    Object payload = new Object();
    instance.forwardIceMessage(new IceMessage(42, payload), clientConnection.getAuthentication());

    verify(iceService).forwardIceMessage(player, 42, payload);
  }
}
