package org.supcomhub.server.coop;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.supcomhub.server.error.ErrorCode;
import org.supcomhub.server.error.RequestExceptionWithCode;
import org.supcomhub.server.game.Game;
import org.supcomhub.server.game.GamePlayerStats;
import org.supcomhub.server.map.MapVersion;
import org.supcomhub.server.security.Account;

import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CoopServiceTest {

  @Rule
  public ExpectedException expectedException = ExpectedException.none();
  private CoopService instance;
  @Mock
  private CoopMapRepository coopMapRepository;
  @Mock
  private CoopLeaderboardRepository coopLeaderboardRepository;

  @Before
  public void setUp() throws Exception {
    instance = new CoopService(coopMapRepository, coopLeaderboardRepository);
  }

  @Test
  public void reportOperationCompleteNotInGame() {
    expectedException.expect(RequestExceptionWithCode.requestExceptionWithCode(ErrorCode.COOP_CANT_REPORT_NOT_IN_GAME));
    instance.reportOperationComplete(new Account(), true, Duration.ofMinutes(1));
  }

  @Test
  public void reportOperationComplete() {
    Game game = new Game(42).setMapVersion((MapVersion) new MapVersion().setId(222));
    Map<Integer, GamePlayerStats> playerStats = game.getParticipants();
    playerStats.put(1, new GamePlayerStats());
    playerStats.put(2, new GamePlayerStats());
    playerStats.put(3, new GamePlayerStats());

    Account player = new Account();
    player.setCurrentGame(game);

    CoopMap mission = new CoopMap();
    when(coopMapRepository.findOneByMapVersionId(222)).thenReturn(Optional.of(mission));

    instance.reportOperationComplete(player, false, Duration.ofMinutes(8));

    ArgumentCaptor<CoopLeaderboardEntry> captor = ArgumentCaptor.forClass(CoopLeaderboardEntry.class);
    verify(coopLeaderboardRepository).save(captor.capture());
    CoopLeaderboardEntry entry = captor.getValue();

    assertThat(entry.getGameId(), is(42L));
    assertThat(entry.getMission(), is(mission));
    assertThat(entry.getPlayerCount(), is(3));
    assertThat(entry.getGameTime(), is((int) Duration.ofMinutes(8).toSeconds()));
  }

  @Test
  public void getMaps() {
    List<CoopMap> maps = Arrays.asList(new CoopMap(), new CoopMap());
    when(coopMapRepository.findAll()).thenReturn(maps);

    List<CoopMap> result = instance.getMaps();

    assertThat(result, is(maps));
  }
}
