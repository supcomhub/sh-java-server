package org.supcomhub.server.chat;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.supcomhub.server.clan.Clan;
import org.supcomhub.server.client.ClientService;
import org.supcomhub.server.config.ServerProperties;
import org.supcomhub.server.config.ServerProperties.Chat;
import org.supcomhub.server.player.PlayerOnlineEvent;
import org.supcomhub.server.security.Account;
import org.supcomhub.server.security.SecurityTestUtil;
import org.supcomhub.server.security.UserGroup;
import org.supcomhub.server.security.UserRole;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class ChatServiceTest {
  private ChatService instance;

  @Mock
  private ClientService clientService;

  @Before
  public void setUp() throws Exception {
    ServerProperties properties = new ServerProperties();

    Chat chat = properties.getChat();
    chat.setAdminChannels(Collections.singletonList("#admins"));
    chat.setModeratorChannels(Collections.singletonList("#moderators"));
    chat.setDefaultChannels(Arrays.asList("#foo", "#bar"));
    chat.setClanChannelFormat("#clan_%s");

    instance = new ChatService(properties, clientService);
  }

  @Test
  @SuppressWarnings("unchecked")
  public void onAuthenticationSuccessJoinsChannels() throws Exception {
    testJoinChannels(null, "#foo", "#bar", "#clan_junit");
  }

  @Test
  @SuppressWarnings("unchecked")
  public void onAuthenticationSuccessJoinsAdminChannels() throws Exception {
    testJoinChannels(UserRole.ADMINISTRATOR, "#admins", "#foo", "#bar", "#clan_junit");
  }

  @Test
  public void onAuthenticationSuccessJoinsModeratorChannels() throws Exception {
    testJoinChannels(UserRole.MODERATOR, "#moderators", "#foo", "#bar", "#clan_junit");
  }

  @SuppressWarnings("unchecked")
  private void testJoinChannels(UserRole group, String... expectedChannels) {
    Account player = SecurityTestUtil.createAccount()
      .setClan(new Clan().setTag("junit"));

    if (group != null) {
      player.getUserGroups().add(new UserGroup().setTechnicalName(group.getString()));
    }

    instance.onPlayerOnlineEvent(new PlayerOnlineEvent(this, player));

    ArgumentCaptor<List<String>> captor = ArgumentCaptor.forClass(List.class);
    verify(clientService).sendChatChannels(captor.capture(), any());

    List<String> channels = captor.getValue();
    assertThat(channels, containsInAnyOrder(expectedChannels));
  }
}
