package org.supcomhub.server.mod;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.supcomhub.server.client.ClientService;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class ModServiceTest {

  @Mock
  private ModVersionRepository modVersionRepository;
  @Mock
  private FeaturedModRepository featuredModRepository;
  @Mock
  private ClientService clientService;

  private ModService instance;

  @Before
  public void setUp() throws Exception {
    instance = new ModService(modVersionRepository, featuredModRepository, clientService);
  }

  @Test
  public void getFeaturedMod() {
    instance.getFeaturedMod(1);

    verify(featuredModRepository).findById(1);
  }
}
