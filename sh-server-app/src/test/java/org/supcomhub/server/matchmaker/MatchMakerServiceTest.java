package org.supcomhub.server.matchmaker;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.supcomhub.server.client.ClientService;
import org.supcomhub.server.client.ConnectionAware;
import org.supcomhub.server.config.ServerProperties;
import org.supcomhub.server.error.ErrorCode;
import org.supcomhub.server.error.RequestExceptionWithCode;
import org.supcomhub.server.game.Faction;
import org.supcomhub.server.game.Game;
import org.supcomhub.server.game.GameParticipant;
import org.supcomhub.server.game.GameService;
import org.supcomhub.server.game.GameVisibility;
import org.supcomhub.server.game.LobbyMode;
import org.supcomhub.server.game.PlayerOptions;
import org.supcomhub.server.map.MapService;
import org.supcomhub.server.map.MapVersion;
import org.supcomhub.server.mod.FeaturedMod;
import org.supcomhub.server.mod.ModService;
import org.supcomhub.server.player.PlayerService;
import org.supcomhub.server.rating.Rating;
import org.supcomhub.server.rating.RatingService;
import org.supcomhub.server.security.Account;
import org.supcomhub.server.security.Ban;
import org.supcomhub.server.security.Ban.BanScope;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyIterable;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.timeout;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MatchMakerServiceTest {

  private static final String POOL_NAME = "ladder1v1";
  private static final String PLAYER_1_NAME = "Player 1";
  private static final String PLAYER_2_NAME = "Player 2";
  private static final int FEATURED_MOD_ID = 1;
  private static final int LEADERBOARD_ID = 9;

  @Rule
  public ExpectedException expectedException = ExpectedException.none();

  private MatchMakerService instance;
  private ServerProperties properties;

  @Mock
  private ModService modService;
  @Mock
  private ClientService clientService;
  @Mock
  private GameService gameService;
  @Mock
  private MapService mapService;
  @Mock
  private PlayerService playerService;
  @Mock
  private MatchMakerPoolRepository matchMakerPoolRepository;

  @Before
  public void setUp() throws Exception {
    properties = new ServerProperties();
    FeaturedMod ladder1v1Mod = (FeaturedMod) new FeaturedMod().setTechnicalName("ladder1v1").setId(FEATURED_MOD_ID);

    when(modService.getLadder1v1Mod()).thenReturn(Optional.of(ladder1v1Mod));
    when(mapService.getRandomLadderMap(anyIterable())).thenReturn(new MapVersion().setFilename("SCMP_001.zip"));
    when(matchMakerPoolRepository.findAll()).thenReturn(Collections.singletonList(
      new MatchMakerPool(FEATURED_MOD_ID, LEADERBOARD_ID, "ladder1v1")
    ));

    RatingService ratingService = new RatingService(properties);
    instance = new MatchMakerService(modService, properties, ratingService, clientService, gameService, mapService, playerService, matchMakerPoolRepository);
    instance.afterPropertiesSet();
  }

  @Test
  public void startSearchAlreadyInGame() {
    Account player = new Account();
    player.setCurrentGame(new Game());

    expectedException.expect(RequestExceptionWithCode.requestExceptionWithCode(ErrorCode.ALREADY_IN_GAME));
    instance.submitSearch(player, Faction.CYBRAN, POOL_NAME);
  }

  @Test
  public void startSearchBannedIndefinitely() {
    Account player = new Account();
    player.getBans().add(new Ban().setScope(BanScope.MATCHMAKER));

    expectedException.expect(RequestExceptionWithCode.requestExceptionWithCode(ErrorCode.BANNED_FROM_MATCH_MAKER));
    instance.submitSearch(player, Faction.CYBRAN, POOL_NAME);
  }

  @Test
  public void startSearchBannedWithExpiry() {
    Account player = new Account();
    player.getBans().add(new Ban()
      .setScope(BanScope.MATCHMAKER)
      .setExpiresAt(Instant.now().plus(1, ChronoUnit.DAYS))
    );

    expectedException.expect(RequestExceptionWithCode.requestExceptionWithCode(ErrorCode.BANNED_FROM_MATCH_MAKER));
    instance.submitSearch(player, Faction.CYBRAN, POOL_NAME);
  }

  @Test
  public void startSearchExpiredBan() {
    Account player = new Account();
    player.getBans().add(new Ban()
      .setScope(BanScope.MATCHMAKER)
      .setExpiresAt(Instant.now().minus(1, ChronoUnit.SECONDS))
    );

    instance.submitSearch(player, Faction.CYBRAN, POOL_NAME);
    // Expect no exception thrown
  }

  @Test
  public void startSearchModNotAvailable() {
    expectedException.expect(RequestExceptionWithCode.requestExceptionWithCode(ErrorCode.MATCH_MAKER_POOL_DOESNT_EXIST));
    instance.submitSearch(new Account(), Faction.CYBRAN, "foobar");
  }

  @Test
  public void startSearchEmptyQueue() {
    instance.submitSearch(new Account(), Faction.CYBRAN, POOL_NAME);
    instance.processPools();

    verifyZeroInteractions(gameService);
  }

  /**
   * Tests whether two players who never played a game (and thus have no rating associated) don't match immediately,
   * because such players always have a low game quality.
   */
  @Test
  public void submitSearchTwoFreshPlayersDontMatchImmediately() {
    Account player1 = (Account) new Account().setDisplayName(PLAYER_1_NAME).setId(1);
    Account player2 = (Account) new Account().setDisplayName(PLAYER_2_NAME).setId(2);

    properties.getMatchMaker().setAcceptableQualityWaitTime(10);
    instance.submitSearch(player1, Faction.CYBRAN, POOL_NAME);
    instance.submitSearch(player2, Faction.AEON, POOL_NAME);
    instance.processPools();

    verify(gameService, never()).createGame(any(), anyInt(), any(), any(), any(), anyInt(), anyInt(), any(), any(), any(), any());
    verify(gameService, never()).joinGame(anyInt(), eq(null), any());
  }

  /**
   * Tests whether two players who never played a game (and thus have no rating associated) will be matched.
   */
  @Test
  public void submitSearchTwoFreshPlayersMatch() {
    Account player1 = (Account) new Account().setDisplayName(PLAYER_1_NAME).setId(1);
    Account player2 = (Account) new Account().setDisplayName(PLAYER_2_NAME).setId(2);
    Game game = new Game(1);

    GameParticipant gameParticipant1 = new GameParticipant(1, Faction.CYBRAN, 1, PLAYER_1_NAME, 0);
    GameParticipant gameParticipant2 = new GameParticipant(2, Faction.AEON, 1, PLAYER_2_NAME, 0);

    when(gameService.createGame(any(), anyInt(), any(), any(), any(), any(), any(), any(), any(), any(), any()))
      .thenReturn(CompletableFuture.completedFuture(game));
    when(gameService.joinGame(1, null, player2))
      .thenReturn(CompletableFuture.completedFuture(game));
    when(playerService.getOnlinePlayer(1)).thenReturn(Optional.of(player1));
    when(playerService.getOnlinePlayer(2)).thenReturn(Optional.of(player2));

    properties.getMatchMaker().setAcceptableQualityWaitTime(0);
    instance.submitSearch(player1, Faction.CYBRAN, POOL_NAME);
    instance.submitSearch(player2, Faction.AEON, POOL_NAME);
    instance.processPools();

    verify(gameService).createGame(PLAYER_1_NAME + " vs. " + PLAYER_2_NAME, FEATURED_MOD_ID, "SCMP_001.zip",
      null, GameVisibility.PRIVATE, null, null, player1, LobbyMode.NONE, Optional.of(List.of(gameParticipant1, gameParticipant2)), Optional.of(LEADERBOARD_ID));
    verify(gameService).joinGame(1, null, player2);

    verify(gameService, times(10)).updatePlayerOption(any(), anyInt(), any(), any());
  }

  /**
   * Tests whether two players whose ratings are too far apart don't get matched.
   */
  @Test
  public void submitSearchTwoPlayersDontMatchIfRatingsTooFarApart() {
    Account player1 = (Account) new Account()
      .setDisplayName(PLAYER_1_NAME)
      .setId(1);
    player1.getRatings().put(FEATURED_MOD_ID, new Rating(300d, 50d, FEATURED_MOD_ID, player1,3));

    Account player2 = (Account) new Account()
      .setDisplayName(PLAYER_2_NAME)
      .setId(2);
    player2.getRatings().put(FEATURED_MOD_ID, new Rating(1300d, 50d, FEATURED_MOD_ID, player2,3));

    instance.submitSearch(player1, Faction.CYBRAN, POOL_NAME);
    instance.submitSearch(player2, Faction.AEON, POOL_NAME);
    instance.processPools();

    verify(gameService, never()).createGame(any(), anyInt(), any(), any(), any(), anyInt(), anyInt(), any(), any(), any(), any());
    verify(gameService, never()).joinGame(anyInt(), eq(null), any());
  }

  @Test
  public void cancelSearch() {
    Account player1 = (Account) new Account().setDisplayName(PLAYER_1_NAME).setId(1);
    Account player2 = (Account) new Account().setDisplayName(PLAYER_2_NAME).setId(2);

    instance.submitSearch(player1, Faction.CYBRAN, POOL_NAME);
    instance.submitSearch(player2, Faction.AEON, POOL_NAME);

    assertThat(instance.getSearchPools().get(POOL_NAME).keySet(), hasSize(2));

    instance.cancelSearch(POOL_NAME, player1);
    assertThat(instance.getSearchPools().get(POOL_NAME).keySet(), hasSize(1));

    instance.cancelSearch(POOL_NAME, player1);
    assertThat(instance.getSearchPools().get(POOL_NAME).keySet(), hasSize(1));

    instance.cancelSearch(POOL_NAME, player2);
    assertThat(instance.getSearchPools().get(POOL_NAME).keySet(), hasSize(0));
  }

  @Test
  public void onClientDisconnect() {
    Account player1 = (Account) new Account().setDisplayName(PLAYER_1_NAME).setId(1);
    Account player2 = (Account) new Account().setDisplayName(PLAYER_2_NAME).setId(2);

    instance.submitSearch(player1, Faction.CYBRAN, POOL_NAME);
    instance.submitSearch(player2, Faction.AEON, POOL_NAME);

    assertThat(instance.getSearchPools().get(POOL_NAME).keySet(), hasSize(2));

    instance.removePlayer(player1);

    assertThat(instance.getSearchPools().get(POOL_NAME).keySet(), hasSize(1));
  }

  @Test
  public void createMatch() {
    GameParticipant participant1 = new GameParticipant().setId(1).setFaction(Faction.UEF).setTeam(1).setStartSpot(1);
    GameParticipant participant2 = new GameParticipant().setId(2).setFaction(Faction.AEON).setTeam(2).setStartSpot(2);

    ConnectionAware requester = mock(ConnectionAware.class);
    UUID requestId = UUID.randomUUID();
    List<GameParticipant> participants = Arrays.asList(
      participant1, participant2
    );
    int mapVersionId = 1;

    Account player1 = (Account) new Account().setId(1);
    Account player2 = (Account) new Account().setId(2);

    when(playerService.getOnlinePlayer(participant1.getId())).thenReturn(Optional.of(player1));
    when(playerService.getOnlinePlayer(participant2.getId())).thenReturn(Optional.of(player2));
    when(mapService.findMap(mapVersionId)).thenReturn(Optional.of(new MapVersion().setFilename("foo.zip")));

    Game game = (Game) new Game().setId(1);
    when(gameService.createGame("Test match", 4, "foo.zip", null, GameVisibility.PRIVATE, null, null, player1, LobbyMode.NONE, Optional.of(participants), Optional.of(LEADERBOARD_ID)))
      .thenReturn(CompletableFuture.completedFuture(game));

    when(gameService.joinGame(game.getId(), null, player2)).thenReturn(CompletableFuture.completedFuture(game));

    instance.createMatch(requester, requestId, "Test match", 4, participants, mapVersionId, LEADERBOARD_ID);

    verify(clientService, timeout(5000)).sendMatchCreatedNotification(requestId, 1, requester);

    verify(gameService, timeout(1000)).updatePlayerOption(player1, player1.getId(), PlayerOptions.OPTION_TEAM, participant1.getTeam());
    verify(gameService, timeout(1000)).updatePlayerOption(player1, player1.getId(), PlayerOptions.OPTION_FACTION, participant1.getFaction().toFaValue());
    verify(gameService, timeout(1000)).updatePlayerOption(player1, player1.getId(), PlayerOptions.OPTION_START_SPOT, participant1.getStartSpot());
    verify(gameService, timeout(1000)).updatePlayerOption(player1, player1.getId(), PlayerOptions.OPTION_COLOR, 1);
    verify(gameService, timeout(1000)).updatePlayerOption(player1, player1.getId(), PlayerOptions.OPTION_ARMY, 1);

    verify(gameService, timeout(1000)).updatePlayerOption(player1, player2.getId(), PlayerOptions.OPTION_TEAM, participant2.getTeam());
    verify(gameService, timeout(1000)).updatePlayerOption(player1, player2.getId(), PlayerOptions.OPTION_FACTION, participant2.getFaction().toFaValue());
    verify(gameService, timeout(1000)).updatePlayerOption(player1, player2.getId(), PlayerOptions.OPTION_START_SPOT, participant2.getStartSpot());
    verify(gameService, timeout(1000)).updatePlayerOption(player1, player2.getId(), PlayerOptions.OPTION_COLOR, 2);
    verify(gameService, timeout(1000)).updatePlayerOption(player1, player2.getId(), PlayerOptions.OPTION_ARMY, 2);
  }

  // TODO test updating queue
}
