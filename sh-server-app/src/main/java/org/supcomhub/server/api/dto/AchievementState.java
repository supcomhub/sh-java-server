package org.supcomhub.server.api.dto;

public enum AchievementState {
  REVEALED, UNLOCKED, HIDDEN
}
