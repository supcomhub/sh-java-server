package org.supcomhub.server.mod;

import io.jsonwebtoken.lang.Assert;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.supcomhub.server.cache.CacheNames;
import org.supcomhub.server.client.ClientService;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@Slf4j
public class ModService {
  private static final String COOP_MOD_NAME = "coop";
  private static final String LADDER_1V1_MOD_NAME = "ladder1v1";

  private final ModVersionRepository modVersionRepository;
  private final FeaturedModRepository featuredModRepository;

  private Optional<FeaturedMod> coopFeaturedMod;
  private Optional<FeaturedMod> ladder1v1FeaturedMod;

  @Autowired
  @SuppressWarnings("SpringAutowiredFieldsWarningInspection")
  // Required for access to @Cacheable methods since inner calls do not go through proxy object.
  private ModService modService;
  private boolean applicationInitialized;

  public ModService(ModVersionRepository modVersionRepository, FeaturedModRepository featuredModRepository,
                    ClientService clientService) {
    this.modVersionRepository = modVersionRepository;
    this.featuredModRepository = featuredModRepository;

    coopFeaturedMod = Optional.empty();
    ladder1v1FeaturedMod = Optional.empty();
  }

  @EventListener
  @Transactional(readOnly = true)
  public void onApplicationEvent(ContextRefreshedEvent event) {
    coopFeaturedMod = featuredModRepository.findOneByTechnicalName(COOP_MOD_NAME);
    ladder1v1FeaturedMod = featuredModRepository.findOneByTechnicalName(LADDER_1V1_MOD_NAME);

    if (coopFeaturedMod.isEmpty()) {
      log.warn("No mod named '{}' was found in the database. Coop will not be available.", COOP_MOD_NAME);
    }
    if (ladder1v1FeaturedMod.isEmpty()) {
      log.warn("No mod named '{}' was found in the database. Ladder 1v1 will not be available.", LADDER_1V1_MOD_NAME);
    }
    applicationInitialized = true;
  }

  // TODO cache
  public List<ModVersion> findModVersionsByUids(List<UUID> uids) {
    return modVersionRepository.findByUuidIn(uids);
  }

  @Cacheable(CacheNames.RANKED_MODS)
  // FIXME check why this is unused
  public boolean isModRanked(UUID simModId) {
    return modVersionRepository.findOneByUuidAndRankedTrue(simModId).isPresent();
  }

  public boolean isCoop(int featuredModId) {
    verifyInitialized();
    return coopFeaturedMod.isPresent() && coopFeaturedMod.get().equals(featuredModId);
  }

  public boolean isLadder1v1(int featuredModId) {
    return ladder1v1FeaturedMod.isPresent() && ladder1v1FeaturedMod.get().getId().equals(featuredModId);
  }

  public Optional<FeaturedMod> getLadder1v1Mod() {
    verifyInitialized();
    return ladder1v1FeaturedMod;
  }

  public Optional<FeaturedMod> getFeaturedMod(String featuredModName) {
    return featuredModRepository.findOneByTechnicalName(featuredModName);
  }

  public Optional<FeaturedMod> getFeaturedMod(int featuredModId) {
    return featuredModRepository.findById(featuredModId);
  }

  private void verifyInitialized() {
    Assert.state(applicationInitialized, "In order to know whether this mod is available, the database must be queried. " +
      "This can only be done after the application has been started, which is not the case.");
  }
}
