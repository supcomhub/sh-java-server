package org.supcomhub.server.mod;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Immutable;
import org.supcomhub.server.common.jpa.AbstractIntegerIdEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "\"mod\"")
@Immutable
@Getter
@Setter
@NoArgsConstructor
@ToString(includeFieldNames = false, of = {"displayName"})
public class Mod extends AbstractIntegerIdEntity {

  @Column(name = "display_name")
  private String displayName;

  @Column(name = "author")
  private String author;

  @Column(name = "uploader_id")
  private int uploader;

}
