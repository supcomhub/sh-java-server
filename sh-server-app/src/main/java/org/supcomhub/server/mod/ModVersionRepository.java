package org.supcomhub.server.mod;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface ModVersionRepository extends JpaRepository<ModVersion, Integer> {

  Optional<ModVersion> findOneByUuidAndRankedTrue(UUID uuid);

  List<ModVersion> findByUuidIn(List<UUID> uids);
}
