package org.supcomhub.server.mod;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.supcomhub.server.common.jpa.AbstractIntegerIdEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "featured_mod")
@Getter
@Setter
@NoArgsConstructor
public class FeaturedMod extends AbstractIntegerIdEntity {

  @Column(name = "short_name")
  private String technicalName;

  @Column(name = "description")
  private String description;

  @Column(name = "display_name")
  private String displayName;

  @Column(name = "public")
  private boolean visible;

  @Column(name = "ordinal")
  private int displayOrder;

  @Column(name = "current_version")
  private int currentVersion;

}
