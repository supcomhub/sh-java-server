package org.supcomhub.server.mod;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.supcomhub.server.common.jpa.AbstractIntegerIdEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Table(name = "mod_version")
@Getter
@Setter
@NoArgsConstructor
public class ModVersion extends AbstractIntegerIdEntity {

  @Column(name = "uuid")
  private UUID uuid;

  @Column(name = "type")
  private ModType type;

  @Column(name = "description")
  private String description;

  @Column(name = "version")
  private short version;

  @Column(name = "filename")
  private String filename;

  @Column(name = "icon")
  private String icon;

  @Column(name = "ranked")
  private boolean ranked;

  @Column(name = "hidden")
  private boolean hidden;

  @ManyToOne(optional = false)
  @JoinColumn(name = "mod_id")
  private Mod mod;
}
