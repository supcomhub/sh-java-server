package org.supcomhub.server.coop;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.supcomhub.server.error.ErrorCode;
import org.supcomhub.server.error.Requests;
import org.supcomhub.server.game.Game;
import org.supcomhub.server.security.Account;

import java.time.Duration;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class CoopService {
  private final CoopMapRepository coopMapRepository;
  private final CoopLeaderboardRepository coopLeaderboardRepository;

  public CoopService(CoopMapRepository coopMapRepository, CoopLeaderboardRepository coopLeaderboardRepository) {
    this.coopMapRepository = coopMapRepository;
    this.coopLeaderboardRepository = coopLeaderboardRepository;
  }

  public void reportOperationComplete(Account player, boolean secondaryTargets, Duration duration) {
    Game game = player.getCurrentGame();
    Requests.verify(game != null, ErrorCode.COOP_CANT_REPORT_NOT_IN_GAME);

    log.debug("Player '{}' reported coop result '{}' with secondary targets '{}' for game: {}", player, duration, secondaryTargets, game);

    Assert.state(game.getMapVersion() != null, () -> "Coop game without known map version");

    Optional<CoopMap> optional = coopMapRepository.findOneByMapVersionId(game.getMapVersion().getId());
    if (optional.isPresent()) {
      CoopLeaderboardEntry coopLeaderboardEntry = new CoopLeaderboardEntry();
      coopLeaderboardEntry.setGameId(game.getId());
      coopLeaderboardEntry.setMission(optional.get());
      coopLeaderboardEntry.setPlayerCount(game.getParticipants().size());
      coopLeaderboardEntry.setSecondary(secondaryTargets);
      coopLeaderboardEntry.setGameTime((int) duration.toSeconds());

      coopLeaderboardRepository.save(coopLeaderboardEntry);
    }
  }

  public List<CoopMap> getMaps() {
    return coopMapRepository.findAll();
  }
}
