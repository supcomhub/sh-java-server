package org.supcomhub.server.coop;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.supcomhub.server.common.jpa.AbstractIntegerIdEntity;
import org.supcomhub.server.map.MapVersion;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "coop_mission")
@Getter
@Setter
@NoArgsConstructor
public class CoopMap extends AbstractIntegerIdEntity {

  @Column(name = "name")
  private String name;

  @Column(name = "description")
  private String description;

  @ManyToOne(optional = false)
  @JoinColumn(name = "map_version_id")
  private MapVersion mapVersion;

}
