package org.supcomhub.server.coop;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.supcomhub.server.common.jpa.AbstractIntegerIdEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "coop_leaderboard")
@Getter
@Setter
@NoArgsConstructor
public class CoopLeaderboardEntry extends AbstractIntegerIdEntity {

  @ManyToOne(optional = false)
  @JoinColumn(name = "coop_mission_id")
  private CoopMap mission;

  @Column(name = "game_id")
  private long gameId;

  @Column(name = "secondary_targets")
  private boolean secondary;

  /** Game time in seconds. */
  @Column(name = "game_time")
  private Integer gameTime;

  @Column(name = "player_count")
  private int playerCount;
}
