package org.supcomhub.server.coop;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.supcomhub.server.common.ClientMessage;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OperationCompleteReport implements ClientMessage {
  private int gameTime;
  private boolean allPrimary;
  private boolean allSecondary;
}
