package org.supcomhub.server.clan;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Immutable;
import org.jetbrains.annotations.NotNull;
import org.supcomhub.server.common.jpa.AbstractIntegerIdEntity;
import org.supcomhub.server.security.Account;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Table(name = "clan")
@Getter
@Setter
@Immutable
public class Clan extends AbstractIntegerIdEntity {

  @Column(name = "name")
  @NotNull
  private String name;

  @Column(name = "tag")
  @Size(max = 3)
  @NotNull
  private String tag;

  @ManyToOne(optional = false)
  @JoinColumn(name = "founder_id")
  private Account founder;

  @ManyToOne(optional = false)
  @JoinColumn(name = "leader_id")
  private Account leader;

  @Column(name = "description")
  private String description;

  @OneToMany(mappedBy = "clan")
  private List<ClanMembership> memberships;
}
