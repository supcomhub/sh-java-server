package org.supcomhub.server.clan;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Immutable;
import org.supcomhub.server.common.jpa.AbstractIntegerIdEntity;
import org.supcomhub.server.security.Account;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "clan_membership")
@Getter
@Setter
@Immutable
public class ClanMembership extends AbstractIntegerIdEntity {

  @ManyToOne(optional = false)
  @JoinColumn(name = "clan_id")
  private Clan clan;

  @ManyToOne(optional = false)
  @JoinColumn(name = "member_id")
  private Account member;
}
