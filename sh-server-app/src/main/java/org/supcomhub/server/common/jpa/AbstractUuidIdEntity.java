package org.supcomhub.server.common.jpa;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.util.UUID;

/**
 * Since {@code AbstractEntity<UUID>} can't be used to specify the ID type because due to type erasure Elide can't
 * resolve the concrete type anymore, classes who use {@link UUID} as ID type are supposed to extend this class.
 */
@MappedSuperclass
@Setter
@Getter
@EqualsAndHashCode(of = "id", callSuper = false)
public abstract class AbstractUuidIdEntity extends AbstractEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  protected UUID id;
}
