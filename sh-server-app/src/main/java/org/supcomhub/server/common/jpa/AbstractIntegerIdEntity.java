package org.supcomhub.server.common.jpa;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * Since {@code AbstractEntity<Integer>} can't be used to specify the ID type because due to type erasure Elide can't
 * resolve the concrete type anymore, classes who use {@link Integer} as ID type are supposed to extend this class.
 */
@MappedSuperclass
@Setter
@Getter
@EqualsAndHashCode(of = "id", callSuper = false)
@ToString
public abstract class AbstractIntegerIdEntity extends AbstractEntity {

  @Id
  @org.springframework.data.annotation.Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  protected Integer id;
}
