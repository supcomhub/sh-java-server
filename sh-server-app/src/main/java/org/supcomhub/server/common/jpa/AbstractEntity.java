package org.supcomhub.server.common.jpa;

import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.time.Instant;

@MappedSuperclass
@Setter
public abstract class AbstractEntity {

  @CreatedDate
  @Column(name = "create_time")
  protected Instant createTime;

  @LastModifiedDate
  @Column(name = "update_time")
  protected Instant updateTime;

  /**
   * Supplement method for @EqualsAndHashCode overriding the default lombok implementation
   */
  protected boolean canEqual(Object other) {
    return other instanceof AbstractEntity && this.getClass() == other.getClass();
  }
}
