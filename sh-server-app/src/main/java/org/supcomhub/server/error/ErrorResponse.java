package org.supcomhub.server.error;

import lombok.Data;
import org.supcomhub.server.common.ServerMessage;

import java.util.UUID;

@Data
public class ErrorResponse implements ServerMessage {
  private final ErrorCode errorCode;
  private final UUID requestId;
  private final Object[] args;
}
