package org.supcomhub.server;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ConfigurableApplicationContext;
import org.supcomhub.server.config.ServerProperties;

@SpringBootApplication
@EnableConfigurationProperties({ServerProperties.class})
public class ShServerApplication {

  public static void main(String[] args) {
    ConfigurableApplicationContext context = new SpringApplicationBuilder(ShServerApplication.class)
      .registerShutdownHook(false)
      .run(args);

    Runtime.getRuntime().addShutdownHook(new Thread(() -> {
      context.publishEvent(ApplicationShutdownEvent.INSTANCE);
      context.close();
    }));
  }

  public enum ApplicationShutdownEvent {
    INSTANCE
  }
}
