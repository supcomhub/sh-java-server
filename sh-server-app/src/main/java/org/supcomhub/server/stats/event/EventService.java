package org.supcomhub.server.stats.event;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.supcomhub.server.api.ApiAccessor;
import org.supcomhub.server.api.dto.UpdatedEventResponse;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@Service
@Slf4j
public class EventService {

  private final ApiAccessor apiAccessor;

  public EventService(ApiAccessor apiAccessor) {
    this.apiAccessor = apiAccessor;
  }

  public CompletableFuture<List<UpdatedEventResponse>> executeBatchUpdate(List<EventUpdate> eventUpdates) {
    log.debug("Updating '{}' events", eventUpdates.size());
    return CompletableFuture.completedFuture(apiAccessor.updateEvents(eventUpdates));
  }
}
