package org.supcomhub.server.stats;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.supcomhub.server.common.ClientMessage;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ArmyStatisticsReport implements ClientMessage {
  private List<ArmyStatistics> armyStatistics;
}
