package org.supcomhub.server.cache;

public class CacheNames {
  public static final String RANKED_MODS = "rankedMods";
  public static final String MAP_VERSIONS = "mapVersions";
  public static final String MOD_VERSIONS = "modVersions";
  public static final String LEADERBOARD_ID = "leaderboardId";
}
