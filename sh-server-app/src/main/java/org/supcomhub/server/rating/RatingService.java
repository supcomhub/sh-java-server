package org.supcomhub.server.rating;

import de.gesundkrank.jskills.GameInfo;
import de.gesundkrank.jskills.IPlayer;
import de.gesundkrank.jskills.ITeam;
import de.gesundkrank.jskills.Player;
import de.gesundkrank.jskills.Team;
import de.gesundkrank.jskills.TrueSkillCalculator;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.supcomhub.server.config.ServerProperties;
import org.supcomhub.server.game.GamePlayerStats;
import org.supcomhub.server.security.Account;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.Optional.ofNullable;

/**
 * Service for calculating game ratings, currently using TrueSkill.
 */
@Service
@Slf4j
public class RatingService {

  private final GameInfo gameInfo;

  public RatingService(ServerProperties properties) {
    ServerProperties.TrueSkill params = properties.getTrueSkill();
    gameInfo = new GameInfo(
      params.getInitialMean(),
      params.getInitialStandardDeviation(),
      params.getBeta(),
      params.getDynamicFactor(),
      params.getDrawProbability()
    );
  }

  public double calculateQuality(Rating left, Rating right) {
    de.gesundkrank.jskills.Rating leftRating = ofNullable(left)
      .map(rating -> new de.gesundkrank.jskills.Rating(left.getMean(), left.getDeviation()))
      .orElse(gameInfo.getDefaultRating());
    de.gesundkrank.jskills.Rating rightRating = ofNullable(left)
      .map(rating -> new de.gesundkrank.jskills.Rating(right.getMean(), right.getDeviation()))
      .orElse(gameInfo.getDefaultRating());

    Collection<ITeam> teams = Arrays.asList(
      new Team(new Player<>(1), leftRating),
      new Team(new Player<>(2), rightRating)
    );
    return TrueSkillCalculator.calculateMatchQuality(gameInfo, teams);
  }

  /**
   * Updates the ratings of all players in {@code playerStats}, according to the outcome of the game <strong> without
   * persisting the results</strong>.
   *
   * @param noTeamId ID of the "no team" team
   * @param featuredModId if {@code null}, the "global" rating will be updated
   */
  @SuppressWarnings("unchecked")
  public void updateRatings(Collection<GamePlayerStats> playerStats, int noTeamId, @Nullable Integer featuredModId) {
    Map<Integer, GamePlayerStats> playerStatsByPlayerId = playerStats.stream()
      .collect(Collectors.toMap(participant -> participant.getPlayer().getId(), Function.identity()));

    Map<Integer, ScoredTeam> teamsById = teamsById(playerStats, noTeamId);

    List<ScoredTeam> teamsSortedByScore = new ArrayList<>(teamsById.values());
    teamsSortedByScore.sort(Comparator.comparingInt(o -> ((ScoredTeam) o).getScore().intValue()).reversed());

    int[] teamRanks = calculateTeamRanks(teamsSortedByScore);

    // New ArrayList is needed because the JSkill API doesn't handle subclass type parameters
    TrueSkillCalculator.calculateNewRatings(gameInfo, new ArrayList<>(teamsSortedByScore), teamRanks).entrySet()
      .forEach(entry -> {
        Player<Integer> player = (Player<Integer>) entry.getKey();
        GamePlayerStats stats = playerStatsByPlayerId.get(player.getId());

        updatePlayerStats(entry, stats);
        updateRating(featuredModId, stats);
      });
  }

  public void initializeRating(Account player, int leaderboardId) {
    Assert.state(
      player.getRatings().get(leaderboardId) == null,
      () -> "Rating for leaderboard " + leaderboardId + " has already been set for player: " + player
    );

    Rating rating = new Rating(gameInfo.getInitialMean(), gameInfo.getInitialStandardDeviation(), leaderboardId, player, 3);
    player.getRatings().put(leaderboardId, rating);
  }

  private Map<Integer, ScoredTeam> teamsById(Collection<GamePlayerStats> participants, int noTeamId) {
    int highestTeamId = participants.stream()
      .map(GamePlayerStats::getTeam)
      .max(Integer::compareTo).orElse(noTeamId);

    Map<Integer, ScoredTeam> teamsById = new HashMap<>();
    for (GamePlayerStats participant : participants) {
      IPlayer player = new Player<>(participant.getPlayer().getId());
      de.gesundkrank.jskills.Rating rating = new de.gesundkrank.jskills.Rating(participant.getRatingMeanBefore(), participant.getRatingDeviationBefore());

      int teamId = participant.getTeam();
      if (teamId == noTeamId) {
        // If a player is in the "no team" team, assign him to his own team
        teamId = participant.getPlayer().getId() + highestTeamId;
      }
      ScoredTeam team = teamsById.computeIfAbsent(teamId, ScoredTeam::new);
      team.addPlayer(player, rating);
      team.getScore().updateAndGet(score -> score + Optional.ofNullable(participant.getScore()).orElse(0));
    }
    return teamsById;
  }

  /**
   * Returns an array in the form of {@code [1, 2, 2, 3, 4]}.
   */
  private int[] calculateTeamRanks(List<ScoredTeam> teamsSortedByScore) {
    int[] teamRanks = new int[teamsSortedByScore.size()];
    int rank = 0;
    for (int i = 0; i < teamRanks.length; i++) {
      ScoredTeam team = teamsSortedByScore.get(i);
      if (i == 0 || team.getScore().intValue() != teamsSortedByScore.get(i - 1).getScore().intValue()) {
        rank++;
      }
      teamRanks[i] = rank;
    }
    return teamRanks;
  }

  private void updatePlayerStats(Entry<IPlayer, de.gesundkrank.jskills.Rating> entry, GamePlayerStats stats) {
    de.gesundkrank.jskills.Rating newRating = entry.getValue();
    double newMean = newRating.getMean();
    double newDeviation = newRating.getStandardDeviation();

    stats.setRatingMeanAfter(newMean);
    stats.setRatingDeviationAfter(newDeviation);
  }

  private void updateRating(@Nullable Integer featuredModId, GamePlayerStats gameParticipant) {
    Account participant = gameParticipant.getPlayer();

    Rating rating = gameParticipant.getPlayer().getRatings().get(featuredModId);
    Assert.state(
      rating != null,
      () -> String.format("Rating has not been initialized for featured mod: %d and account: %s", featuredModId, participant)
    );

    rating.setMean(gameParticipant.getRatingMeanAfter());
    rating.setDeviation(gameParticipant.getRatingDeviationAfter());

    log.debug("New rating for featured mod '{}' and player '{}' is: {}", featuredModId, participant, rating);
  }

  @AllArgsConstructor
  @Getter
  private static class ScoredTeam extends Team {
    private final AtomicInteger score = new AtomicInteger();
    private final int id;
  }
}
