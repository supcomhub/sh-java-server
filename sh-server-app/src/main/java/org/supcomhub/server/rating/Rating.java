package org.supcomhub.server.rating;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.jetbrains.annotations.Nullable;
import org.supcomhub.server.common.jpa.AbstractIntegerIdEntity;
import org.supcomhub.server.security.Account;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "leaderboard_rating")
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString(of = {"mean", "deviation"})
// TODO rename class according to table name
public class Rating extends AbstractIntegerIdEntity {

  @Column(name = "mean")
  private double mean;

  @Column(name = "deviation")
  private double deviation;

  @Column(name = "leaderboard_id")
  @Nullable
  private Integer leaderboardId;

  @ManyToOne(optional = false)
  @JoinColumn(name = "account_id", updatable = false, insertable = false)
  private Account player;

  // FIXME implement
  @Transient
  private int rank;
}
