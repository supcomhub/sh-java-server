package org.supcomhub.server.matchmaker;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.supcomhub.server.common.ClientMessage;
import org.supcomhub.server.game.Faction;

import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateMatchRequest implements ClientMessage {

  private UUID requestId;
  private String title;
  private int mapVersionId;
  private int featuredModId;
  private List<Participant> participants;
  private LobbyMode lobbyMode;
  private int leaderboardId;

  public enum LobbyMode {
    DEFAULT, NONE
  }

  @Getter
  @Setter
  public static class Participant {
    private int id;
    private Faction faction;
    private int team;
    private String name;
    private int startSpot;
  }
}
