package org.supcomhub.server.matchmaker;

import lombok.Value;
import org.supcomhub.server.common.ServerMessage;

import java.util.UUID;

@Value
public class MatchCreatedResponse implements ServerMessage {
  UUID requestId;
  int gameId;
}
