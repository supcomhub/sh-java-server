package org.supcomhub.server.matchmaker;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.supcomhub.server.common.ClientMessage;


/**
 * Requests to cancel matchmaker search.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MatchMakerCancelRequest implements ClientMessage {
  private String poolName;
}
