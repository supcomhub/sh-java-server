package org.supcomhub.server.matchmaker;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.supcomhub.server.common.jpa.AbstractEntity;
import org.supcomhub.server.common.jpa.AbstractIntegerIdEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "matchmaker_pool")
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString(of = {"technicalName"})
public class MatchMakerPool extends AbstractIntegerIdEntity {

  @Column(name = "featured_mod_id")
  private int featuredModId;

  @Column(name = "leaderboard_id")
  private int leaderboardId;

  @Column(name = "technical_name")
  private String technicalName;
}
