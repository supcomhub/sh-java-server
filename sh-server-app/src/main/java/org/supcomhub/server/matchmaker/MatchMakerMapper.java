package org.supcomhub.server.matchmaker;

import org.mapstruct.Mapper;
import org.supcomhub.server.game.GameParticipant;
import org.supcomhub.server.matchmaker.CreateMatchRequest.Participant;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MatchMakerMapper {
  List<GameParticipant> map(List<Participant> participants);

  GameParticipant map(Participant participants);
}
