package org.supcomhub.server.matchmaker;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.supcomhub.server.common.ServerMessage;

import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MatchMakerResponse implements ServerMessage {

  /** A map of match maker pool names to the number of players who are currently searching for a game in this pool. */
  private Map<String, Integer> playersByPool;
}
