package org.supcomhub.server.matchmaker;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.supcomhub.server.common.ClientMessage;
import org.supcomhub.server.game.Faction;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MatchMakerSearchRequest implements ClientMessage {
  private Faction faction;
  private String poolName;
}
