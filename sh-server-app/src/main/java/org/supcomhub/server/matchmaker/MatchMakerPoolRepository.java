package org.supcomhub.server.matchmaker;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MatchMakerPoolRepository extends JpaRepository<MatchMakerPool, Integer> {
}
