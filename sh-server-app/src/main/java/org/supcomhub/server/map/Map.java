package org.supcomhub.server.map;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Immutable;
import org.supcomhub.server.common.jpa.AbstractIntegerIdEntity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "map")
@Immutable
@Getter
@Setter
@NoArgsConstructor
public class Map extends AbstractIntegerIdEntity {

}
