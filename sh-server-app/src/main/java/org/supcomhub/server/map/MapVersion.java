package org.supcomhub.server.map;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Immutable;
import org.supcomhub.server.common.jpa.AbstractIntegerIdEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "map_version")
@Immutable
@Getter
@Setter
@NoArgsConstructor
public class MapVersion extends AbstractIntegerIdEntity {

  /** The file name as stored in the database (e. g. {@code something.zip}). */
  @Column(name = "filename")
  private String filename;

  @Column(name = "ranked")
  private boolean ranked;

  @ManyToOne(optional = false)
  @JoinColumn(name = "map_id")
  private Map map;
}
