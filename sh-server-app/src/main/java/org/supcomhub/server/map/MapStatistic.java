package org.supcomhub.server.map;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Immutable;
import org.supcomhub.server.common.jpa.AbstractIntegerIdEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "map_statistic")
@Getter
@Immutable
@NoArgsConstructor
public class MapStatistic extends AbstractIntegerIdEntity {

  @Column(name = "times_played")
  private int timesPlayed;

}

