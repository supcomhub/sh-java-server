package org.supcomhub.server.map;

import com.google.common.annotations.VisibleForTesting;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.supcomhub.server.cache.CacheNames;
import org.supcomhub.server.config.ServerProperties;
import org.supcomhub.server.mod.FeaturedMod;
import org.supcomhub.server.mod.ModService;
import org.supcomhub.server.security.Account;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@Service
public class MapService {

  @VisibleForTesting
  static final String MAP_FILENAME_EXTENSION = ".zip";

  private final MapVersionRepository mapVersionRepository;
  private final Ladder1v1MapRepository ladder1v1MapRepository;
  private final Random random;
  private final ServerProperties serverProperties;
  private final ModService modService;

  public MapService(MapVersionRepository mapVersionRepository,
                    Ladder1v1MapRepository ladder1v1MapRepository,
                    ServerProperties serverProperties,
                    ModService modService) {
    this.mapVersionRepository = mapVersionRepository;
    this.ladder1v1MapRepository = ladder1v1MapRepository;
    this.serverProperties = serverProperties;
    this.modService = modService;
    this.random = new Random();
  }

  /**
   * @param mapName The name of the map to be hosted. This is the maps directory name, e.g. {@code SCMP_001}
   */
  @Cacheable(CacheNames.MAP_VERSIONS)
  public Optional<MapVersion> findMap(String mapName) {
    return mapVersionRepository.findByFilenameIgnoreCase(mapName + MAP_FILENAME_EXTENSION);
  }

  @Cacheable(CacheNames.MAP_VERSIONS)
  public Optional<MapVersion> findMap(int mapVersionId) {
    return mapVersionRepository.findById(mapVersionId);
  }

  public MapVersion getRandomLadderMap(Iterable<Account> players) {
    List<MapVersion> originalLadderMapPool = getLadder1v1Maps();
    List<MapVersion> modifiedLadderMapPool = new ArrayList<>(originalLadderMapPool);

    int lastMapsNotConsidered = serverProperties.getLadder1v1().getLastMapsNotConsidered();

    for (Account player : players) {
      modifiedLadderMapPool.removeAll(getRecentlyPlayedLadderMapVersions(player, lastMapsNotConsidered));
    }

    if (modifiedLadderMapPool.size() > 1) {
      return modifiedLadderMapPool.get(random.nextInt(modifiedLadderMapPool.size() - 1));
    }
    return originalLadderMapPool.get(random.nextInt(originalLadderMapPool.size() - 1));
  }

  @Transactional
  public List<MapVersion> getRecentlyPlayedLadderMapVersions(Account player, int limit) {
    Optional<FeaturedMod> ladder1v1 = modService.getLadder1v1Mod();
    if (ladder1v1.isEmpty()) {
      throw new IllegalStateException("Ladder 1v1 mod could not be found");
    }
    return ladder1v1MapRepository.findRecentlyPlayedLadderMapVersions(PageRequest.of(0, limit), player.getId(), ladder1v1.get().getId()).getContent();
  }

  private List<MapVersion> getLadder1v1Maps() {
    return ladder1v1MapRepository.findAll();
  }
}
