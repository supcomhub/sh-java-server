package org.supcomhub.server.config.security;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.supcomhub.server.player.AccountRepository;
import org.supcomhub.server.security.Account;
import org.supcomhub.server.security.EmailAddressService;
import org.supcomhub.server.security.ShUserDetails;
import org.supcomhub.server.security.UserRole;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@Slf4j
@ConditionalOnProperty(value = "sh-server.disable-authentication")
@Service
class UserCreatingAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

  private final AccountRepository accountRepository;
  private final EmailAddressService emailAddressService;
  private final PasswordEncoder passwordEncoder;

  UserCreatingAuthenticationProvider(AccountRepository accountRepository, EmailAddressService emailAddressService, PasswordEncoder passwordEncoder) {
    this.accountRepository = accountRepository;
    this.emailAddressService = emailAddressService;
    this.passwordEncoder = passwordEncoder;
    setPreAuthenticationChecks(new AccountBannedChecker());
  }

  private Account updatePassword(Account user, String password) {
    user.setPassword(passwordEncoder.encode(password));
    return accountRepository.save(user);
  }

  private Account createUser(String emailAddress, String credentials) {
    log.debug("User '{}' does not yet exist, creating", emailAddress);

    Account account = new Account();
    account.setDisplayName(emailAddress.split("@")[0]);
    account.setPassword(passwordEncoder.encode(credentials));
    account = accountRepository.save(account);

    account.setEmailAddresses(new HashSet<>(Collections.singleton(
      emailAddressService.create(emailAddress, account)
    )));

    return account;
  }

  @Override
  protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
    // Does nothing
  }

  @Override
  protected UserDetails retrieveUser(String emailAddress, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
    Account user = emailAddressService.findOwnerOfAddress(emailAddress)
      .map(u -> updatePassword(u, String.valueOf(authentication.getCredentials())))
      .orElseGet(() -> createUser(emailAddress, String.valueOf(authentication.getCredentials())));

    return new ShUserDetails(user, Set.of(
      new SimpleGrantedAuthority(UserRole.ADMINISTRATOR.getAuthority()),
      new SimpleGrantedAuthority(UserRole.MODERATOR.getAuthority()),
      new SimpleGrantedAuthority(UserRole.USER.getAuthority())
    ));
  }

  @Override
  public boolean supports(Class<?> authentication) {
    return UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication);
  }
}
