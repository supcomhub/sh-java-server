package org.supcomhub.server.config.security;

import org.springframework.boot.actuate.autoconfigure.security.servlet.EndpointRequest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.ObjectPostProcessor;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;

@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

  private final Optional<AuthenticationProvider> authenticationProvider;
  private final UserDetailsService userDetailsService;
  private final PasswordEncoder passwordEncoder;

  public WebSecurityConfig(Optional<AuthenticationProvider> authenticationProvider, UserDetailsService userDetailsService, PasswordEncoder passwordEncoder) {
    this.authenticationProvider = authenticationProvider;
    this.userDetailsService = userDetailsService;
    this.passwordEncoder = passwordEncoder;
  }

  private ObjectPostProcessor<DaoAuthenticationProvider> daoAuthenticationProviderPostProcessor() {
    return new ObjectPostProcessor<>() {
      @Override
      public <O extends DaoAuthenticationProvider> O postProcess(O object) {
        object.setPreAuthenticationChecks(new AccountBannedChecker());
        return object;
      }
    };
  }

  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth
      .userDetailsService(userDetailsService)
      .withObjectPostProcessor(daoAuthenticationProviderPostProcessor())
      .passwordEncoder(passwordEncoder);

    authenticationProvider.ifPresent(auth::authenticationProvider);
  }

  @Bean
  @Override
  public AuthenticationManager authenticationManagerBean() throws Exception {
    return super.authenticationManagerBean();
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http
      .authorizeRequests()
      .requestMatchers(EndpointRequest.toAnyEndpoint())
      .permitAll()
      .anyRequest().authenticated()
      .and().formLogin().and().httpBasic()
    ;
  }
}
