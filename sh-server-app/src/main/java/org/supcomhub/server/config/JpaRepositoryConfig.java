package org.supcomhub.server.config;

import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.supcomhub.server.game.ActiveGameRepository;
import org.supcomhub.server.player.OnlinePlayerRepository;

@EnableJpaRepositories(excludeFilters = @Filter(
  type = FilterType.ASSIGNABLE_TYPE,
  classes = {
    ActiveGameRepository.class,
    OnlinePlayerRepository.class
  }),
  basePackages = "org.supcomhub.server"
)
@Configuration
public class JpaRepositoryConfig {
}
