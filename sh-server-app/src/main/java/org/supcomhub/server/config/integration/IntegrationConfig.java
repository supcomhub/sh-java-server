package org.supcomhub.server.config.integration;

import com.google.common.collect.ImmutableMap;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.IntegrationMessageHeaderAccessor;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.context.IntegrationContextUtils;
import org.springframework.integration.core.GenericSelector;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.event.inbound.ApplicationEventListeningMessageProducer;
import org.springframework.integration.router.AbstractMappingMessageRouter;
import org.springframework.integration.router.AbstractMessageRouter;
import org.springframework.integration.router.PayloadTypeRouter;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.integration.transformer.AbstractTransformer;
import org.springframework.integration.transformer.MessageTransformationException;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHandlingException;
import org.springframework.messaging.MessagingException;
import org.supcomhub.server.avatar.SelectAvatarRequest;
import org.supcomhub.server.client.ClientConnection;
import org.supcomhub.server.client.ClientDisconnectedEvent;
import org.supcomhub.server.client.DisconnectClientRequest;
import org.supcomhub.server.client.PingReport;
import org.supcomhub.server.coop.CoopMissionCompletedReport;
import org.supcomhub.server.error.ErrorResponse;
import org.supcomhub.server.error.RequestException;
import org.supcomhub.server.game.AiOptionReport;
import org.supcomhub.server.game.ArmyOutcomeReport;
import org.supcomhub.server.game.ArmyScoreReport;
import org.supcomhub.server.game.BottleneckClearedReport;
import org.supcomhub.server.game.BottleneckReport;
import org.supcomhub.server.game.ClearSlotRequest;
import org.supcomhub.server.game.DesyncReport;
import org.supcomhub.server.game.DisconnectPeerRequest;
import org.supcomhub.server.game.EnforceRatingReport;
import org.supcomhub.server.game.GameChatMessageReport;
import org.supcomhub.server.game.GameEndedReport;
import org.supcomhub.server.game.GameModsReport;
import org.supcomhub.server.game.GameOptionReport;
import org.supcomhub.server.game.GameStateReport;
import org.supcomhub.server.game.HostGameRequest;
import org.supcomhub.server.game.JoinGameRequest;
import org.supcomhub.server.game.MutuallyAgreedDrawRequest;
import org.supcomhub.server.game.PlayerDisconnectedReport;
import org.supcomhub.server.game.PlayerOptionReport;
import org.supcomhub.server.game.RehostRequest;
import org.supcomhub.server.game.RestoreGameSessionRequest;
import org.supcomhub.server.game.TeamKillReportReport;
import org.supcomhub.server.game.VerifyPlayerReport;
import org.supcomhub.server.ice.IceMessage;
import org.supcomhub.server.ice.IceServersRequest;
import org.supcomhub.server.integration.ChannelNames;
import org.supcomhub.server.integration.MessageHeaders;
import org.supcomhub.server.matchmaker.CreateMatchRequest;
import org.supcomhub.server.matchmaker.MatchMakerCancelRequest;
import org.supcomhub.server.matchmaker.MatchMakerSearchRequest;
import org.supcomhub.server.protocol.v2.dto.client.DisconnectedClientMessage;
import org.supcomhub.server.social.UpdateSocialRelationRequest;
import org.supcomhub.server.stats.ArmyStatisticsReport;

import java.util.Collections;
import java.util.List;

@Configuration
@IntegrationComponentScan("org.supcomhub.server.integration")
public class IntegrationConfig {

  /**
   * Reads messages from the standard client inbound channel. Since messages are resequenced, all messages must specify
   * the header {@link IntegrationMessageHeaderAccessor#SEQUENCE_NUMBER}.
   */
  @Bean
  public IntegrationFlow inboundFlow() {
    return IntegrationFlows
      .from(ChannelNames.CLIENT_INBOUND)
      // FIXME websocket messages don't have a sequence number yet
//      .resequence(spec -> spec.releasePartialSequences(true))
      .channel(ChannelNames.INBOUND_DISPATCH)
      .get();
  }

  @Bean
  public IntegrationFlow dispatchFlow() {
    return IntegrationFlows
      .from(ChannelNames.INBOUND_DISPATCH)
      .route(inboundRouter())
      .get();
  }

  /**
   * Reads messages from the client outbound channel and sends it to the target adapter's channel.
   */
  @Bean
  public IntegrationFlow outboundFlow() {
    return IntegrationFlows
      .from(ChannelNames.CLIENT_OUTBOUND)
      .route(outboundAdapterRouter())
      .get();
  }

  /**
   * Reads messages from the broadcast channel, enriches them with a "broadcast" header and routes it to all adapter
   * channels.
   */
  @Bean
  public IntegrationFlow broadcastFlow() {
    return IntegrationFlows
      .from(ChannelNames.CLIENT_OUTBOUND_BROADCAST)
      .enrichHeaders(ImmutableMap.of(MessageHeaders.BROADCAST, true))
      .routeToRecipients(spec -> spec
        .recipient(ChannelNames.WEB_OUTBOUND)
      )
      .get();
  }

  /**
   * Subscribes to the errorChannel and transforms any {@link RequestException} into and {@link ErrorResponse}.
   */
  @Bean
  public IntegrationFlow errorFlow() {
    return IntegrationFlows
      .from(IntegrationContextUtils.ERROR_CHANNEL_BEAN_NAME)
      .filter(requestExceptionFilter())
      .transform(requestExceptionTransformer())
      .channel(ChannelNames.CLIENT_OUTBOUND)
      .get();
  }

  /**
   * Turns specific application events into messages.
   */
  @Bean
  public ApplicationEventListeningMessageProducer applicationEventListeningMessageProducer() {
    ApplicationEventListeningMessageProducer producer = new ApplicationEventListeningMessageProducer();
    producer.setEventTypes(
      ClientDisconnectedEvent.class
    );
    producer.setOutputChannelName(ChannelNames.INBOUND_DISPATCH);
    return producer;
  }

  /**
   * Routes response messages to the appropriate outbound adapter.
   */
  private AbstractMessageRouter outboundAdapterRouter() {
    return new AbstractMappingMessageRouter() {
      @Override
      protected List<Object> getChannelKeys(Message<?> message) {
        switch (message.getHeaders().get(MessageHeaders.CLIENT_CONNECTION, ClientConnection.class).getProtocol()) {
          case V2_JSON_UTF_8:
            return Collections.singletonList(ChannelNames.WEB_OUTBOUND);
          default:
            throw new UnsupportedOperationException("Only legacy outbound route is currently specified");
        }
      }
    };
  }

  /**
   * Routes request messages to their corresponding request channel.
   */
  private PayloadTypeRouter inboundRouter() {
    PayloadTypeRouter router = new PayloadTypeRouter();
    router.setChannelMapping(HostGameRequest.class.getName(), ChannelNames.HOST_GAME_REQUEST);
    router.setChannelMapping(JoinGameRequest.class.getName(), ChannelNames.JOIN_GAME_REQUEST);
    router.setChannelMapping(GameStateReport.class.getName(), ChannelNames.UPDATE_GAME_STATE_REQUEST);
    router.setChannelMapping(GameOptionReport.class.getName(), ChannelNames.GAME_OPTION_REQUEST);
    router.setChannelMapping(PlayerOptionReport.class.getName(), ChannelNames.PLAYER_OPTION_REQUEST);
    router.setChannelMapping(VerifyPlayerReport.class.getName(), ChannelNames.VERIFY_PLAYER_REQUEST);
    router.setChannelMapping(ClearSlotRequest.class.getName(), ChannelNames.CLEAR_SLOT_REQUEST);
    router.setChannelMapping(AiOptionReport.class.getName(), ChannelNames.AI_OPTION_REQUEST);
    router.setChannelMapping(DesyncReport.class.getName(), ChannelNames.DESYNC_REPORT);
    router.setChannelMapping(GameModsReport.class.getName(), ChannelNames.GAME_MODS_REPORT);
    router.setChannelMapping(ArmyScoreReport.class.getName(), ChannelNames.ARMY_SCORE_REPORT);
    router.setChannelMapping(ArmyOutcomeReport.class.getName(), ChannelNames.ARMY_OUTCOME_REPORT);
    router.setChannelMapping(CoopMissionCompletedReport.class.getName(), ChannelNames.OPERATION_COMPLETE_REPORT);
    router.setChannelMapping(ArmyStatisticsReport.class.getName(), ChannelNames.GAME_STATISTICS_REPORT);
    router.setChannelMapping(TeamKillReportReport.class.getName(), ChannelNames.TEAM_KILL_REPORT_REPORT);
    router.setChannelMapping(DisconnectPeerRequest.class.getName(), ChannelNames.DISCONNECT_PEER_REQUEST);
    router.setChannelMapping(DisconnectClientRequest.class.getName(), ChannelNames.DISCONNECT_CLIENT_REQUEST);
    router.setChannelMapping(MatchMakerSearchRequest.class.getName(), ChannelNames.MATCH_MAKER_SEARCH_REQUEST);
    router.setChannelMapping(MatchMakerCancelRequest.class.getName(), ChannelNames.MATCH_MAKER_CANCEL_REQUEST);
    router.setChannelMapping(IceServersRequest.class.getName(), ChannelNames.ICE_SERVERS_REQUEST);
    router.setChannelMapping(IceMessage.class.getName(), ChannelNames.ICE_MESSAGE);
    router.setChannelMapping(RestoreGameSessionRequest.class.getName(), ChannelNames.RESTORE_GAME_SESSION_REQUEST);
    router.setChannelMapping(MutuallyAgreedDrawRequest.class.getName(), ChannelNames.MUTUALLY_AGREED_DRAW_REQUEST);
    router.setChannelMapping(ClientDisconnectedEvent.class.getName(), ChannelNames.CLIENT_DISCONNECTED_EVENT);
    router.setChannelMapping(SelectAvatarRequest.class.getName(), ChannelNames.SELECT_AVATAR);
    router.setChannelMapping(CreateMatchRequest.class.getName(), ChannelNames.CREATE_MATCH_REQUEST);
    router.setChannelMapping(UpdateSocialRelationRequest.class.getName(), ChannelNames.UPDATE_SOCIAL_RELATION_REQUEST);
    router.setChannelMapping(PlayerDisconnectedReport.class.getName(), ChannelNames.DISCONNECTED_REPORT);
    router.setChannelMapping(GameEndedReport.class.getName(), ChannelNames.GAME_ENDED_REPORT);
    // These are sent by the game but currently, the server has no interest in it.
    router.setChannelMapping(BottleneckClearedReport.class.getName(), IntegrationContextUtils.NULL_CHANNEL_BEAN_NAME);
    router.setChannelMapping(BottleneckReport.class.getName(), IntegrationContextUtils.NULL_CHANNEL_BEAN_NAME);
    router.setChannelMapping(DisconnectedClientMessage.class.getName(), IntegrationContextUtils.NULL_CHANNEL_BEAN_NAME);
    router.setChannelMapping(GameChatMessageReport.class.getName(), IntegrationContextUtils.NULL_CHANNEL_BEAN_NAME);
    // This should be handled by the client rather than being forwarded to the server
    router.setChannelMapping(RehostRequest.class.getName(), IntegrationContextUtils.NULL_CHANNEL_BEAN_NAME);
    // This is sent by the game but deprecated, so discarded.
    router.setChannelMapping(EnforceRatingReport.class.getName(), IntegrationContextUtils.NULL_CHANNEL_BEAN_NAME);
    // The message itself doesn't matter, receiving it already triggered ClientConnectionChannelInterceptor
    router.setChannelMapping(PingReport.class.getName(), IntegrationContextUtils.NULL_CHANNEL_BEAN_NAME);
    return router;
  }

  /**
   * Filter that lets {@link RequestException RequestExceptions} pass only.
   */
  private GenericSelector<Object> requestExceptionFilter() {
    return (Object source) -> {
      if (!(source instanceof MessagingException)) {
        return false;
      }
      MessagingException messagingException = (MessagingException) source;
      return messagingException.getCause() instanceof RequestException
        || messagingException.getCause() != null && messagingException.getCause().getCause() instanceof RequestException;
    };
  }

  /**
   * Transformer that transforms a {@link MessageHandlingException} into and {@link ErrorResponse}.
   */
  private AbstractTransformer requestExceptionTransformer() {
    return new AbstractTransformer() {
      @Override
      protected Object doTransform(Message<?> message) {
        MessagingException messageException = (MessagingException) message.getPayload();
        Message<?> failedMessage = messageException.getFailedMessage();

        // TODO try to make more intuitive? Meanwhile, look at requestExceptionFilter() to understand.
        RequestException requestException;
        if (messageException instanceof MessageTransformationException) {
          requestException = (RequestException) messageException.getCause().getCause();
        } else {
          requestException = (RequestException) messageException.getCause();
        }

        MessageBuilder<ErrorResponse> builder = MessageBuilder
          .withPayload(new ErrorResponse(requestException.getErrorCode(), requestException.getRequestId(), requestException.getArgs()))
          .copyHeaders(message.getHeaders());
        builder.setHeader(MessageHeaders.CLIENT_CONNECTION, failedMessage.getHeaders().get(MessageHeaders.CLIENT_CONNECTION, ClientConnection.class));

        return builder.build();
      }
    };
  }
}
