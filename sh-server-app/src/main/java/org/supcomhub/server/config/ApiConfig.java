package org.supcomhub.server.config;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;
import org.springframework.web.client.RestTemplate;
import org.supcomhub.server.api.JsonApiMessageConverter;
import org.supcomhub.server.config.ServerProperties.Api;

import java.io.IOException;

/** Configuration for SH-API access. */
@Configuration
public class ApiConfig {

  @Bean
  public RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder, JsonApiMessageConverter jsonApiMessageConverter, ServerProperties properties) throws IOException {
    Api api = properties.getApi();

    ClientCredentialsResourceDetails resourceDetails = new ClientCredentialsResourceDetails();
    resourceDetails.setClientId(api.getClientId());
    resourceDetails.setClientSecret(api.getClientSecret());
    resourceDetails.setAccessTokenUri(api.getAccessTokenUri());

    return restTemplateBuilder
      .additionalMessageConverters(jsonApiMessageConverter)
      .rootUri(api.getBaseUrl())
      .configure(new OAuth2RestTemplate(resourceDetails));
  }
}
