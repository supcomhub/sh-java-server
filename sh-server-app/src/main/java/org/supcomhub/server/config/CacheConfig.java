package org.supcomhub.server.config;

import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.caffeine.CaffeineCache;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.supcomhub.server.cache.CacheNames;

import java.util.Arrays;

import static com.github.benmanes.caffeine.cache.Caffeine.newBuilder;
import static java.util.concurrent.TimeUnit.MINUTES;

@Configuration
@EnableCaching
public class CacheConfig {

  @Bean
  public CacheManager cacheManager() {
    SimpleCacheManager cacheManager = new SimpleCacheManager();
    cacheManager.setCaches(Arrays.asList(
      new CaffeineCache(CacheNames.RANKED_MODS, newBuilder().expireAfterWrite(5, MINUTES).build()),
      new CaffeineCache(CacheNames.MAP_VERSIONS, newBuilder().expireAfterAccess(5, MINUTES).build())
    ));
    return cacheManager;
  }
}
