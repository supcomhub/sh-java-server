package org.supcomhub.server.config;

import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.data.map.repository.config.EnableMapRepositories;
import org.supcomhub.server.game.ActiveGameRepository;
import org.supcomhub.server.player.OnlinePlayerRepository;

@EnableMapRepositories(includeFilters = @Filter(
  type = FilterType.ASSIGNABLE_TYPE,
  classes = {
    ActiveGameRepository.class,
    OnlinePlayerRepository.class
  }),
  basePackages = "org.supcomhub.server"
)
@Configuration
public class MapRepositoryConfig {
}
