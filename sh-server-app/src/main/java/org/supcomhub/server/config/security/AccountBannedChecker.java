package org.supcomhub.server.config.security;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsChecker;
import org.springframework.util.Assert;
import org.supcomhub.server.security.ShUserDetails;

@Slf4j
public class AccountBannedChecker implements UserDetailsChecker {

  @Override
  public void check(UserDetails user) {
    Assert.isInstanceOf(ShUserDetails.class, user);

    ShUserDetails shUserDetails = (ShUserDetails) user;

    if (!shUserDetails.isAccountNonLocked()) {
      log.debug("User '{}' is locked", shUserDetails.getUsername());
      throw new ShLockedException(shUserDetails);
    }
  }
}
