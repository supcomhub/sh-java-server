package org.supcomhub.server.config.security;

import lombok.EqualsAndHashCode;
import lombok.Value;
import org.springframework.security.authentication.LockedException;
import org.supcomhub.server.security.ShUserDetails;

@EqualsAndHashCode(callSuper = true)
@Value
public class ShLockedException extends LockedException {
  ShUserDetails shUserDetails;

  public ShLockedException(ShUserDetails shUserDetails) {
    super("Account " + shUserDetails.getUsername() + " is locked");
    this.shUserDetails = shUserDetails;
  }
}
