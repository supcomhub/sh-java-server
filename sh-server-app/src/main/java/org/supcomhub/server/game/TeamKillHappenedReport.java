package org.supcomhub.server.game;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.supcomhub.server.common.ClientMessage;

import java.time.Duration;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TeamKillHappenedReport implements ClientMessage {
  private Duration time;
  private int killerId;
}
