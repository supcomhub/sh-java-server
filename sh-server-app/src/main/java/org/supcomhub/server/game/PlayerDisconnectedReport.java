package org.supcomhub.server.game;

import lombok.Value;
import org.supcomhub.server.common.ClientMessage;

@Value
public class PlayerDisconnectedReport implements ClientMessage {
  int playerId;
}
