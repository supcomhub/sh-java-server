package org.supcomhub.server.game;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.supcomhub.server.common.ClientMessage;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DisconnectedReport implements ClientMessage {
  private int playerId;
}
