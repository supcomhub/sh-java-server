package org.supcomhub.server.game;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.supcomhub.server.common.ClientMessage;

import java.net.URL;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class VerifyPlayerReport implements ClientMessage {
  private int id;
  private String name;
  private int rank;
  private String country;
  private URL avatarUrl;
  private String avatarDescription;
}
