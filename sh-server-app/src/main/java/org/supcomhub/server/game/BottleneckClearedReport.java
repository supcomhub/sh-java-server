package org.supcomhub.server.game;

import org.supcomhub.server.common.ClientMessage;

public enum BottleneckClearedReport implements ClientMessage {
  INSTANCE
}
