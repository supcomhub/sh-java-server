package org.supcomhub.server.game;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.supcomhub.server.common.ClientMessage;

import java.time.Duration;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TeamKillReportReport implements ClientMessage {
  private Duration time;
  @Deprecated(forRemoval = true)
  private int victimId;
  @Deprecated(forRemoval = true)
  private String victimName;
  private int killerId;
  @Deprecated(forRemoval = true)
  private String killerName;
}
