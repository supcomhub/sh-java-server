package org.supcomhub.server.game;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Immutable;
import org.supcomhub.server.common.jpa.AbstractIntegerIdEntity;
import org.supcomhub.server.security.Account;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.Instant;

@Entity
@Table(name = "game_participant")
@Immutable
@NoArgsConstructor
@Getter
@Setter
@Cacheable
// TODO rename to GameParticipant (but this class already exist, check how to name and use them)
public class GamePlayerStats extends AbstractIntegerIdEntity {

  @ManyToOne(optional = false)
  @JoinColumn(name = "participant_id")
  private Account player;

  @Column(name = "participant_id", insertable = false, updatable = false)
  private int participantId;

  @Column(name = "faction")
  private int faction;

  @Column(name = "color")
  private int color;

  @Column(name = "team")
  private int team;

  @Column(name = "start_spot")
  private int startSpot;

  @Column(name = "rating_mean_before")
  private double ratingMeanBefore;

  @Column(name = "rating_deviation_before")
  private double ratingDeviationBefore;

  @Column(name = "rating_mean_after")
  private Double ratingMeanAfter;

  @Column(name = "rating_deviation_after")
  private Double ratingDeviationAfter;

  @Column(name = "score")
  private Integer score;

  @Column(name = "finish_time")
  private Instant finishTime;

  @ManyToOne(optional = false)
  @JoinColumn(name = "game_id")
  private Game game;

  public GamePlayerStats(Game game, Account player) {
    this.game = game;
    this.player = player;
    this.participantId = player.getId();
  }
}
