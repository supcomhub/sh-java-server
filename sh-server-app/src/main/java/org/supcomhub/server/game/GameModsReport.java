package org.supcomhub.server.game;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.supcomhub.server.common.ClientMessage;

import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GameModsReport implements ClientMessage {
  /**
   * The number of active mods. Required because of current game code implementation.
   *
   * @deprecated the server can just count {@code modUuids}, there is no need for the game to tell how many mods are in
   * that list.
   */
  @Deprecated(forRemoval = true)
  private int activatedMods;

  private List<UUID> modUuids;
}
