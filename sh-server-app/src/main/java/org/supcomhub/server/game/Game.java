package org.supcomhub.server.game;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.keyvalue.annotation.KeySpace;
import org.supcomhub.server.common.jpa.AbstractIntegerIdEntity;
import org.supcomhub.server.common.jpa.PostgreSQLEnumType;
import org.supcomhub.server.map.MapVersion;
import org.supcomhub.server.mod.ModVersion;
import org.supcomhub.server.security.Account;
import org.supcomhub.server.stats.ArmyStatistics;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapKey;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicInteger;

@Entity
@Table(name = "game")
@Setter
@Getter
@ToString(of = {"title", "state"}, includeFieldNames = false, callSuper = true)
@KeySpace("game")
@TypeDef(name = PostgreSQLEnumType.TYPE_NAME, typeClass = PostgreSQLEnumType.class)
public class Game extends AbstractIntegerIdEntity {

  // TODO javadoc
  @Transient
  private Optional<List<GameParticipant>> presetParticipants;

  /**
   * A key-value map of gamespecific options, like {@code "PrebuiltUnits" -> "Off"}.
   */
  @Transient
  private final Map<String, Object> options;

  @Transient
  private final AtomicInteger desyncCounter;

  /**
   * Maps player IDs to a map of {@code armyId -> result} reported by this player.
   */
  @Transient
  private final Map<Integer, Map<Integer, ArmyResult>> reportedArmyResults;

  @Transient
  private final List<ArmyStatistics> armyStatistics;

  /**
   * Returns the players who are currently connected to the game (including observers), mapped by their player ID.
   */
  @Transient
  private final Map<Integer, Account> connectedPlayers;

  /**
   * Maps player IDs to key-value option maps, like {@code 1234 -> "Color" -> 1 }
   */
  // TODO wrap this in an accessor class
  @Transient
  private final Map<Integer, Map<String, Object>> playerOptions;

  /**
   * Set of player ids who accepted mutual draw
   */
  @Transient
  private final Set<Integer> mutuallyAcceptedDrawPlayerIds;

  /**
   * Maps AI names to key-value option maps, like {@code "Julian (AI: Rufus)" -> "Color" -> 1 }. Note that the game
   * doesn't always send the AI options with the same AI name. During lobby mode, it sends start spot, color etc. for
   * "AI: Easy" but when the game starts, it sends the Army ID as "Julian (AI: Easy)".
   */
  @Transient
  private final Map<String, Map<String, Object>> aiOptions;

  @Transient
  private final List<ModVersion> simMods;

  @Transient
  private Integer minRank;

  @Transient
  private Integer maxRank;

  @Column(name = "start_time")
  private Instant startTime;

  @Column(name = "end_time")
  private Instant endTime;

  @Column(name = "victory_condition")
  @Enumerated(EnumType.STRING)
  @Type(type = PostgreSQLEnumType.TYPE_NAME)
  private VictoryCondition victoryCondition;

  @Column(name = "featured_mod_id")
  private int featuredModId;

  @JoinColumn(name = "leaderboard_id")
  private Integer leaderboardId;

  @ManyToOne(optional = false, fetch = FetchType.LAZY)
  @JoinColumn(name = "host_id")
  private Account host;

  /**
   * {@code null} if the map is not known by the server.
   *
   * @see #getMapFolderName()
   */
  @ManyToOne(optional = false)
  @JoinColumn(name = "map_version_id")
  @Nullable
  private MapVersion mapVersion;

  @Column(name = "name")
  private String title;

  @Column(name = "validity")
  @Enumerated(EnumType.STRING)
  @Type(type = PostgreSQLEnumType.TYPE_NAME)
  private Validity validity;

  /**
   * Mapped by player ID. Meant to be set just before game start.
   */
  @OneToMany(mappedBy = "game", cascade = CascadeType.ALL)
  @MapKey(name = "participantId")
  private Map<Integer, GamePlayerStats> participants;

  @Transient
  private String featuredModName;

  @Transient
  private GameState state;

  @Transient
  private String password;

  /**
   * Since some maps are unknown by the server (e.g. in-develop versions from map creators), the literal map folder name
   * (as sent by the game, e.g. {@code SCMP_001}) is kept. This is also the one that needs to be sent to participants.
   */
  @Transient
  private String mapFolderName;

  @Transient
  private int maxPlayers;

  @Transient
  private boolean ratingEnforced;

  @Transient
  private GameVisibility gameVisibility;

  @Transient
  private boolean mutuallyAgreedDraw;

  /**
   * Future that is completed as soon as the game is ready to be joined. A game may never complete this future, for
   * instance if the host's game crashes before entering lobby mode, so never wait without a timeout.
   */
  @Transient
  private CompletableFuture<Game> joinableFuture;

  @Transient
  private Collection<Integer> playerIdsWhoReportedGameEnd;

  @Transient
  private LobbyMode lobbyMode;

  public Game(int id) {
    this();
    setId(id);
  }

  public Game() {
    state = GameState.INITIALIZING;
    presetParticipants = Optional.empty();
    playerOptions = new HashMap<>();
    options = new HashMap<>();
    aiOptions = new HashMap<>();
    reportedArmyResults = new HashMap<>();
    mutuallyAcceptedDrawPlayerIds = new HashSet<>();
    armyStatistics = new ArrayList<>();
    participants = new HashMap<>();
    simMods = new ArrayList<>();
    connectedPlayers = new HashMap<>();
    desyncCounter = new AtomicInteger();
    validity = Validity.VALID;
    gameVisibility = GameVisibility.PUBLIC;
    victoryCondition = VictoryCondition.DEMORALIZATION;
    joinableFuture = new CompletableFuture<>();
    playerIdsWhoReportedGameEnd = new ArrayList<>();
  }

  public void replaceArmyStatistics(List<ArmyStatistics> newList) {
    synchronized (armyStatistics) {
      armyStatistics.clear();
      armyStatistics.addAll(newList);
    }
  }

  /**
   * Returns an unmodifiable list of army statistics.
   */
  public List<ArmyStatistics> getArmyStatistics() {
    return Collections.unmodifiableList(armyStatistics);
  }

  public Game setState(GameState state) {
    GameState.verifyTransition(this.state, state);
    this.state = state;
    if (state == GameState.OPEN) {
      joinableFuture.complete(this);
    }
    return this;
  }
}
