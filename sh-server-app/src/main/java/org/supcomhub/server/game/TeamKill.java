package org.supcomhub.server.game;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.supcomhub.server.common.jpa.AbstractIntegerIdEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "teamkill_report")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TeamKill extends AbstractIntegerIdEntity {

  @Column(name = "teamkiller_id")
  private int teamKiller;

  @Column(name = "victim_id")
  private int victim;

  @Column(name = "game_id")
  private int gameId;

  @Column(name = "game_time")
  private int gameTime;

}
