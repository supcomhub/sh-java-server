package org.supcomhub.server.game;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import org.supcomhub.server.common.ServerMessage;

/**
 * Tells the Client to start a game process.
 */
@Getter
@AllArgsConstructor
public class StartGameProcessResponse implements ServerMessage {

  private final String mod;
  private final int gameId;

  /** Only set if the server decides which map will be played, e.g. in leaderboard games. */
  private final String mapFolderName;
  private final LobbyMode lobbyMode;
  private final Faction faction;
  @NonNull
  private final String name;
  private final Integer expectedPlayers;
  private final int team;
  private final Integer mapPosition;
}
