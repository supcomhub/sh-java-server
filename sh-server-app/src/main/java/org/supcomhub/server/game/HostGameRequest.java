package org.supcomhub.server.game;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.supcomhub.server.common.ClientMessage;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HostGameRequest implements ClientMessage {

  private String map;
  private String title;
  private String featuredModName;
  private String password;
  private GameVisibility visibility;
  private Integer minRank;
  private Integer maxRank;
}
