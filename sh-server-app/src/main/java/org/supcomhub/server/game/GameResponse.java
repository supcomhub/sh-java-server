package org.supcomhub.server.game;

import lombok.Data;
import lombok.Value;
import org.supcomhub.server.client.GameResponses;
import org.supcomhub.server.common.ServerMessage;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

/**
 * Sends game information to the client. Multiple objects should be wrapped in a {@link GameResponses}.
 */
@Data
public class GameResponse implements ServerMessage {

  private final int id;
  private final String title;
  private final GameVisibility gameVisibility;
  private final boolean passwordProtected;
  private final GameState state;
  private final FeaturedMod featuredMod;
  private final List<SimMod> simMods;
  private final String technicalMapName;
  private final GameParticipant host;
  private final List<GameParticipant> players;
  private final int maxPlayers;
  private final Instant startTime;
  private final Integer minRank;
  private final Integer maxRank;
  private final Integer leaderboardId;

  @Value
  public static class GameParticipant {
    int id;
    String name;
    int team;
  }

  @Value
  public static class SimMod {
    UUID uuid;
    String displayName;
  }

  @Value
  public static class FeaturedMod {
    String name;
    int version;
  }
}
