package org.supcomhub.server.game;

import lombok.experimental.UtilityClass;

@UtilityClass
public class PlayerOptions {

  public static final String OPTION_START_SPOT = "StartSpot";
  public static final String OPTION_FACTION = "Faction";
  public static final String OPTION_COLOR = "Color";
  public static final String OPTION_ARMY = "Army";
  public static final String OPTION_TEAM = "Team";
}
