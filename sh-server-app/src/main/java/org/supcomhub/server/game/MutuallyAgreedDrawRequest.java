package org.supcomhub.server.game;

import org.supcomhub.server.common.ClientMessage;

/**
 * Sent by client when player in game requests mutual draw
 */
public enum MutuallyAgreedDrawRequest implements ClientMessage {
  INSTANCE
}
