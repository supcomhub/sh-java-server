package org.supcomhub.server.game;

import org.supcomhub.server.common.ClientMessage;

/**
 * Sent by the game to enforce rating, even though the minimum game time has not yet been reached.
 *
 * @deprecated since it's none of the game's business to decide whether or not a game should be rated, this will be
 * removed in a future version.
 */
@Deprecated
public enum EnforceRatingReport implements ClientMessage {
  INSTANCE
}
