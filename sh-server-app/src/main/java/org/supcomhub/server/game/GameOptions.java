package org.supcomhub.server.game;

import lombok.experimental.UtilityClass;

@UtilityClass
public class GameOptions {

  public static final String OPTION_FOG_OF_WAR = "FogOfWar";
  public static final String OPTION_CHEATS_ENABLED = "CheatsEnabled";
  public static final String OPTION_PREBUILT_UNITS = "PrebuiltUnits";
  public static final String OPTION_NO_RUSH = "NoRushOption";
  public static final String OPTION_RESTRICTED_CATEGORIES = "RestrictedCategories";
  public static final String OPTION_SLOTS = "Slots";
  public static final String OPTION_SCENARIO_FILE = "ScenarioFile";
  public static final String OPTION_TITLE = "Title";
  public static final String OPTION_TEAM_LOCK = "TeamLock";
  public static final String OPTION_VICTORY_CONDITION = VictoryCondition.GAME_OPTION_NAME;
}
