package org.supcomhub.server.game;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.supcomhub.server.common.ClientMessage;

/**
 * Requests the disconnection of a player from his/her current game.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DisconnectPeerRequest implements ClientMessage {
  private int playerId;
}
