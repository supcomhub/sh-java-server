package org.supcomhub.server.game;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.supcomhub.server.avatar.Avatar;
import org.supcomhub.server.avatar.AvatarService;
import org.supcomhub.server.client.ClientService;
import org.supcomhub.server.error.ErrorCode;
import org.supcomhub.server.error.Requests;
import org.supcomhub.server.player.PlayerService;
import org.supcomhub.server.rating.Rating;
import org.supcomhub.server.security.Account;

import java.net.URL;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * When players joins a game, they send their own information (like name and rating) to other players. This information
 * can, therefore, not be trusted. Players can send the information they received from others to this service in order
 * to have it verified. If this service receives multiple reports of incorrect data, it will tell all peers to
 * disconnect the player who spoofs its data.
 */
@Service
@Slf4j
public class SpoofDetectorService {

  private final PlayerService playerService;
  private final ClientService clientService;
  private final AvatarService avatarService;

  public SpoofDetectorService(PlayerService playerService, ClientService clientService, AvatarService avatarService) {
    this.playerService = playerService;
    this.clientService = clientService;
    this.avatarService = avatarService;
  }

  public void verifyPlayer(Account reporter, int reporteeId, String name, int rank, String country, URL avatarUrl, String avatarDescription) {
    Game reporterGame = reporter.getCurrentGame();
    Requests.verify(reporterGame != null, ErrorCode.THIS_PLAYER_NOT_IN_GAME);

    Optional<Account> reporteeOptional = playerService.getOnlinePlayer(reporteeId);
    Requests.verify(reporteeOptional.isPresent(), ErrorCode.PLAYER_NOT_ONLINE, reporteeId);
    Account reportee = reporteeOptional.get();

    Game reporteeGame = reportee.getCurrentGame();
    Requests.verify(reporteeGame != null, ErrorCode.OTHER_PLAYER_NOT_IN_GAME, reporteeId);

    Requests.verify(reporterGame.equals(reporteeGame), ErrorCode.NOT_SAME_GAME, reportee);

    boolean passesValidation = isNameCorrect(reporter, name, reportee)
      && isRatingCorrect(reporter, rank, reportee)
      && isCountryCorrect(reporter, country, reportee)
      && isAvatarDataCorrect(reporter, avatarUrl, avatarDescription, reportee);

    if (!passesValidation) {
      reportee.getFraudReporterIds().add(reporter.getId());

      int reportedFrauds = reportee.getFraudReporterIds().size();
      if (reportedFrauds > 1 && reportedFrauds >= reporterGame.getConnectedPlayers().size() / 2) {
        Collection<Account> peers = reporteeGame.getConnectedPlayers().values().stream()
          .filter(player -> !Objects.equals(player.getId(), reporteeId))
          .collect(Collectors.toList());

        clientService.disconnectPlayerFromGame(reporteeId, peers);
      }
    }
  }

  private boolean isAvatarDataCorrect(Account reporter, URL avatarUrl, String avatarDescription, Account reportee) {
    Optional<Avatar> optionalAvatar = avatarService.getCurrentAvatar(reportee);
    if (optionalAvatar.isEmpty()) {
      return true;
    }
    Avatar avatar = optionalAvatar.get();
    if (!Objects.equals(avatarUrl, avatar.getUrl())) {
      log.debug("Avatar URL '{}' of player '{}' does not match in-game URL '{}' as reported by player '{}'",
        avatarUrl, reportee, avatar.getUrl(), reporter);
      return false;
    }
    if (!Objects.equals(avatarDescription, avatar.getDescription())) {
      log.debug("Avatar description '{}' of player '{}' does not match in-game description '{}' as reported by player '{}'",
        avatarDescription, reportee, avatar.getDescription(), reporter);
      return false;
    }
    return true;
  }

  private boolean isCountryCorrect(Account reporter, String country, Account reportee) {
    if (!Objects.equals(country, reportee.getCountry())) {
      log.debug("Country '{}' of player '{}' does not match in-game country '{}' as reported by player '{}'",
        reportee.getCountry(), reportee, country, reporter);
      return false;
    }
    return true;
  }

  private boolean isRatingCorrect(Account reporter, int rank, Account reportee) {
    Rating reporteeRating = reportee.getRatingWithinCurrentGame();

    if (!Objects.equals(rank, reporteeRating.getRank())) {
      log.debug("Rank '{}' of player '{}' does not match in-game rank '{}' as reported by player '{}'",
        reporteeRating.getRank(), reporteeRating.getDeviation(), reportee, rank, reporter);
      return false;
    }
    return true;
  }

  private boolean isNameCorrect(Account reporter, String name, Account reportee) {
    if (!Objects.equals(name, reportee.getDisplayName())) {
      log.debug("Name of player '{}' does not match in-game name '{}' as reported by player '{}'", reportee, name, reporter);
      return false;
    }
    return true;
  }
}
