package org.supcomhub.server.game;

import lombok.Data;
import org.supcomhub.server.common.ServerMessage;

@Data
public class HostGameResponse implements ServerMessage {
  private final String mapFilename;
}
