package org.supcomhub.server.game;

import org.supcomhub.server.common.ClientMessage;

/**
 * Sent by the client whenever a desynchronization of the game state occurred.
 */
public enum DesyncReport implements ClientMessage {
  INSTANCE
}
