package org.supcomhub.server.game;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.supcomhub.server.common.ServerMessage;

import java.util.Set;

/**
 * Results of a finished game.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class GameResultResponse implements ServerMessage {
  private int gameId;
  private boolean draw;
  private Set<PlayerResult> playerResults;

  @Getter
  @Setter
  public static class PlayerResult {
    private int playerId;
    private boolean winner;
    private boolean acuKilled;
  }
}
