package org.supcomhub.server.game;

import lombok.Value;

@Value
public class GameClosedEvent {
  int gameId;
}
