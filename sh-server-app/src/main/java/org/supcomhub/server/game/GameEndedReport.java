package org.supcomhub.server.game;

import org.supcomhub.server.common.ClientMessage;

/**
 * Sent by the game whenever the simulation ended.
 */
public enum GameEndedReport implements ClientMessage {
  INSTANCE
}
