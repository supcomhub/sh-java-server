package org.supcomhub.server.ice;


import org.supcomhub.server.security.Account;

/**
 * Interface for ICE server list providers.
 */
public interface IceServersProvider {
  IceServerList getIceServerList(Account player);
}
