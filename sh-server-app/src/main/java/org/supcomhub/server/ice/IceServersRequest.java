package org.supcomhub.server.ice;

import org.supcomhub.server.common.ClientMessage;

/**
 * Requests a list of ICE servers.
 */
public enum IceServersRequest implements ClientMessage {
  INSTANCE
}
