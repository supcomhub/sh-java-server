package org.supcomhub.server.ice;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.supcomhub.server.client.ClientService;
import org.supcomhub.server.player.PlayerService;
import org.supcomhub.server.security.Account;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class IceService {

  private final ClientService clientService;
  private final PlayerService playerService;
  private final List<IceServersProvider> serversProviders;

  public IceService(ClientService clientService, PlayerService playerService, List<IceServersProvider> serversProviders) {
    this.clientService = clientService;
    this.playerService = playerService;
    this.serversProviders = serversProviders;
  }

  public void requestIceServers(Account player) {
    log.trace("Player '{}' requested a list of ICE servers", player);

    List<IceServerList> iceServerLists = serversProviders.stream()
      .map(iceServersProvider -> iceServersProvider.getIceServerList(player))
      .collect(Collectors.toList());

    log.trace("Sending list of ICE servers to player '{}': {}", player, iceServerLists);

    // TODO list of list is questionable. Make flat?
    clientService.sendIceServers(iceServerLists, player);
  }

  public void forwardIceMessage(Account sender, int receiverId, Object content) {
    Optional<Account> recipient = playerService.getOnlinePlayer(receiverId);
    if (recipient.isEmpty()) {
      log.warn("Player '{}' requested to send ICE message to offline player '{}': {}", sender, receiverId, content);
      return;
    }

    clientService.sendIceMessage(sender.getId(), content, recipient.get());
  }
}
