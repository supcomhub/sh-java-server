package org.supcomhub.server.ice;

import lombok.Data;
import org.supcomhub.server.common.ServerMessage;

/**
 * ICE message that is being forwarded.
 *
 * @see IceMessage
 */
@Data
public class ForwardedIceMessage implements ServerMessage {
  private final int senderId;
  private final Object content;
}
