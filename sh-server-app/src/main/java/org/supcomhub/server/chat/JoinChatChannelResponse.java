package org.supcomhub.server.chat;

import lombok.Data;
import org.supcomhub.server.common.ServerMessage;

import java.util.List;

/**
 * Tells the client to join chat channels.
 */
@Data
public class JoinChatChannelResponse implements ServerMessage {
  private final List<String> channels;
}
