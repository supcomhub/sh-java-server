package org.supcomhub.server.chat;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import org.supcomhub.server.client.ClientService;
import org.supcomhub.server.config.ServerProperties;
import org.supcomhub.server.config.ServerProperties.Chat;
import org.supcomhub.server.player.PlayerOnlineEvent;
import org.supcomhub.server.security.Account;
import org.supcomhub.server.security.UserGroup;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

@Service
@Slf4j
public class ChatService {

  private final ServerProperties properties;
  private final ClientService clientService;

  public ChatService(ServerProperties properties, ClientService clientService) {
    this.properties = properties;
    this.clientService = clientService;
  }

  @EventListener
  public void onPlayerOnlineEvent(PlayerOnlineEvent event) {
    Chat chat = properties.getChat();

    List<String> channels = new ArrayList<>(chat.getDefaultChannels());

    Optional.ofNullable(event.getPlayer().getClan())
      .map(clan -> (String.format(chat.getClanChannelFormat(), clan.getTag())))
      .ifPresent(channels::add);

    Account user = event.getPlayer();
    Set<UserGroup> userGroups = user.getUserGroups();

    // TODO We need to reconsider how to map roles within the code to roles in the database. This is unstable and ugly.
    userGroups.forEach(userGroup -> {
      if (Objects.equals(userGroup.getTechnicalName(), "ADMINISTRATOR")) {
        channels.addAll(chat.getAdminChannels());
      } else if (Objects.equals("MODERATOR", userGroup.getTechnicalName())) {
        channels.addAll(chat.getModeratorChannels());
      }
    });

    clientService.sendChatChannels(channels, event.getPlayer());
  }
}
