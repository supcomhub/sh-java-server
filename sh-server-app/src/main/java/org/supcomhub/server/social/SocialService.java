package org.supcomhub.server.social;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.supcomhub.server.client.ClientService;
import org.supcomhub.server.player.PlayerOnlineEvent;
import org.supcomhub.server.player.SocialRelation;
import org.supcomhub.server.player.SocialRelationStatus;
import org.supcomhub.server.security.Account;
import org.supcomhub.server.social.SocialRelationListResponse.SocialRelationResponse;
import org.supcomhub.server.social.SocialRelationListResponse.SocialRelationResponse.RelationType;
import org.supcomhub.server.social.UpdateSocialRelationRequest.Operation;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class SocialService {

  private final SocialRelationRepository socialRelationRepository;
  private final ClientService clientService;

  public SocialService(SocialRelationRepository socialRelationRepository, ClientService clientService) {
    this.socialRelationRepository = socialRelationRepository;
    this.clientService = clientService;
  }

  @EventListener
  public void onPlayerOnlineEvent(PlayerOnlineEvent event) {
    Account player = event.getPlayer();
    List<SocialRelation> socialRelations = socialRelationRepository.findAllByPlayer(player);

    if (socialRelations.isEmpty()) {
      return;
    }

    clientService.sendSocialRelations(new SocialRelationListResponse(
      socialRelations.stream()
        .map(socialRelation -> new SocialRelationResponse(
          socialRelation.getToId(), RelationType.valueOf(socialRelation.getStatus().toString())))
        .collect(Collectors.toList())
    ), player);
  }

  @Transactional
  public void updateSocialRelation(Account requester, Operation operation, UpdateSocialRelationRequest.RelationType relationType, int otherAccountId) {
    switch (operation) {
      case ADD:
        addSocialRelation(requester, relationType, otherAccountId);
        break;
      case REMOVE:
        removeSocialRelation(requester, relationType, otherAccountId);
        break;
    }
  }

  private void addFriend(Account requester, int friendId) {
    removeFoe(requester, friendId);

    log.debug("Adding '{}' as a friend of player '{}'", friendId, requester);
    socialRelationRepository.save(new SocialRelation(requester.getId(), requester, friendId, SocialRelationStatus.FRIEND));
  }

  private void addFoe(Account requester, int foeId) {
    removeFriend(requester, foeId);

    log.debug("Adding '{}' as a foe of player '{}'", foeId, requester);
    socialRelationRepository.save(new SocialRelation(requester.getId(), requester, foeId, SocialRelationStatus.FOE));
  }

  private void removeFriend(Account requester, int friendId) {
    log.debug("Removing '{}' as a friend of player '{}'", friendId, requester);
    socialRelationRepository.deleteByFromIdAndToIdAndStatus(requester.getId(), friendId, SocialRelationStatus.FRIEND);
  }

  private void removeFoe(Account requester, int foeId) {
    log.debug("Removing '{}' as a foe of player '{}'", foeId, requester);
    socialRelationRepository.deleteByFromIdAndToIdAndStatus(requester.getId(), foeId, SocialRelationStatus.FOE);
  }

  private void removeSocialRelation(Account requester, UpdateSocialRelationRequest.RelationType relationType, int otherAccountId) {
    switch (relationType) {
      case FRIEND:
        removeFriend(requester, otherAccountId);
        break;
      case FOE:
        removeFoe(requester, otherAccountId);
        break;
    }
  }

  private void addSocialRelation(Account requester, UpdateSocialRelationRequest.RelationType relationType, int otherAccountId) {
    switch (relationType) {
      case FRIEND:
        addFriend(requester, otherAccountId);
        break;
      case FOE:
        addFoe(requester, otherAccountId);
        break;
    }
    return;
  }
}
