package org.supcomhub.server.social;

import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.security.core.Authentication;
import org.supcomhub.server.integration.ChannelNames;
import org.supcomhub.server.integration.MessageHeaders;
import org.supcomhub.server.security.Account;
import org.supcomhub.server.security.ShUserDetails;

@MessageEndpoint
public class SocialServiceActivators {

  private final SocialService socialService;

  public SocialServiceActivators(SocialService socialService) {
    this.socialService = socialService;
  }

  @ServiceActivator(inputChannel = ChannelNames.UPDATE_SOCIAL_RELATION_REQUEST)
  public void updateSocialRelation(UpdateSocialRelationRequest request, @Header(MessageHeaders.USER_HEADER) Authentication authentication) {
    socialService.updateSocialRelation(getPlayer(authentication), request.getOperation(), request.getRelationType(), request.getAccountId());
  }

  private Account getPlayer(Authentication authentication) {
    return ((ShUserDetails) authentication.getPrincipal()).getPlayer();
  }
}
