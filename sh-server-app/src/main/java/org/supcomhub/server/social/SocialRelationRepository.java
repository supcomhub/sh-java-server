package org.supcomhub.server.social;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.supcomhub.server.player.SocialRelation;
import org.supcomhub.server.player.SocialRelationStatus;
import org.supcomhub.server.security.Account;

import java.util.List;

@Transactional(propagation = Propagation.MANDATORY)
public interface SocialRelationRepository extends JpaRepository<SocialRelation, Integer> {
  void deleteByFromIdAndToIdAndStatus(int fromId, int toId, SocialRelationStatus status);

  List<SocialRelation> findAllByPlayer(Account player);
}
