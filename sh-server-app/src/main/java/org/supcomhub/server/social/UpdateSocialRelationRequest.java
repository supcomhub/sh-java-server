package org.supcomhub.server.social;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.supcomhub.server.common.ClientMessage;

/**
 * Tells the server to remove the specified player from the sending player's friend list.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateSocialRelationRequest implements ClientMessage {

  /** The ID of the "other" account. */
  private int accountId;

  /** The operation to perform on this social relation. */
  private Operation operation;

  /** The type of the relation. */
  private RelationType relationType;

  /** The type of the relation. */
  public enum RelationType {
    /** Modify a 'friend' relation. */
    FRIEND,

    /** Modify a 'foe' relation. */
    FOE
  }

  /** The operation to perform on this social relation. */
  public enum Operation {
    /** Add this social relation. */
    ADD,
    /** Remove this social relation. */
    REMOVE
  }
}
