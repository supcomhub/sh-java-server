package org.supcomhub.server.social;

import lombok.Data;
import org.supcomhub.server.common.ServerMessage;

import java.util.List;

@Data
public class SocialRelationListResponse implements ServerMessage {
  private final List<SocialRelationResponse> socialRelations;

  @Data
  public static class SocialRelationResponse {
    private final Integer accountId;
    private final RelationType type;

    public enum RelationType {
      FRIEND, FOE
    }
  }
}
