package org.supcomhub.server.ladder1v1;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.supcomhub.server.player.PlayerDivisionInfo;
import org.supcomhub.server.security.Account;

@Repository
public interface PlayerDivisionInfoRepository extends JpaRepository<PlayerDivisionInfo, Integer> {
  PlayerDivisionInfo findByPlayerAndSeason(Account player, int season);
}
