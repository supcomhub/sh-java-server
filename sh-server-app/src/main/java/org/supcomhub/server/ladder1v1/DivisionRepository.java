package org.supcomhub.server.ladder1v1;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DivisionRepository extends JpaRepository<Division, Integer> {
  List<Division> findAllByOrderByLeagueAscThresholdAsc();
}
