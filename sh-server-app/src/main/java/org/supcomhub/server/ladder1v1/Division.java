package org.supcomhub.server.ladder1v1;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.supcomhub.server.common.jpa.AbstractIntegerIdEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "ladder_division")
@Getter
@Setter
@NoArgsConstructor
@ToString(of = {"league", "name", "threshold"}, callSuper = true)
public class Division extends AbstractIntegerIdEntity {

  @Column(name = "name", nullable = false)
  private String name;

  @Column(name = "league")
  private int league;

  @Column(name = "threshold")
  private int threshold;
}
