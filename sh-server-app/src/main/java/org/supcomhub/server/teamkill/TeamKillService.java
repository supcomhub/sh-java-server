package org.supcomhub.server.teamkill;

import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import org.supcomhub.server.error.ErrorCode;
import org.supcomhub.server.error.Requests;
import org.supcomhub.server.game.Game;
import org.supcomhub.server.game.GameClosedEvent;
import org.supcomhub.server.game.TeamKill;
import org.supcomhub.server.player.PlayerService;
import org.supcomhub.server.security.Account;

import java.time.Duration;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Slf4j
@Service
public class TeamKillService {
  private final PlayerService playerService;
  private final TeamKillRepository teamKillRepository;
  // This isn't a Set of `TeamKill` since it's a mutable class
  private Set<ReportableTeamKill> reportableTeamKills;

  public TeamKillService(PlayerService playerService, TeamKillRepository teamKillRepository) {
    this.playerService = playerService;
    this.teamKillRepository = teamKillRepository;

    reportableTeamKills = new HashSet<>();
  }

  public void reportTeamKill(Account reporter, Duration gameTime, int killerId) {
    Game game = reporter.getCurrentGame();
    if (game == null) {
      log.warn("Player '{}' reported team kill by '{}' but is not associated with a game", reporter, killerId);
      return;
    }

    boolean isReportable = reportableTeamKills.remove(ReportableTeamKill.of(killerId, reporter.getId(), game.getId(), (int) gameTime.toSeconds()));
    Requests.verify(isReportable, ErrorCode.TEAM_KILL_NOT_REPORTABLE);

    Optional<Account> killer = playerService.getOnlinePlayer(killerId);
    if (!killer.isPresent()) {
      log.warn("Player '{}' reported team kill by unknown reporter '{}' in game '{}' (victim: '{}')", reporter, killerId, game, reporter);
      return;
    }

    boolean isKillerPartOfGame = game.getParticipants().values().stream()
      .anyMatch(gamePlayerStats -> gamePlayerStats.getPlayer().getId() == killerId);

    if (!isKillerPartOfGame) {
      log.warn("Player '{}' reported team kill by '{}' in game '{}', but killer is not part of the game", reporter, killer, game);
    }

    log.debug("Player '{}' reported team kill by '{}' in game: {}", reporter, killer, game);

    TeamKill teamKill = new TeamKill(killerId, reporter.getId(), game.getId(), (int) gameTime.getSeconds());
    teamKillRepository.save(teamKill);
  }

  public void onTeamKillHappened(Account reporter, Duration time, int killerId) {
    Game game = Optional.ofNullable(reporter.getCurrentGame())
      .orElseThrow(() -> Requests.exception(ErrorCode.NOT_IN_A_GAME));

    reportableTeamKills.add(ReportableTeamKill.of(killerId, reporter.getId(), game.getId(), (int) time.toSeconds()));
  }

  // Other than the ClientDisconnectedEvent, this event isn't queued via the ChannelNames.INBOUND_DISPATCH channel since
  //  "teamkill happened" has to be sent by the player before he closes the game. Therefore, GameClosedEvent can only
  //  ever occur after (given that messages from the same connection are processed in sequence).
  @EventListener
  public void onGameClosed(GameClosedEvent event) {
    reportableTeamKills.removeIf(reportableTeamKill -> reportableTeamKill.getGameId() == event.getGameId());
  }

  @Value(staticConstructor = "of")
  private static class ReportableTeamKill {
    int killerId;
    int victimId;
    int gameId;
    int gameTime;
  }
}
