package org.supcomhub.server.teamkill;

import org.springframework.data.jpa.repository.JpaRepository;
import org.supcomhub.server.game.TeamKill;

public interface TeamKillRepository extends JpaRepository<TeamKill, Integer> {
}
