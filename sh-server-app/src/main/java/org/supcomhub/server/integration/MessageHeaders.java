package org.supcomhub.server.integration;

import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.supcomhub.server.client.ClientConnection;
import org.supcomhub.server.security.ShUserDetails;

/**
 * Holds the names of our own message headers.
 */
public final class MessageHeaders {

  /** Boolean flag to indicate whether a message is to be broadcasted. */
  public static final String BROADCAST = "broadcast";

  /**
   * The {@link ClientConnection} a message belongs to. Basically, this tells us which client sent the message.
   */
  public static final String CLIENT_CONNECTION = "clientConnection";

  /**
   * Contains the sender's {@link org.springframework.security.core.Authentication}. Its principal is expected to be of
   * type {@link ShUserDetails}.
   */
  public static final String USER_HEADER = SimpMessageHeaderAccessor.USER_HEADER;

  /** Contains the WebSocket session ID. */
  public static final String WS_SESSION_ID = SimpMessageHeaderAccessor.SESSION_ID_HEADER;

  private MessageHeaders() {
    // Not instantiatable
  }
}
