package org.supcomhub.server.integration;

import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.security.core.Authentication;
import org.supcomhub.server.client.ClientDisconnectedEvent;
import org.supcomhub.server.game.AiOptionReport;
import org.supcomhub.server.game.ArmyOutcomeReport;
import org.supcomhub.server.game.ArmyScoreReport;
import org.supcomhub.server.game.ClearSlotRequest;
import org.supcomhub.server.game.DesyncReport;
import org.supcomhub.server.game.DisconnectPeerRequest;
import org.supcomhub.server.game.GameEndedReport;
import org.supcomhub.server.game.GameModsReport;
import org.supcomhub.server.game.GameOptionReport;
import org.supcomhub.server.game.GameService;
import org.supcomhub.server.game.GameStateReport;
import org.supcomhub.server.game.HostGameRequest;
import org.supcomhub.server.game.JoinGameRequest;
import org.supcomhub.server.game.LobbyMode;
import org.supcomhub.server.game.MutuallyAgreedDrawRequest;
import org.supcomhub.server.game.PlayerDisconnectedReport;
import org.supcomhub.server.game.PlayerOptionReport;
import org.supcomhub.server.game.RestoreGameSessionRequest;
import org.supcomhub.server.security.Account;
import org.supcomhub.server.security.ShUserDetails;
import org.supcomhub.server.stats.ArmyStatisticsReport;

import java.util.Optional;

import static org.supcomhub.server.integration.MessageHeaders.USER_HEADER;


@MessageEndpoint
public class GameServiceActivators {

  private final GameService gameService;

  public GameServiceActivators(GameService gameService) {
    this.gameService = gameService;
  }

  @ServiceActivator(inputChannel = ChannelNames.HOST_GAME_REQUEST)
  public void hostGameRequest(HostGameRequest request, @Header(USER_HEADER) Authentication authentication) {
    gameService.createCustomGame(request.getTitle(), request.getFeaturedModName(), request.getMap(), request.getPassword(),
      request.getVisibility(), request.getMinRank(), request.getMaxRank(), getPlayer(authentication),
      LobbyMode.DEFAULT, Optional.empty());
  }

  @ServiceActivator(inputChannel = ChannelNames.JOIN_GAME_REQUEST)
  public void joinGameRequest(JoinGameRequest request, @Header(USER_HEADER) Authentication authentication) {
    gameService.joinGame(request.getId(), request.getPassword(), getPlayer(authentication));
  }

  @ServiceActivator(inputChannel = ChannelNames.UPDATE_GAME_STATE_REQUEST)
  public void updateGameState(GameStateReport report, @Header(USER_HEADER) Authentication authentication) {
    gameService.updatePlayerGameState(report.getState(), getPlayer(authentication));
  }

  @ServiceActivator(inputChannel = ChannelNames.GAME_OPTION_REQUEST)
  public void updateGameOption(GameOptionReport report, @Header(USER_HEADER) Authentication authentication) {
    gameService.updateGameOption(getPlayer(authentication), report.getKey(), report.getValue());
  }

  @ServiceActivator(inputChannel = ChannelNames.PLAYER_OPTION_REQUEST)
  public void updatePlayerOption(PlayerOptionReport report, @Header(USER_HEADER) Authentication authentication) {
    gameService.updatePlayerOption(getPlayer(authentication), report.getPlayerId(), report.getKey(), report.getValue());
  }

  @ServiceActivator(inputChannel = ChannelNames.CLEAR_SLOT_REQUEST)
  public void clearSlot(ClearSlotRequest request, @Header(USER_HEADER) Authentication authentication) {
    gameService.clearSlot(getPlayer(authentication).getCurrentGame(), request.getSlotId());
  }

  @ServiceActivator(inputChannel = ChannelNames.AI_OPTION_REQUEST)
  public void updateAiOption(AiOptionReport report, @Header(USER_HEADER) Authentication authentication) {
    gameService.updateAiOption(getPlayer(authentication), report.getAiName(), report.getKey(), report.getValue());
  }

  @ServiceActivator(inputChannel = ChannelNames.DESYNC_REPORT)
  public void reportDesync(DesyncReport desyncReport, @Header(USER_HEADER) Authentication authentication) {
    gameService.reportDesync(getPlayer(authentication));
  }

  @ServiceActivator(inputChannel = ChannelNames.GAME_MODS_REPORT)
  public void updateGameMods(GameModsReport report, @Header(USER_HEADER) Authentication authentication) {
    gameService.updateGameMods(getPlayer(authentication).getCurrentGame(), report.getModUuids());
  }

  @ServiceActivator(inputChannel = ChannelNames.ARMY_OUTCOME_REPORT)
  public void reportArmyOutcome(ArmyOutcomeReport report, @Header(USER_HEADER) Authentication authentication) {
    gameService.reportArmyOutcome(getPlayer(authentication), report.getArmyId(), report.getOutcome(), report.getScore());
  }

  @ServiceActivator(inputChannel = ChannelNames.ARMY_SCORE_REPORT)
  public void reportArmyScore(ArmyScoreReport report, @Header(USER_HEADER) Authentication authentication) {
    gameService.reportArmyScore(getPlayer(authentication), report.getArmyId(), report.getScore());
  }

  @ServiceActivator(inputChannel = ChannelNames.GAME_STATISTICS_REPORT)
  public void reportGameStatistics(ArmyStatisticsReport report, @Header(USER_HEADER) Authentication authentication) {
    gameService.reportArmyStatistics(getPlayer(authentication), report.getArmyStatistics());
  }

  @ServiceActivator(inputChannel = ChannelNames.DISCONNECT_PEER_REQUEST)
  public void disconnectFromGame(DisconnectPeerRequest request, @Header(USER_HEADER) Authentication authentication) {
    gameService.disconnectPlayerFromGame(getPlayer(authentication), request.getPlayerId());
  }

  @ServiceActivator(inputChannel = ChannelNames.RESTORE_GAME_SESSION_REQUEST)
  public void restoreGameSession(RestoreGameSessionRequest request, @Header(USER_HEADER) Authentication authentication) {
    gameService.restoreGameSession(getPlayer(authentication), request.getGameId());
  }

  @ServiceActivator(inputChannel = ChannelNames.MUTUALLY_AGREED_DRAW_REQUEST)
  public void mutuallyAgreeDraw(MutuallyAgreedDrawRequest request, @Header(USER_HEADER) Authentication authentication) {
    gameService.mutuallyAgreeDraw(getPlayer(authentication));
  }

  @ServiceActivator(inputChannel = ChannelNames.CLIENT_DISCONNECTED_EVENT)
  public void onClientDisconnected(ClientDisconnectedEvent event) {
    Optional.ofNullable(event.getClientConnection().getAuthentication())
      .ifPresent(authentication -> gameService.removePlayer(((ShUserDetails) authentication.getPrincipal()).getPlayer()));
  }

  @ServiceActivator(inputChannel = ChannelNames.GAME_ENDED_REPORT)
  public void onGameEnded(GameEndedReport event, @Header(USER_HEADER) Authentication authentication) {
    gameService.reportGameEnded(getPlayer(authentication));
  }

  @ServiceActivator(inputChannel = ChannelNames.DISCONNECTED_REPORT)
  public void onDisconnected(PlayerDisconnectedReport event, @Header(USER_HEADER) Authentication authentication) {
    gameService.playerDisconnected(getPlayer(authentication), event.getPlayerId());
  }

  private Account getPlayer(Authentication authentication) {
    return ((ShUserDetails) authentication.getPrincipal()).getPlayer();
  }
}
