package org.supcomhub.server.integration;

import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.security.core.Authentication;
import org.supcomhub.server.client.ClientDisconnectedEvent;
import org.supcomhub.server.client.ConnectionAware;
import org.supcomhub.server.matchmaker.CreateMatchRequest;
import org.supcomhub.server.matchmaker.MatchMakerCancelRequest;
import org.supcomhub.server.matchmaker.MatchMakerMapper;
import org.supcomhub.server.matchmaker.MatchMakerSearchRequest;
import org.supcomhub.server.matchmaker.MatchMakerService;
import org.supcomhub.server.security.Account;
import org.supcomhub.server.security.ShUserDetails;

import java.util.Optional;

/**
 * Message endpoint that takes match maker messages and calls the respective methods on the {@link MatchMakerService}.
 */
@MessageEndpoint
public class MatchMakerServiceActivator {
  private final MatchMakerService matchMakerService;
  private final MatchMakerMapper matchMakerMapper;

  public MatchMakerServiceActivator(MatchMakerService matchMakerService, MatchMakerMapper matchMakerMapper) {
    this.matchMakerService = matchMakerService;
    this.matchMakerMapper = matchMakerMapper;
  }

  @ServiceActivator(inputChannel = ChannelNames.MATCH_MAKER_SEARCH_REQUEST)
  public void startSearch(MatchMakerSearchRequest request, @Header(MessageHeaders.USER_HEADER) Authentication authentication) {
    matchMakerService.submitSearch(
      getPlayer(authentication),
      request.getFaction(),
      request.getPoolName()
    );
  }

  @ServiceActivator(inputChannel = ChannelNames.MATCH_MAKER_CANCEL_REQUEST)
  public void cancelSearch(MatchMakerCancelRequest request, @Header(MessageHeaders.USER_HEADER) Authentication authentication) {
    matchMakerService.cancelSearch(request.getPoolName(), getPlayer(authentication));
  }

  private Account getPlayer(@Header(MessageHeaders.USER_HEADER) Authentication authentication) {
    return ((ShUserDetails) authentication.getPrincipal()).getPlayer();
  }

  @ServiceActivator(inputChannel = ChannelNames.CLIENT_DISCONNECTED_EVENT)
  public void onClientDisconnected(ClientDisconnectedEvent event) {
    Optional.ofNullable(event.getClientConnection().getAuthentication())
      .ifPresent(authentication -> matchMakerService.removePlayer(getPlayer(authentication)));
  }

  @ServiceActivator(inputChannel = ChannelNames.CREATE_MATCH_REQUEST)
  public void createMatch(CreateMatchRequest request, @Header(MessageHeaders.USER_HEADER) Authentication authentication) {
    matchMakerService.createMatch(
      getRequester(authentication),
      request.getRequestId(),
      request.getTitle(),
      request.getFeaturedModId(),
      matchMakerMapper.map(request.getParticipants()),
      request.getMapVersionId(),
      request.getLeaderboardId()
    );
  }

  private ConnectionAware getRequester(Authentication authentication) {
    return (ConnectionAware) authentication.getPrincipal();
  }
}
