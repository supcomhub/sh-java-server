package org.supcomhub.server.integration;

import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.messaging.handler.annotation.Header;
import org.supcomhub.server.client.ClientConnection;
import org.supcomhub.server.common.ServerMessage;

/**
 * A Spring Integration gateway to send messages to the client. When a service calls a gateway method, th
 */
@MessagingGateway
public interface ClientGateway {

  /**
   * Sends the specified message to the specified client connection.
   */
  @Gateway(requestChannel = ChannelNames.CLIENT_OUTBOUND)
  void send(ServerMessage serverMessage, @Header(MessageHeaders.CLIENT_CONNECTION) ClientConnection clientConnection);

  /**
   * Sends the specified message to all clients.
   */
  @Gateway(requestChannel = ChannelNames.CLIENT_OUTBOUND_BROADCAST)
  void broadcast(ServerMessage response);
}
