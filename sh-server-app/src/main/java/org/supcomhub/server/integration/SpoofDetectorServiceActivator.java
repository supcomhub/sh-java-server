package org.supcomhub.server.integration;

import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.security.core.Authentication;
import org.supcomhub.server.game.SpoofDetectorService;
import org.supcomhub.server.game.VerifyPlayerReport;
import org.supcomhub.server.security.Account;
import org.supcomhub.server.security.ShUserDetails;

@MessageEndpoint
public class SpoofDetectorServiceActivator {
  private final SpoofDetectorService spoofDetectorService;

  public SpoofDetectorServiceActivator(SpoofDetectorService spoofDetectorService) {
    this.spoofDetectorService = spoofDetectorService;
  }

  @ServiceActivator(inputChannel = ChannelNames.VERIFY_PLAYER_REQUEST)
  public void verifyPlayerReport(VerifyPlayerReport report, @Header(MessageHeaders.USER_HEADER) Authentication authentication) {
    spoofDetectorService.verifyPlayer(
      getPlayer(authentication),
      report.getId(),
      report.getName(),
      report.getRank(),
      report.getCountry(),
      report.getAvatarUrl(),
      report.getAvatarDescription()
    );
  }

  private Account getPlayer(Authentication authentication) {
    return ((ShUserDetails) authentication.getPrincipal()).getPlayer();
  }
}
