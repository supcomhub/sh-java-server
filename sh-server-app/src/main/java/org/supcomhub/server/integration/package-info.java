/**
 * <p>Contains Spring Integration infrastructure classes. While {@link org.supcomhub.server.config.integration}
 * contains configuration classes that wire everything together, this package contains the actual components that are
 * being wired together. Everything outside this package should know nothing about Spring Integration</p>
 */
package org.supcomhub.server.integration;
