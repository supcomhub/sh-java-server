package org.supcomhub.server.integration;

import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.security.core.Authentication;
import org.supcomhub.server.coop.CoopMissionCompletedReport;
import org.supcomhub.server.coop.CoopService;
import org.supcomhub.server.security.ShUserDetails;

/**
 * Message endpoint that takes Co-Op messages and calls the respective methods on the {@link CoopService}.
 */
@MessageEndpoint
public class CoopServiceActivators {

  private final CoopService coopService;

  public CoopServiceActivators(CoopService coopService) {
    this.coopService = coopService;
  }

  @ServiceActivator(inputChannel = ChannelNames.OPERATION_COMPLETE_REPORT)
  public void reportOperationComplete(CoopMissionCompletedReport report, @Header(MessageHeaders.USER_HEADER) Authentication authentication) {
    coopService.reportOperationComplete(((ShUserDetails) authentication.getPrincipal()).getPlayer(), report.isSecondaryTargets(), report.getTime());
  }
}
