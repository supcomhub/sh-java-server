package org.supcomhub.server.integration;

import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.security.core.Authentication;
import org.supcomhub.server.avatar.AvatarService;
import org.supcomhub.server.avatar.SelectAvatarRequest;
import org.supcomhub.server.security.Account;
import org.supcomhub.server.security.ShUserDetails;

@MessageEndpoint
public class AvatarServiceActivator {
  private final AvatarService avatarService;

  public AvatarServiceActivator(AvatarService avatarService) {
    this.avatarService = avatarService;
  }

  @ServiceActivator(inputChannel = ChannelNames.SELECT_AVATAR)
  public void selectAvatar(SelectAvatarRequest request, @Header(MessageHeaders.USER_HEADER) Authentication authentication) {
    avatarService.selectAvatar(getPlayer(authentication), request.getAvatarId());
  }

  private Account getPlayer(Authentication authentication) {
    return ((ShUserDetails) authentication.getPrincipal()).getPlayer();
  }
}
