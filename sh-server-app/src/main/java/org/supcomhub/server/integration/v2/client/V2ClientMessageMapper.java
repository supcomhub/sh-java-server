package org.supcomhub.server.integration.v2.client;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.supcomhub.server.avatar.SelectAvatarRequest;
import org.supcomhub.server.client.BroadcastRequest;
import org.supcomhub.server.client.DisconnectClientRequest;
import org.supcomhub.server.client.PingReport;
import org.supcomhub.server.coop.CoopMissionCompletedReport;
import org.supcomhub.server.game.AiOptionReport;
import org.supcomhub.server.game.ArmyOutcomeReport;
import org.supcomhub.server.game.ArmyScoreReport;
import org.supcomhub.server.game.BottleneckClearedReport;
import org.supcomhub.server.game.BottleneckReport;
import org.supcomhub.server.game.ClearSlotRequest;
import org.supcomhub.server.game.DesyncReport;
import org.supcomhub.server.game.DisconnectPeerRequest;
import org.supcomhub.server.game.DisconnectedReport;
import org.supcomhub.server.game.EnforceRatingReport;
import org.supcomhub.server.game.GameChatMessageReport;
import org.supcomhub.server.game.GameEndedReport;
import org.supcomhub.server.game.GameModsReport;
import org.supcomhub.server.game.GameOptionReport;
import org.supcomhub.server.game.GameStateReport;
import org.supcomhub.server.game.HostGameRequest;
import org.supcomhub.server.game.JoinGameRequest;
import org.supcomhub.server.game.MutuallyAgreedDrawRequest;
import org.supcomhub.server.game.PlayerOptionReport;
import org.supcomhub.server.game.RehostRequest;
import org.supcomhub.server.game.RestoreGameSessionRequest;
import org.supcomhub.server.game.TeamKillHappenedReport;
import org.supcomhub.server.game.TeamKillReportReport;
import org.supcomhub.server.game.VerifyPlayerReport;
import org.supcomhub.server.ice.IceMessage;
import org.supcomhub.server.ice.IceServersRequest;
import org.supcomhub.server.matchmaker.CreateMatchRequest;
import org.supcomhub.server.matchmaker.MatchMakerCancelRequest;
import org.supcomhub.server.matchmaker.MatchMakerSearchRequest;
import org.supcomhub.server.protocol.v2.dto.client.AgreeDrawClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.AiOptionClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.ArmyOutcomeClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.ArmyScoreClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.ArmyStatsClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.BottleneckClearedClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.BottleneckClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.BroadcastClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.CancelMatchSearchClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.ClearSlotClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.CreateMatchClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.DisconnectClientClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.DisconnectPeerClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.DisconnectedClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.EnforceRatingClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.GameChatClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.GameDesyncClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.GameEndedClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.GameModsClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.GameOptionClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.GameStateClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.HostGameClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.IceClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.JoinGameClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.ListIceServersClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.OperationCompleteClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.PingClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.PlayerOptionClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.RehostClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.RestoreGameSessionClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.SearchMatchClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.SelectAvatarClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.TeamKillHappenedClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.TeamKillReportClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.UpdateSocialRelationClientMessage;
import org.supcomhub.server.protocol.v2.dto.client.VerifyPlayerClientMessage;
import org.supcomhub.server.social.UpdateSocialRelationRequest;
import org.supcomhub.server.stats.ArmyStatisticsReport;

@Mapper(componentModel = "spring", uses = ArmyStatsMapper.class)
public interface V2ClientMessageMapper {

  AiOptionReport map(AiOptionClientMessage message);

  ArmyScoreReport map(ArmyScoreClientMessage message);

  BroadcastRequest map(BroadcastClientMessage message);

  @Mapping(source = "pool", target = "poolName")
  MatchMakerCancelRequest map(CancelMatchSearchClientMessage message);

  @Mapping(expression = "java(java.time.Duration.ofSeconds(message.getTime()))", target = "time")
  CoopMissionCompletedReport map(OperationCompleteClientMessage message);

  DisconnectClientRequest map(DisconnectClientClientMessage message);

  DisconnectedReport map(DisconnectedClientMessage message);

  DisconnectPeerRequest map(DisconnectPeerClientMessage message);

  GameOptionReport map(GameOptionClientMessage message);

  GameStateReport map(GameStateClientMessage message);

  IceMessage map(IceClientMessage message);

  JoinGameRequest map(JoinGameClientMessage message);

  PlayerOptionReport map(PlayerOptionClientMessage message);

  RestoreGameSessionRequest map(RestoreGameSessionClientMessage message);

  @Mapping(source = "stats", target = "armyStatistics")
  ArmyStatisticsReport map(ArmyStatsClientMessage message);

  @Mapping(source = "pool", target = "poolName")
  MatchMakerSearchRequest map(SearchMatchClientMessage message);

  SelectAvatarRequest map(SelectAvatarClientMessage message);

  @Mapping(expression = "java(java.time.Duration.ofSeconds(message.getTime()))", target = "time")
  TeamKillReportReport map(TeamKillReportClientMessage message);

  @Mapping(expression = "java(java.time.Duration.ofSeconds(message.getTime()))", target = "time")
  TeamKillHappenedReport map(TeamKillHappenedClientMessage message);

  @Mapping(target = "featuredModName", source = "mod")
  HostGameRequest map(HostGameClientMessage message);

  GameModsReport map(GameModsClientMessage message);

  @Mapping(source = "map", target = "mapVersionId")
  CreateMatchRequest map(CreateMatchClientMessage message);

  ArmyOutcomeReport map(ArmyOutcomeClientMessage message);

  GameChatMessageReport map(GameChatClientMessage message);

  VerifyPlayerReport map(VerifyPlayerClientMessage message);

  UpdateSocialRelationRequest map(UpdateSocialRelationClientMessage message);

  default EnforceRatingReport map(@SuppressWarnings("unused") EnforceRatingClientMessage message) {
    return EnforceRatingReport.INSTANCE;
  }

  default IceServersRequest map(@SuppressWarnings("unused") ListIceServersClientMessage message) {
    return IceServersRequest.INSTANCE;
  }

  default PingReport map(@SuppressWarnings("unused") PingClientMessage message) {
    return PingReport.INSTANCE;
  }

  default DesyncReport map(@SuppressWarnings("unused") GameDesyncClientMessage message) {
    return DesyncReport.INSTANCE;
  }

  default GameEndedReport map(@SuppressWarnings("unused") GameEndedClientMessage message) {
    return GameEndedReport.INSTANCE;
  }

  default RehostRequest map(@SuppressWarnings("unused") RehostClientMessage message) {
    return RehostRequest.INSTANCE;
  }

  default BottleneckReport map(@SuppressWarnings("unused") BottleneckClientMessage message) {
    return BottleneckReport.INSTANCE;
  }

  default BottleneckClearedReport map(@SuppressWarnings("unused") BottleneckClearedClientMessage message) {
    return BottleneckClearedReport.INSTANCE;
  }

  default ClearSlotRequest map(ClearSlotClientMessage message) {
    return ClearSlotRequest.valueOf(message.getSlotId());
  }


  default MutuallyAgreedDrawRequest map(@SuppressWarnings("unused") AgreeDrawClientMessage message) {
    return MutuallyAgreedDrawRequest.INSTANCE;
  }

}
