package org.supcomhub.server.integration;

import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.ServiceActivator;
import org.supcomhub.server.client.ClientDisconnectedEvent;
import org.supcomhub.server.player.PlayerService;
import org.supcomhub.server.security.ShUserDetails;

import java.util.Optional;

/**
 * Message endpoint that calls {@link PlayerServiceActivator}.
 */
@MessageEndpoint
public class PlayerServiceActivator {

  private final PlayerService playerService;

  public PlayerServiceActivator(PlayerService playerService) {
    this.playerService = playerService;
  }

  @ServiceActivator(inputChannel = ChannelNames.CLIENT_DISCONNECTED_EVENT)
  public void onClientDisconnected(ClientDisconnectedEvent event) {
    Optional.ofNullable(event.getClientConnection().getAuthentication())
      .ifPresent(authentication -> playerService.removePlayer(((ShUserDetails) authentication.getPrincipal()).getPlayer()));
  }
}
