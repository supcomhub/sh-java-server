package org.supcomhub.server.integration;

import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.security.core.Authentication;
import org.supcomhub.server.ice.IceMessage;
import org.supcomhub.server.ice.IceServersRequest;
import org.supcomhub.server.ice.IceService;
import org.supcomhub.server.security.ShUserDetails;

/**
 * Message endpoint that takes ICE (Interactive Connectivity Establishment) messages and calls the respective methods on
 * the {@link IceService}.
 */
@MessageEndpoint
public class IceServiceActivators {
  private final IceService iceService;

  public IceServiceActivators(IceService iceService) {
    this.iceService = iceService;
  }

  @ServiceActivator(inputChannel = ChannelNames.ICE_SERVERS_REQUEST)
  public void requestIceServers(IceServersRequest request, @Header(MessageHeaders.USER_HEADER) Authentication authentication) {
    iceService.requestIceServers(((ShUserDetails) authentication.getPrincipal()).getPlayer());
  }

  @ServiceActivator(inputChannel = ChannelNames.ICE_MESSAGE)
  public void forwardIceMessage(IceMessage message, @Header(MessageHeaders.USER_HEADER) Authentication authentication) {
    iceService.forwardIceMessage(((ShUserDetails) authentication.getPrincipal()).getPlayer(), message.getReceiverId(), message.getContent());
  }
}
