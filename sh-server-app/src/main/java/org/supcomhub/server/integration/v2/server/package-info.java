/**
 * Contains mappers and transformers to serialize messages sent from the server to the client.
 */
package org.supcomhub.server.integration.v2.server;
