package org.supcomhub.server.integration.v2.server;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.supcomhub.server.chat.JoinChatChannelResponse;
import org.supcomhub.server.client.ConnectToHostResponse;
import org.supcomhub.server.client.ConnectToPeerResponse;
import org.supcomhub.server.client.DisconnectPlayerFromGameResponse;
import org.supcomhub.server.client.GameResponses;
import org.supcomhub.server.client.IceServersResponse;
import org.supcomhub.server.client.InfoResponse;
import org.supcomhub.server.client.PlayerResponses;
import org.supcomhub.server.client.UpdatedAchievementsResponse;
import org.supcomhub.server.error.ErrorCode;
import org.supcomhub.server.error.ErrorResponse;
import org.supcomhub.server.game.GameResponse;
import org.supcomhub.server.game.GameResultResponse;
import org.supcomhub.server.game.HostGameResponse;
import org.supcomhub.server.game.StartGameProcessResponse;
import org.supcomhub.server.matchmaker.MatchCreatedResponse;
import org.supcomhub.server.matchmaker.MatchMakerResponse;
import org.supcomhub.server.player.LoginDetailsResponse;
import org.supcomhub.server.player.PlayerOfflineResponse;
import org.supcomhub.server.player.PlayerResponse;
import org.supcomhub.server.protocol.v2.dto.server.AccountDetailsServerMessage;
import org.supcomhub.server.protocol.v2.dto.server.ChatChannelsServerMessage;
import org.supcomhub.server.protocol.v2.dto.server.ConnectToHostServerMessage;
import org.supcomhub.server.protocol.v2.dto.server.ConnectToPeerServerMessage;
import org.supcomhub.server.protocol.v2.dto.server.DisconnectPeerServerMessage;
import org.supcomhub.server.protocol.v2.dto.server.ErrorServerMessage;
import org.supcomhub.server.protocol.v2.dto.server.GameInfoServerMessage;
import org.supcomhub.server.protocol.v2.dto.server.GameInfosServerMessage;
import org.supcomhub.server.protocol.v2.dto.server.GameResultMessage;
import org.supcomhub.server.protocol.v2.dto.server.HostGameServerMessage;
import org.supcomhub.server.protocol.v2.dto.server.IceServersServerMessage;
import org.supcomhub.server.protocol.v2.dto.server.InfoServerMessage;
import org.supcomhub.server.protocol.v2.dto.server.MatchCreatedServerMessage;
import org.supcomhub.server.protocol.v2.dto.server.MatchMakerInfoServerMessage;
import org.supcomhub.server.protocol.v2.dto.server.PlayerOfflineServerMessage;
import org.supcomhub.server.protocol.v2.dto.server.PlayerServerMessage;
import org.supcomhub.server.protocol.v2.dto.server.PlayersServerMessage;
import org.supcomhub.server.protocol.v2.dto.server.SocialRelationsServerMessage;
import org.supcomhub.server.protocol.v2.dto.server.StartGameProcessServerMessage;
import org.supcomhub.server.protocol.v2.dto.server.UpdatedAchievementsServerMessage;
import org.supcomhub.server.social.SocialRelationListResponse;

import java.text.MessageFormat;
import java.util.Optional;
import java.util.UUID;

@Mapper(componentModel = "spring")
public interface V2ServerMessageMapper {
  ChatChannelsServerMessage map(JoinChatChannelResponse source);

  @Mapping(source = "offer", target = "offer")
  ConnectToPeerServerMessage map(ConnectToPeerResponse source);

  DisconnectPeerServerMessage map(DisconnectPlayerFromGameResponse source);

  @Mapping(source = "featuredMod", target = "mod")
  @Mapping(source = "technicalMapName", target = "map")
  GameInfoServerMessage map(GameResponse source);

  IceServersServerMessage map(IceServersResponse source);

  InfoServerMessage map(InfoResponse source);

  AccountDetailsServerMessage map(LoginDetailsResponse source);

  MatchMakerInfoServerMessage map(MatchMakerResponse source);

  SocialRelationsServerMessage map(SocialRelationListResponse source);

  @Mapping(source = "mapFolderName", target = "map")
  StartGameProcessServerMessage map(StartGameProcessResponse source);

  ConnectToHostServerMessage map(ConnectToHostResponse source);

  UpdatedAchievementsServerMessage map(UpdatedAchievementsResponse source);

  @Mapping(source = "responses", target = "players")
  PlayersServerMessage map(PlayerResponses source);

  PlayerServerMessage map(PlayerResponse source);

  @Mapping(source = "mapFilename", target = "mapName")
    // TODO check and document if the map files are folder names, file names or file names with path
  HostGameServerMessage map(HostGameResponse source);

  @Mapping(source = "responses", target = "games")
  GameInfosServerMessage map(GameResponses source);

  MatchCreatedServerMessage map(MatchCreatedResponse source);

  GameResultMessage map(GameResultResponse source);

  @Mapping(source = "id", target = "player.id")
  @Mapping(source = "displayName", target = "player.name")
  PlayerOfflineServerMessage map(PlayerOfflineResponse source);

  default ErrorServerMessage map(ErrorResponse source) {
    ErrorCode errorCode = source.getErrorCode();
    Object[] args = source.getArgs();

    return new ErrorServerMessage(
      errorCode.getCode(),
      MessageFormat.format(errorCode.getTitle(), args),
      MessageFormat.format(errorCode.getDetail(), args),
      Optional.ofNullable(source.getRequestId()).map(UUID::toString).orElse(null),
      args
    );
  }
}
