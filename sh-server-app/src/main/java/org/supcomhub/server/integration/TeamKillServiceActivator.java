package org.supcomhub.server.integration;

import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.security.core.Authentication;
import org.supcomhub.server.game.TeamKillHappenedReport;
import org.supcomhub.server.game.TeamKillReportReport;
import org.supcomhub.server.security.ShUserDetails;
import org.supcomhub.server.teamkill.TeamKillService;

/**
 * Message endpoint that takes team kill messages and calls the respective methods on the {@link TeamKillService}.
 */
@MessageEndpoint
public class TeamKillServiceActivator {
  private final TeamKillService teamKillService;

  public TeamKillServiceActivator(TeamKillService teamKillService) {
    this.teamKillService = teamKillService;
  }

  // TODO unit test
  @ServiceActivator(inputChannel = ChannelNames.TEAM_KILL_REPORT_REPORT)
  public void reportTeamKill(TeamKillReportReport report, @Header(MessageHeaders.USER_HEADER) Authentication authentication) {
    teamKillService.reportTeamKill(((ShUserDetails) authentication.getPrincipal()).getPlayer(), report.getTime(), report.getKillerId());
  }

  // TODO unit test
  @ServiceActivator(inputChannel = ChannelNames.TEAM_KILL_HAPPENED_REPORT)
  public void onTeamKillHappened(TeamKillHappenedReport report, @Header(MessageHeaders.USER_HEADER) Authentication authentication) {
    teamKillService.onTeamKillHappened(((ShUserDetails) authentication.getPrincipal()).getPlayer(), report.getTime(), report.getKillerId());
  }
}
