package org.supcomhub.server.integration;

import org.supcomhub.server.avatar.SelectAvatarRequest;
import org.supcomhub.server.client.ClientDisconnectedEvent;
import org.supcomhub.server.client.DisconnectClientRequest;
import org.supcomhub.server.common.ClientMessage;
import org.supcomhub.server.common.ServerMessage;
import org.supcomhub.server.config.integration.ChannelConfiguration;
import org.supcomhub.server.coop.CoopMissionCompletedReport;
import org.supcomhub.server.game.AiOptionReport;
import org.supcomhub.server.game.ArmyOutcomeReport;
import org.supcomhub.server.game.ArmyScoreReport;
import org.supcomhub.server.game.ClearSlotRequest;
import org.supcomhub.server.game.DesyncReport;
import org.supcomhub.server.game.DisconnectPeerRequest;
import org.supcomhub.server.game.EnforceRatingReport;
import org.supcomhub.server.game.GameEndedReport;
import org.supcomhub.server.game.GameModsReport;
import org.supcomhub.server.game.GameOptionReport;
import org.supcomhub.server.game.GameStateReport;
import org.supcomhub.server.game.HostGameRequest;
import org.supcomhub.server.game.JoinGameRequest;
import org.supcomhub.server.game.MutuallyAgreedDrawRequest;
import org.supcomhub.server.game.PlayerDisconnectedReport;
import org.supcomhub.server.game.PlayerOptionReport;
import org.supcomhub.server.game.RestoreGameSessionRequest;
import org.supcomhub.server.game.TeamKillHappenedReport;
import org.supcomhub.server.game.TeamKillReportReport;
import org.supcomhub.server.game.VerifyPlayerReport;
import org.supcomhub.server.ice.IceMessage;
import org.supcomhub.server.ice.IceServersRequest;
import org.supcomhub.server.matchmaker.CreateMatchRequest;
import org.supcomhub.server.matchmaker.MatchMakerCancelRequest;
import org.supcomhub.server.matchmaker.MatchMakerSearchRequest;
import org.supcomhub.server.social.UpdateSocialRelationRequest;
import org.supcomhub.server.stats.ArmyStatisticsReport;

/**
 * Holds the names of all channel. A channel's name is also the name of its bean. Channels can be configured in {@link
 * ChannelConfiguration}.
 */
public final class ChannelNames {

  /**
   * Channel for {@link JoinGameRequest JoinGameRequests}
   */
  public static final String JOIN_GAME_REQUEST = "joinGameRequest";

  /**
   * Channel for single-recipient outbound client messages. The payload of messages in this channel is {@link
   * ServerMessage}.
   */
  public static final String CLIENT_OUTBOUND = "clientOutbound";

  /**
   * Channel for broadcast outbound client messages. The payload of messages in this channel is {@link ServerMessage}.
   */
  public static final String CLIENT_OUTBOUND_BROADCAST = "clientOutboundBroadcast";

  /**
   * Channel for inbound client messages. The payload of messages in this channel is {@link ClientMessage}.
   */
  public static final String CLIENT_INBOUND = "clientInbound";

  /**
   * Channel for outbound messages over the standard protocol.
   */
  public static final String WEB_OUTBOUND = "webOutbound";

  /**
   * Channel for {@link UpdateSocialRelationRequest}.
   */
  public static final String UPDATE_SOCIAL_RELATION_REQUEST = "updateSocialRelationRequest";

  /**
   * Channel for {@link HostGameRequest}.
   */
  public static final String HOST_GAME_REQUEST = "hostGameRequest";

  /**
   * Channel for {@link GameStateReport}.
   */
  public static final String UPDATE_GAME_STATE_REQUEST = "updateGameStateRequest";

  /**
   * Channel for {@link GameOptionReport}.
   */
  public static final String GAME_OPTION_REQUEST = "gameOptionRequest";

  /**
   * Channel for {@link PlayerOptionReport}.
   */
  public static final String PLAYER_OPTION_REQUEST = "playerOptionRequest";

  /**
   * Channel for {@link VerifyPlayerReport}.
   */
  public static final String VERIFY_PLAYER_REQUEST = "verifyPlayerReport";

  /**
   * Channel for {@link ClearSlotRequest}.
   */
  public static final String CLEAR_SLOT_REQUEST = "clearSlotRequest";

  /**
   * Channel for {@link AiOptionReport}.
   */
  public static final String AI_OPTION_REQUEST = "aiOptionRequest";

  /**
   * Channel for {@link DesyncReport}.
   */
  public static final String DESYNC_REPORT = "desyncReport";

  /**
   * Channel for {@link ArmyScoreReport}.
   */
  public static final String ARMY_SCORE_REPORT = "armyScoreReport";

  /**
   * Channel for {@link ArmyOutcomeReport}.
   */
  public static final String ARMY_OUTCOME_REPORT = "armyOutcomeReport";

  /**
   * Channel for {@link GameModsReport}.
   */
  public static final String GAME_MODS_REPORT = "gameModsReport";

  /**
   * Channel for {@link CoopMissionCompletedReport}.
   */
  public static final String OPERATION_COMPLETE_REPORT = "operationCompleteReport";

  /**
   * Channel for {@link ArmyStatisticsReport}.
   */
  public static final String GAME_STATISTICS_REPORT = "gameStatisticsReport";

  /**
   * Channel for {@link EnforceRatingReport}.
   */
  public static final String ENFORCE_RATING_REQUEST = "enforceRatingRequest";

  /**
   * Channel for {@link TeamKillReportReport}.
   */
  public static final String TEAM_KILL_REPORT_REPORT = "teamKillReportReport";

  /**
   * Channel for {@link TeamKillHappenedReport}.
   */
  public static final String TEAM_KILL_HAPPENED_REPORT = "teamKillHappenedReport";

  /**
   * Channel for {@link MatchMakerSearchRequest}.
   */
  public static final String MATCH_MAKER_SEARCH_REQUEST = "matchMakerSearchRequest";

  /**
   * Channel for {@link MatchMakerCancelRequest}.
   */
  public static final String MATCH_MAKER_CANCEL_REQUEST = "matchMakerCancelRequest";

  /**
   * Channel for {@link CreateMatchRequest}.
   */
  public static final String CREATE_MATCH_REQUEST = "createMatchRequest";

  /**
   * Channel for {@link DisconnectPeerRequest}
   */
  public static final String DISCONNECT_PEER_REQUEST = "disconnectPeerRequest";

  /**
   * Channel for {@link DisconnectClientRequest}.
   */
  public static final String DISCONNECT_CLIENT_REQUEST = "disconnectClientRequest";

  /**
   * Channel for {@link IceServersRequest}.
   */
  public static final String ICE_SERVERS_REQUEST = "iceServersRequest";

  /**
   * Channel for {@link IceMessage}.
   */
  public static final String ICE_MESSAGE = "iceMessage";

  /**
   * Channel for {@link RestoreGameSessionRequest}.
   */
  public static final String RESTORE_GAME_SESSION_REQUEST = "restoreGameSessionRequest";

  /**
   * Channel for {@link MutuallyAgreedDrawRequest}.
   */
  public static final String MUTUALLY_AGREED_DRAW_REQUEST = "mutuallyAgreedDrawRequest";

  /**
   * Channel for all messages that need to be processed, no matter whether they are coming from an external system or
   * from the server itself. This is required to make sure that all actions are processed in a serial order no matter
   * where they originate from.
   */
  public static final String INBOUND_DISPATCH = "inboundDispatch";

  /**
   * Channel for {@link ClientDisconnectedEvent}.
   */
  public static final String CLIENT_DISCONNECTED_EVENT = "clientDisconnectedEvent";

  /**
   * Channel for raw legacy outbound messages to be written on the TCP socket.
   */
  public static final String LEGACY_TCP_OUTBOUND = "legacyTcpOutbound";

  /**
   * Channel for raw outbound messages to be written on the web socket.
   */
  public static final String WEB_SOCKET_OUTBOUND = "webSocketOutbound";

  /**
   * Channel for {@link SelectAvatarRequest}.
   */
  public static final String SELECT_AVATAR = "selectAvatar";

  /**
   * Channel for {@link PlayerDisconnectedReport}.
   */
  public static final String DISCONNECTED_REPORT = "disconnectedReport";

  /**
   * Channel for {@link GameEndedReport}.
   */
  public static final String GAME_ENDED_REPORT = "gameEndedReport";
}
