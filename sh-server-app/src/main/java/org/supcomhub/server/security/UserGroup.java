package org.supcomhub.server.security;


import lombok.Getter;
import lombok.Setter;
import org.supcomhub.server.common.jpa.AbstractIntegerIdEntity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity
@Table(name = "user_group")
@Setter
@Getter
public class UserGroup extends AbstractIntegerIdEntity {

  @Column(name = "technical_name")
  private String technicalName;

  @Column(name = "public")
  private boolean visible;

  @JoinTable(name = "user_group_membership",
    joinColumns = @JoinColumn(name = "group_id"),
    inverseJoinColumns = @JoinColumn(name = "member_id")
  )
  @ManyToMany(cascade = {
    CascadeType.PERSIST,
    CascadeType.MERGE
  })
  @Size(min = 1)
  @Valid
  @NotNull
  private Set<Account> members;
}
