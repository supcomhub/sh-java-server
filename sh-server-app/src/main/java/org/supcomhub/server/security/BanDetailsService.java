package org.supcomhub.server.security;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.supcomhub.server.security.Ban.BanScope;

@Service
@Slf4j
public class BanDetailsService {
  private final BanDetailsRepository banDetailsRepository;

  public BanDetailsService(BanDetailsRepository banDetailsRepository) {
    this.banDetailsRepository = banDetailsRepository;
  }

  @Transactional
  public void banUser(int userId, int authorId, BanScope scope, String reason) {
    Ban ban = new Ban()
      .setUser((Account) new Account().setId(userId))
      .setAuthor((Account) new Account().setId(authorId))
      .setReason(reason)
      .setScope(scope);

    banDetailsRepository.save(ban);
  }
}
