package org.supcomhub.server.security;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.supcomhub.server.common.jpa.AbstractUuidIdEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "oauth_client")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OAuthClient extends AbstractUuidIdEntity {

  @Column(name = "name")
  private String name;

  @Column(name = "client_secret")
  private String secret;

  @NotNull
  @Column(name = "redirect_uris")
  private String redirectUris;

  @NotNull
  @Column(name = "default_redirect_uri")
  private String defaultRedirectUri;

  @NotNull
  @Column(name = "default_scope")
  private String defaultScope;
}
