package org.supcomhub.server.security;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.supcomhub.server.common.jpa.AbstractIntegerIdEntity;
import org.supcomhub.server.common.jpa.PostgreSQLEnumType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.Instant;

@Entity
@Table(name = "ban")
@Getter
@Setter
@NoArgsConstructor
@TypeDef(name = PostgreSQLEnumType.TYPE_NAME, typeClass = PostgreSQLEnumType.class)
public class Ban extends AbstractIntegerIdEntity {

  @ManyToOne(optional = false)
  @JoinColumn(name = "account_id")
  private Account user;

  @ManyToOne(optional = false)
  @JoinColumn(name = "author_id")
  private Account author;

  @Column(name = "reason")
  private String reason;

  @Column(name = "expiry_time")
  private Instant expiresAt;

  @Column(name = "revocation_time")
  private Instant revocationTime;

  @Column(name = "scope")
  @Enumerated(EnumType.STRING)
  @Type(type = PostgreSQLEnumType.TYPE_NAME)
  private BanScope scope;

  public enum BanScope {
    CHAT, MATCHMAKER, GLOBAL
  }
}
