package org.supcomhub.server.security;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.management.MXBean;
import java.util.Optional;

@Service
@Slf4j
public class AccountService {

  private final EmailAddressService emailAddressService;

  public AccountService(EmailAddressService emailAddressService) {
    this.emailAddressService = emailAddressService;
  }

  public Optional<Account> findAccountByEmail(String email) {
    return emailAddressService.findOwnerOfAddress(email);
  }

}
