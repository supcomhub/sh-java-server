package org.supcomhub.server.security;


import com.google.common.annotations.VisibleForTesting;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.supcomhub.server.player.EmailAddress;

import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.regex.Pattern;

@Service
@Slf4j
public class EmailAddressService {

  private static final Pattern EMAIL_PATTERN = Pattern.compile(".+@.+\\..+$");

  private final EmailAddressRepository emailAddressRepository;

  public EmailAddressService(EmailAddressRepository emailAddressRepository) {
    this.emailAddressRepository = emailAddressRepository;
  }

  /**
   * Some providers allow multiple notations for the same email address. For instance, Gmail delivers messages to {@code
   * foo.bar+baz@gmail.com} and {@code f.o.o.b.a.r@gmail.com} to the same email box.
   * <p>
   * This method distills such email addresses in a provider-specific way. For the example above, the result is {@code
   * foobar@gmail.com}.
   */
  public String distillEmailAddress(String email) {
    if (email.endsWith("@gmail.com") || email.endsWith("@googlemail.com")) {
      int atIndex = email.indexOf('@');
      return (email
        .substring(0, atIndex)
        .replace(".", "")
        .replaceAll("\\+[^@]+", "")
        + email.substring(atIndex))
        .toLowerCase(Locale.US);
    }
    return email;
  }

  public Optional<Account> findOwnerOfAddress(String emailAddress) {
    return emailAddressRepository.findAllByDistilled(distillEmailAddress(emailAddress)).stream()
      .map(EmailAddress::getOwner)
      .findFirst();
  }

  public boolean emailExists(String email) {
    return emailAddressRepository.existsByDistilled(distillEmailAddress(email));
  }

  @VisibleForTesting
  public List<EmailAddress> findAll() {
    return emailAddressRepository.findAll();
  }

  public EmailAddress create(String emailAddress, Account owner) {
    return emailAddressRepository.save(new EmailAddress(emailAddress, distillEmailAddress(emailAddress), true, owner));
  }
}
