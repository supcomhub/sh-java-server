package org.supcomhub.server.security;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Formula;
import org.hibernate.annotations.JoinColumnOrFormula;
import org.hibernate.annotations.JoinColumnsOrFormulas;
import org.hibernate.annotations.JoinFormula;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.keyvalue.annotation.KeySpace;
import org.supcomhub.server.avatar.Avatar;
import org.supcomhub.server.clan.Clan;
import org.supcomhub.server.client.ClientConnection;
import org.supcomhub.server.client.ConnectionAware;
import org.supcomhub.server.common.jpa.AbstractIntegerIdEntity;
import org.supcomhub.server.game.Game;
import org.supcomhub.server.game.GameState;
import org.supcomhub.server.game.PlayerGameState;
import org.supcomhub.server.player.EmailAddress;
import org.supcomhub.server.rating.Rating;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.MapKey;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.CompletableFuture;

@Entity
@Table(name = "account")
@Inheritance(strategy = InheritanceType.JOINED)
@Getter
@Setter
@NoArgsConstructor
@KeySpace("account")
@ToString(of = {"displayName"}, callSuper = true)
public class Account extends AbstractIntegerIdEntity implements ConnectionAware {

  @Column(name = "display_name")
  private String displayName;

  @Column(name = "password")
  private String password;

  @OneToMany(fetch = FetchType.EAGER, mappedBy = "owner", cascade = CascadeType.ALL)
  @Fetch(FetchMode.JOIN)
  private Set<EmailAddress> emailAddresses = new HashSet<>();

  @ManyToMany(mappedBy = "members", fetch = FetchType.EAGER)
  private Set<UserGroup> userGroups = new HashSet<>();

  @Column(name = "last_login_user_agent")
  private String userAgent;

  @Column(name = "last_login_ip_address")
  private String ip;

  @OneToMany(mappedBy = "user", fetch = FetchType.EAGER)
  private Set<Ban> bans = new HashSet<>();

  @Transient
  private String country;

  @Transient
  @Nullable
  private TimeZone timeZone;

  /** Ratings mapped by leaderboard ID. */
  @OneToMany(mappedBy = "player", fetch = FetchType.EAGER)
  @MapKey(name = "leaderboardId")
  private Map<Integer, Rating> ratings = new HashMap<>(0, 1);

  @ManyToOne
  @JoinColumnsOrFormulas({
    @JoinColumnOrFormula(formula = @JoinFormula(value = "(select a.avatar_id from avatar_assignment a where a.account_id = id and a.selected = true)", referencedColumnName = "id")),
  })
  private Avatar avatar;

  @Transient
  @Nullable
  private Game currentGame;

  @Transient
  private PlayerGameState gameState = PlayerGameState.NONE;

  @Transient
  private ClientConnection clientConnection;

  /** ID of players who reported that this player spoofed their data. */
  @Transient
  private Set<Integer> fraudReporterIds = new HashSet<>();

  /**
   * The future that will be completed as soon as the player's game entered {@link GameState#OPEN}. A player's game may
   * never start if it crashes or the player disconnects.
   */
  @Transient
  private CompletableFuture<Game> gameFuture;

  /** The player's rating for the game he joined, at the time he joined. */
  @Transient
  private Rating ratingWithinCurrentGame;

  @ManyToOne
  @JoinColumnsOrFormulas({
    @JoinColumnOrFormula(formula = @JoinFormula(value = "(select cm.clan_id from clan_membership cm where cm.member_id = id)", referencedColumnName = "id")),
  })
  private Clan clan;

  @Formula("(select count(*) from game_participant gp where gp.participant_id = id)")
  private int numberOfGames;

  public void setGameState(PlayerGameState gameState) {
    PlayerGameState.verifyTransition(this.gameState, gameState);
    this.gameState = gameState;
  }

  @Transient
  public EmailAddress getPrimaryEmailAddress() {
    return getEmailAddresses().stream()
      .filter(EmailAddress::isPrimary)
      .findFirst()
      .orElse(null);
  }
}
