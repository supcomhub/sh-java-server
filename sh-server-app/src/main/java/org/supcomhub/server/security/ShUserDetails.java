package org.supcomhub.server.security;

import lombok.ToString;
import org.springframework.security.core.GrantedAuthority;
import org.supcomhub.server.client.ClientConnection;
import org.supcomhub.server.client.ConnectionAware;

import java.time.Instant;
import java.util.Collection;
import java.util.Set;

@ToString
public class ShUserDetails extends org.springframework.security.core.userdetails.User implements ConnectionAware {

  private final Account account;

  public ShUserDetails(Account account, Collection<? extends GrantedAuthority> authorities) {
    super(account.getPrimaryEmailAddress().getPlain(), account.getPassword(), true, true, true, isNonLocked(account.getBans()), authorities);
    this.account = account;
  }

  public Account getPlayer() {
    return account;
  }

  @Override
  public ClientConnection getClientConnection() {
    return account.getClientConnection();
  }

  public void setClientConnection(ClientConnection clientConnection) {
    account.setClientConnection(clientConnection);
  }

  private static boolean isNonLocked(Set<Ban> banDetails) {
    return banDetails.stream()
      .noneMatch(details ->
        details.getRevocationTime() == null
          && (details.getExpiresAt() == null || details.getExpiresAt().isAfter(Instant.now()))
      );
  }
}
