package org.supcomhub.server.security;

import org.springframework.security.oauth2.provider.client.BaseClientDetails;
import org.supcomhub.server.client.ClientConnection;
import org.supcomhub.server.client.ConnectionAware;

public class ShClientDetails extends BaseClientDetails implements ConnectionAware {
  private ClientConnection clientConnection;

  ShClientDetails(OAuthClient oAuthClient, String scopes, String redirectUris) {
    super(oAuthClient.getId().toString(),
      null,
      scopes,
      "client_credentials",
      "",
      redirectUris);
  }

  @Override
  public ClientConnection getClientConnection() {
    return clientConnection;
  }

  public void setClientConnection(ClientConnection clientConnection) {
    this.clientConnection = clientConnection;
  }
}
