package org.supcomhub.server.security;

import org.springframework.data.jpa.repository.JpaRepository;
import org.supcomhub.server.player.EmailAddress;

import java.util.Optional;

public interface EmailAddressRepository extends JpaRepository<EmailAddress, Integer> {

  Optional<EmailAddress> findAllByDistilled(String distilledEmailAddress);

  boolean existsByDistilled(String distilledEmailAddress);
}
