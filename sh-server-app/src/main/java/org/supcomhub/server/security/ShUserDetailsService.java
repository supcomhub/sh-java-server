package org.supcomhub.server.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Adapter between Spring's {@link UserDetailsService} and SH's {@code account} table.
 */
@Service
public class ShUserDetailsService implements UserDetailsService {

  private final AccountService accountService;

  public ShUserDetailsService(AccountService accountService) {
    this.accountService = accountService;
  }

  @Override
  public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
    Account account = accountService.findAccountByEmail(email)
      .orElseThrow(() -> new UsernameNotFoundException("User could not be found: " + email));

    ArrayList<GrantedAuthority> authorities = new ArrayList<>();
    authorities.add(UserRole.USER);

    // TODO We need to reconsider how to map roles within the code to roles in the database. This is unstable and ugly.
    account.getUserGroups().stream()
      .map(userGroup -> {
        try {
          return UserRole.valueOf(userGroup.getTechnicalName());
        } catch (IllegalArgumentException e) {
          return null;
        }
      })
      .filter(Objects::nonNull)
      .forEach(authorities::add);

    return new ShUserDetails(account, authorities);
  }
}
