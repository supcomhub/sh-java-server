package org.supcomhub.server.security;

import org.springframework.data.jpa.repository.JpaRepository;

public interface BanDetailsRepository extends JpaRepository<Ban, String> {

}
