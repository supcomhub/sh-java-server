package org.supcomhub.server.avatar;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Immutable;
import org.supcomhub.server.common.jpa.AbstractIntegerIdEntity;
import org.supcomhub.server.security.Account;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "avatar_assignment")
@Getter
@Setter
@Immutable
public class AvatarAssociation extends AbstractIntegerIdEntity {

  @Column(name = "selected")
  private boolean selected;

  @ManyToOne(optional = false)
  @JoinColumn(name = "account_id", referencedColumnName = "id", nullable = false)
  private Account account;

  @ManyToOne(optional = false)
  @JoinColumn(name = "avatar_id", referencedColumnName = "id", nullable = false)
  private Avatar avatar;
}
