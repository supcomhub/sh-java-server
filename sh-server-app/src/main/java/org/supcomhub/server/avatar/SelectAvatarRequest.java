package org.supcomhub.server.avatar;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.supcomhub.server.common.ClientMessage;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SelectAvatarRequest implements ClientMessage {
  Integer avatarId;
}
