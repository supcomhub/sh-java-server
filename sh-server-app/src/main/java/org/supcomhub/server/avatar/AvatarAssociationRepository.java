package org.supcomhub.server.avatar;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.supcomhub.server.security.Account;

import java.util.Optional;

@Repository
public interface AvatarAssociationRepository extends JpaRepository<AvatarAssociation, Integer> {

  @Modifying
  @Query("update AvatarAssociation set selected = case when (avatar.id = :avatarId) then 1 else 0 end where account.id = :playerId ")
  void selectAvatar(@Param("playerId") int playerId, @Param("avatarId") int avatarId);

  Optional<AvatarAssociation> findBySelectedIsTrueAndAccountIs(Account player);
}
