package org.supcomhub.server.avatar;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Immutable;
import org.supcomhub.server.common.jpa.AbstractIntegerIdEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.net.URL;
import java.util.Collection;

@Entity
@Table(name = "avatar")
@Getter
@Setter
@Immutable
public class Avatar extends AbstractIntegerIdEntity {

  @Column(name = "url")
  private URL url;

  @Column(name = "description")
  private String description;

  @OneToMany(mappedBy = "avatar")
  private Collection<AvatarAssociation> owners;

}
