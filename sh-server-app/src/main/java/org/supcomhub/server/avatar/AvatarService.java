package org.supcomhub.server.avatar;

import org.springframework.stereotype.Service;
import org.supcomhub.server.error.ErrorCode;
import org.supcomhub.server.error.Requests;
import org.supcomhub.server.security.Account;

import java.util.Optional;

@Service
public class AvatarService {
  private final AvatarAssociationRepository avatarAssociationRepository;

  public AvatarService(AvatarAssociationRepository avatarAssociationRepository) {
    this.avatarAssociationRepository = avatarAssociationRepository;
  }

  public void selectAvatar(Account player, Integer avatarId) {
    Requests.verify(avatarId != null, ErrorCode.MISSING_AVATAR_ID);
    avatarAssociationRepository.selectAvatar(player.getId(), avatarId);
  }

  public Optional<Avatar> getCurrentAvatar(Account player) {
    return avatarAssociationRepository.findBySelectedIsTrueAndAccountIs(player)
      .map(AvatarAssociation::getAvatar);
  }
}
