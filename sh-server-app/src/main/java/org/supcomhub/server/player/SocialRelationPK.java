package org.supcomhub.server.player;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

@Data
public class SocialRelationPK implements Serializable {
  @Id
  @Column(name = "from_id")
  private Integer toId;

  @Id
  @Column(name = "to_id")
  private Integer fromId;
}
