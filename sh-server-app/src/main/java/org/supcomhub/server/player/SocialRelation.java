package org.supcomhub.server.player;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.supcomhub.server.common.jpa.PostgreSQLEnumType;
import org.supcomhub.server.security.Account;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "social_relation")
@IdClass(SocialRelationPK.class)
@NoArgsConstructor
@AllArgsConstructor
@Data
@TypeDef(name = PostgreSQLEnumType.TYPE_NAME, typeClass = PostgreSQLEnumType.class)
public class SocialRelation {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "from_id")
  private Integer fromId;

  @ManyToOne(optional = false)
  @JoinColumn(name = "from_id", updatable = false, insertable = false)
  private Account player;

  @Column(name = "to_id")
  private Integer toId;

  @Column(name = "relation")
  @Enumerated(EnumType.STRING)
  @Type(type = PostgreSQLEnumType.TYPE_NAME)
  private SocialRelationStatus status;
}
