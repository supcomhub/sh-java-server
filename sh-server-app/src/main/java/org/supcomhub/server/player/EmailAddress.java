package org.supcomhub.server.player;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.supcomhub.server.common.jpa.AbstractIntegerIdEntity;
import org.supcomhub.server.security.Account;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "email_address")
public class EmailAddress extends AbstractIntegerIdEntity {
  @Column(name = "plain")
  private String plain;

  @Column(name = "distilled")
  private String distilled;

  @Column(name = "\"primary\"")
  private boolean primary;

  @ManyToOne(optional = false)
  @JoinColumn(name = "account_id")
  private Account owner;
}
