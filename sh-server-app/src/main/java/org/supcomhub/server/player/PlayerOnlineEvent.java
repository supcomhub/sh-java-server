package org.supcomhub.server.player;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.context.ApplicationEvent;
import org.supcomhub.server.security.Account;

@Data
@EqualsAndHashCode(callSuper = true)
public class PlayerOnlineEvent extends ApplicationEvent {
  private final Account player;

  public PlayerOnlineEvent(Object source, Account player) {
    super(source);
    this.player = player;
  }
}
