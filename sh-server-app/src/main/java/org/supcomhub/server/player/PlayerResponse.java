package org.supcomhub.server.player;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import lombok.Value;
import org.jetbrains.annotations.Nullable;
import org.supcomhub.server.common.ServerMessage;

import java.net.URL;
import java.util.Map;
import java.util.TimeZone;

@Getter
@AllArgsConstructor
@ToString
public class PlayerResponse implements ServerMessage {
  private int id;
  private String displayName;
  private String country;
  private TimeZone timeZone;
  private Map<Integer, Integer> ranks;
  private Avatar avatar;
  private String clanTag;
  private Integer numberOfGames;

  @Value
  public static class Avatar {
    URL url;
    @Nullable
    String description;
  }
}
