package org.supcomhub.server.player;

public class LoginDetailsResponse extends PlayerResponse {

  public LoginDetailsResponse(PlayerResponse playerResponse) {
    super(
      playerResponse.getId(),
      playerResponse.getDisplayName(),
      playerResponse.getCountry(),
      playerResponse.getTimeZone(),
      playerResponse.getRanks(),
      playerResponse.getAvatar(),
      playerResponse.getClanTag(),
      playerResponse.getNumberOfGames()
    );
  }
}
