package org.supcomhub.server.player;

import lombok.Value;
import org.supcomhub.server.common.ServerMessage;

@Value(staticConstructor = "of")
public class PlayerOfflineResponse implements ServerMessage {

  int id;
  String displayName;
}
