package org.supcomhub.server.player;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.supcomhub.server.common.jpa.AbstractIntegerIdEntity;
import org.supcomhub.server.security.Account;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "ladder_division_score")
@Getter
@Setter
@NoArgsConstructor
@ToString(of = {"league", "player", "score"}, callSuper = true)
public class PlayerDivisionInfo extends AbstractIntegerIdEntity {

  @Column(name = "season", nullable = false)
  private int season;

  @ManyToOne(optional = false)
  @JoinColumn(name = "account_id")
  private Account player;

  @Column(name = "league", nullable = false)
  private int league;

  @Column(name = "score", nullable = false)
  private float score;

  @Column(name = "games")
  private int games;

  public boolean isInInferiorLeague(PlayerDivisionInfo comparedTo) {
    return league < comparedTo.league;
  }

  public boolean isInSuperiorLeague(PlayerDivisionInfo comparedTo) {
    return league > comparedTo.league;
  }
}
