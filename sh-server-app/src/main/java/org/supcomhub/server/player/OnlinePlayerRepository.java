package org.supcomhub.server.player;

import org.springframework.data.repository.CrudRepository;
import org.supcomhub.server.security.Account;

import java.util.List;
import java.util.Optional;

public interface OnlinePlayerRepository extends CrudRepository<Account, Integer> {

  Optional<Account> findByDisplayName(String displayName);

  List<Account> findAllByCountry(String country);
}
