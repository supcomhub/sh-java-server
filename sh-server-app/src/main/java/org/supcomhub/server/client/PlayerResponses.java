package org.supcomhub.server.client;

import lombok.Data;
import org.supcomhub.server.common.ServerMessage;
import org.supcomhub.server.player.PlayerResponse;

import java.util.Collection;

@Data
public class PlayerResponses implements ServerMessage {
  private final Collection<PlayerResponse> responses;

  public PlayerResponses(Collection<PlayerResponse> responses) {
    this.responses = responses;
  }
}
