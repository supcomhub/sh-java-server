package org.supcomhub.server.client;

import lombok.Data;
import org.supcomhub.server.common.ServerMessage;
import org.supcomhub.server.ice.IceServerList;

import java.util.List;

@Data
public class IceServersResponse implements ServerMessage {
  private final List<IceServerList> iceServerLists;
}
