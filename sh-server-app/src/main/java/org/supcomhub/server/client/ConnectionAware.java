package org.supcomhub.server.client;

public interface ConnectionAware {
  ClientConnection getClientConnection();
}
