package org.supcomhub.server.client;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.supcomhub.server.common.ClientMessage;

/**
 * A request to broadcast a message to all connected clients.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BroadcastRequest implements ClientMessage {
  private String message;
}
