package org.supcomhub.server.client;

import lombok.Data;
import org.supcomhub.server.common.ServerMessage;

@Data
public class ConnectToPeerResponse implements ServerMessage {
  private final String playerName;
  private final int playerId;
  /** TODO document. */
  private final boolean offer;
}
