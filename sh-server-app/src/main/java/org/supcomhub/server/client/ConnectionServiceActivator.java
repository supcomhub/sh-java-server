package org.supcomhub.server.client;

import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.handler.annotation.Header;
import org.supcomhub.server.integration.ChannelNames;
import org.supcomhub.server.integration.MessageHeaders;

import javax.inject.Inject;

@MessageEndpoint
public class ConnectionServiceActivator {

  private final ClientConnectionService clientConnectionService;

  @Inject
  public ConnectionServiceActivator(ClientConnectionService clientConnectionService) {
    this.clientConnectionService = clientConnectionService;
  }

  @ServiceActivator(inputChannel = ChannelNames.DISCONNECT_CLIENT_REQUEST)
  public void disconnectClientRequest(DisconnectClientRequest request, @Header(MessageHeaders.CLIENT_CONNECTION) ClientConnection clientConnection) {
    clientConnectionService.disconnectClient(clientConnection.getAuthentication(), request.getPlayerId());
  }
}
