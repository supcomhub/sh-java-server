package org.supcomhub.server.client;

import lombok.Data;
import org.supcomhub.server.common.ServerMessage;
import org.supcomhub.server.game.GameResponse;

import java.util.Collection;

@Data
public class GameResponses implements ServerMessage {
  private final Collection<GameResponse> responses;

  public GameResponses(Collection<GameResponse> responses) {
    this.responses = responses;
  }
}
