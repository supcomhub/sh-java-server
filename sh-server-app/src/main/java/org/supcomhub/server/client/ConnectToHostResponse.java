package org.supcomhub.server.client;

import lombok.Value;
import org.supcomhub.server.common.ServerMessage;

@Value
public class ConnectToHostResponse implements ServerMessage {
  String hostUsername;
  int hostId;
}
