package org.supcomhub.server.client;

import lombok.Data;
import org.jetbrains.annotations.Nullable;
import org.springframework.security.core.Authentication;
import org.supcomhub.server.integration.Protocol;

import java.net.InetAddress;
import java.time.Instant;

@Data
public class ClientConnection implements ConnectionAware {

  private final String id;
  private final Protocol protocol;
  private final InetAddress clientAddress;
  private String userAgent;
  /** When the last message was received from the client. */
  private Instant lastSeen = Instant.now();
  @Nullable
  private Authentication authentication;

  @Override
  public ClientConnection getClientConnection() {
    return this;
  }
}
