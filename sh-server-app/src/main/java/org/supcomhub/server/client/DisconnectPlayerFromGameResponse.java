package org.supcomhub.server.client;

import lombok.Data;
import org.supcomhub.server.common.ServerMessage;

/**
 * Message sent from the server to the client, telling it that it should close the connection to the specified peer.
 */
@Data
public class DisconnectPlayerFromGameResponse implements ServerMessage {
  private final int playerId;
}
