package org.supcomhub.server.client;

import lombok.Data;
import org.supcomhub.server.common.ServerMessage;

@Data
public class InfoResponse implements ServerMessage {

  private final String message;
}
