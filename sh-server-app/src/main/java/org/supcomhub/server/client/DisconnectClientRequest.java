package org.supcomhub.server.client;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.supcomhub.server.common.ClientMessage;

/**
 * Requests the disconnection of a user's client.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DisconnectClientRequest implements ClientMessage {
  private int playerId;
}
