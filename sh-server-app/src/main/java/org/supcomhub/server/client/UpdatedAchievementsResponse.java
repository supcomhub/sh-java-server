package org.supcomhub.server.client;

import lombok.Data;
import org.supcomhub.server.api.dto.AchievementState;
import org.supcomhub.server.common.ServerMessage;

import java.util.List;

/**
 * Tells the client that the list of achievements has been updated.
 */
@Data
public class UpdatedAchievementsResponse implements ServerMessage {
  private final List<UpdatedAchievement> updatedAchievements;

  @Data
  public static class UpdatedAchievement {
    private final String achievementId;
    private final Integer currentSteps;
    private final AchievementState currentState;
    private final boolean newlyUnlocked;
  }
}
