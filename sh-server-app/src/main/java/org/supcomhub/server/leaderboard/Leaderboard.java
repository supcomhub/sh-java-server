package org.supcomhub.server.leaderboard;

import lombok.Getter;
import org.supcomhub.server.common.jpa.AbstractIntegerIdEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "leaderboard")
@Getter
public class Leaderboard extends AbstractIntegerIdEntity {
  @Column(name = "technical_name")
  private String technicalName;
}
