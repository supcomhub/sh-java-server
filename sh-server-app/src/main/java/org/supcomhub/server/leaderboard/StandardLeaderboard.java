package org.supcomhub.server.leaderboard;

import lombok.Getter;

@Getter
public enum StandardLeaderboard {
  CUSTOM("custom");

  private final String technicalName;

  StandardLeaderboard(String technicalName) {
    this.technicalName = technicalName;
  }
}
