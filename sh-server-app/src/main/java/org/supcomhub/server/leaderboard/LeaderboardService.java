package org.supcomhub.server.leaderboard;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.supcomhub.server.cache.CacheNames;
import org.supcomhub.server.common.jpa.AbstractIntegerIdEntity;

@Service
public class LeaderboardService {

  private final LeaderboardRepository leaderboardRepository;

  public LeaderboardService(LeaderboardRepository leaderboardRepository) {
    this.leaderboardRepository = leaderboardRepository;
  }

  @Cacheable(CacheNames.LEADERBOARD_ID)
  public int getLeaderboardId(StandardLeaderboard leaderboard) {
    return leaderboardRepository.findByTechnicalName(leaderboard.getTechnicalName())
      .map(AbstractIntegerIdEntity::getId)
      .orElseThrow(() -> new IllegalStateException("Unknown leaderboard: " + leaderboard.getTechnicalName()));
  }
}
