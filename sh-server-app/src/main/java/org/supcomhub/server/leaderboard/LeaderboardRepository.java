package org.supcomhub.server.leaderboard;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface LeaderboardRepository extends JpaRepository<Leaderboard, Integer> {
  Optional<Leaderboard> findByTechnicalName(String technicalName);
}
